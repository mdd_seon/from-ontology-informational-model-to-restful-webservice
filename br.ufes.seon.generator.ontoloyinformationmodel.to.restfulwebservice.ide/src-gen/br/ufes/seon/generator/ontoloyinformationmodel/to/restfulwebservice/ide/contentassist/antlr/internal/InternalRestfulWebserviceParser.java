package br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.services.RestfulWebserviceGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalRestfulWebserviceParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'Configuration'", "'{'", "'}'", "'author:'", "'author_email:'", "'repository:'", "'lib_name:'", "'software:'", "'about:'", "'#'", "'is:'", "'module'", "'.'", "'import'", "'.*'", "'entity'", "'extends'", "':'", "'OneToOne'", "'ManyToMany'", "'OneToMany'", "'function'", "'('", "')'", "','"
    };
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__33=33;
    public static final int T__12=12;
    public static final int T__34=34;
    public static final int T__13=13;
    public static final int T__35=35;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalRestfulWebserviceParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalRestfulWebserviceParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalRestfulWebserviceParser.tokenNames; }
    public String getGrammarFileName() { return "InternalRestfulWebservice.g"; }


    	private RestfulWebserviceGrammarAccess grammarAccess;

    	public void setGrammarAccess(RestfulWebserviceGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleApplication"
    // InternalRestfulWebservice.g:53:1: entryRuleApplication : ruleApplication EOF ;
    public final void entryRuleApplication() throws RecognitionException {
        try {
            // InternalRestfulWebservice.g:54:1: ( ruleApplication EOF )
            // InternalRestfulWebservice.g:55:1: ruleApplication EOF
            {
             before(grammarAccess.getApplicationRule()); 
            pushFollow(FOLLOW_1);
            ruleApplication();

            state._fsp--;

             after(grammarAccess.getApplicationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleApplication"


    // $ANTLR start "ruleApplication"
    // InternalRestfulWebservice.g:62:1: ruleApplication : ( ( rule__Application__Group__0 ) ) ;
    public final void ruleApplication() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:66:2: ( ( ( rule__Application__Group__0 ) ) )
            // InternalRestfulWebservice.g:67:2: ( ( rule__Application__Group__0 ) )
            {
            // InternalRestfulWebservice.g:67:2: ( ( rule__Application__Group__0 ) )
            // InternalRestfulWebservice.g:68:3: ( rule__Application__Group__0 )
            {
             before(grammarAccess.getApplicationAccess().getGroup()); 
            // InternalRestfulWebservice.g:69:3: ( rule__Application__Group__0 )
            // InternalRestfulWebservice.g:69:4: rule__Application__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Application__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getApplicationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleApplication"


    // $ANTLR start "entryRuleConfiguration"
    // InternalRestfulWebservice.g:78:1: entryRuleConfiguration : ruleConfiguration EOF ;
    public final void entryRuleConfiguration() throws RecognitionException {
        try {
            // InternalRestfulWebservice.g:79:1: ( ruleConfiguration EOF )
            // InternalRestfulWebservice.g:80:1: ruleConfiguration EOF
            {
             before(grammarAccess.getConfigurationRule()); 
            pushFollow(FOLLOW_1);
            ruleConfiguration();

            state._fsp--;

             after(grammarAccess.getConfigurationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleConfiguration"


    // $ANTLR start "ruleConfiguration"
    // InternalRestfulWebservice.g:87:1: ruleConfiguration : ( ( rule__Configuration__Group__0 ) ) ;
    public final void ruleConfiguration() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:91:2: ( ( ( rule__Configuration__Group__0 ) ) )
            // InternalRestfulWebservice.g:92:2: ( ( rule__Configuration__Group__0 ) )
            {
            // InternalRestfulWebservice.g:92:2: ( ( rule__Configuration__Group__0 ) )
            // InternalRestfulWebservice.g:93:3: ( rule__Configuration__Group__0 )
            {
             before(grammarAccess.getConfigurationAccess().getGroup()); 
            // InternalRestfulWebservice.g:94:3: ( rule__Configuration__Group__0 )
            // InternalRestfulWebservice.g:94:4: rule__Configuration__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Configuration__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getConfigurationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleConfiguration"


    // $ANTLR start "entryRuleAuthor"
    // InternalRestfulWebservice.g:103:1: entryRuleAuthor : ruleAuthor EOF ;
    public final void entryRuleAuthor() throws RecognitionException {
        try {
            // InternalRestfulWebservice.g:104:1: ( ruleAuthor EOF )
            // InternalRestfulWebservice.g:105:1: ruleAuthor EOF
            {
             before(grammarAccess.getAuthorRule()); 
            pushFollow(FOLLOW_1);
            ruleAuthor();

            state._fsp--;

             after(grammarAccess.getAuthorRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAuthor"


    // $ANTLR start "ruleAuthor"
    // InternalRestfulWebservice.g:112:1: ruleAuthor : ( ( rule__Author__Group__0 ) ) ;
    public final void ruleAuthor() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:116:2: ( ( ( rule__Author__Group__0 ) ) )
            // InternalRestfulWebservice.g:117:2: ( ( rule__Author__Group__0 ) )
            {
            // InternalRestfulWebservice.g:117:2: ( ( rule__Author__Group__0 ) )
            // InternalRestfulWebservice.g:118:3: ( rule__Author__Group__0 )
            {
             before(grammarAccess.getAuthorAccess().getGroup()); 
            // InternalRestfulWebservice.g:119:3: ( rule__Author__Group__0 )
            // InternalRestfulWebservice.g:119:4: rule__Author__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Author__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAuthorAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAuthor"


    // $ANTLR start "entryRuleAuthor_Email"
    // InternalRestfulWebservice.g:128:1: entryRuleAuthor_Email : ruleAuthor_Email EOF ;
    public final void entryRuleAuthor_Email() throws RecognitionException {
        try {
            // InternalRestfulWebservice.g:129:1: ( ruleAuthor_Email EOF )
            // InternalRestfulWebservice.g:130:1: ruleAuthor_Email EOF
            {
             before(grammarAccess.getAuthor_EmailRule()); 
            pushFollow(FOLLOW_1);
            ruleAuthor_Email();

            state._fsp--;

             after(grammarAccess.getAuthor_EmailRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAuthor_Email"


    // $ANTLR start "ruleAuthor_Email"
    // InternalRestfulWebservice.g:137:1: ruleAuthor_Email : ( ( rule__Author_Email__Group__0 ) ) ;
    public final void ruleAuthor_Email() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:141:2: ( ( ( rule__Author_Email__Group__0 ) ) )
            // InternalRestfulWebservice.g:142:2: ( ( rule__Author_Email__Group__0 ) )
            {
            // InternalRestfulWebservice.g:142:2: ( ( rule__Author_Email__Group__0 ) )
            // InternalRestfulWebservice.g:143:3: ( rule__Author_Email__Group__0 )
            {
             before(grammarAccess.getAuthor_EmailAccess().getGroup()); 
            // InternalRestfulWebservice.g:144:3: ( rule__Author_Email__Group__0 )
            // InternalRestfulWebservice.g:144:4: rule__Author_Email__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Author_Email__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAuthor_EmailAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAuthor_Email"


    // $ANTLR start "entryRuleRepository"
    // InternalRestfulWebservice.g:153:1: entryRuleRepository : ruleRepository EOF ;
    public final void entryRuleRepository() throws RecognitionException {
        try {
            // InternalRestfulWebservice.g:154:1: ( ruleRepository EOF )
            // InternalRestfulWebservice.g:155:1: ruleRepository EOF
            {
             before(grammarAccess.getRepositoryRule()); 
            pushFollow(FOLLOW_1);
            ruleRepository();

            state._fsp--;

             after(grammarAccess.getRepositoryRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRepository"


    // $ANTLR start "ruleRepository"
    // InternalRestfulWebservice.g:162:1: ruleRepository : ( ( rule__Repository__Group__0 ) ) ;
    public final void ruleRepository() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:166:2: ( ( ( rule__Repository__Group__0 ) ) )
            // InternalRestfulWebservice.g:167:2: ( ( rule__Repository__Group__0 ) )
            {
            // InternalRestfulWebservice.g:167:2: ( ( rule__Repository__Group__0 ) )
            // InternalRestfulWebservice.g:168:3: ( rule__Repository__Group__0 )
            {
             before(grammarAccess.getRepositoryAccess().getGroup()); 
            // InternalRestfulWebservice.g:169:3: ( rule__Repository__Group__0 )
            // InternalRestfulWebservice.g:169:4: rule__Repository__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Repository__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getRepositoryAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRepository"


    // $ANTLR start "entryRuleLib"
    // InternalRestfulWebservice.g:178:1: entryRuleLib : ruleLib EOF ;
    public final void entryRuleLib() throws RecognitionException {
        try {
            // InternalRestfulWebservice.g:179:1: ( ruleLib EOF )
            // InternalRestfulWebservice.g:180:1: ruleLib EOF
            {
             before(grammarAccess.getLibRule()); 
            pushFollow(FOLLOW_1);
            ruleLib();

            state._fsp--;

             after(grammarAccess.getLibRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLib"


    // $ANTLR start "ruleLib"
    // InternalRestfulWebservice.g:187:1: ruleLib : ( ( rule__Lib__Group__0 ) ) ;
    public final void ruleLib() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:191:2: ( ( ( rule__Lib__Group__0 ) ) )
            // InternalRestfulWebservice.g:192:2: ( ( rule__Lib__Group__0 ) )
            {
            // InternalRestfulWebservice.g:192:2: ( ( rule__Lib__Group__0 ) )
            // InternalRestfulWebservice.g:193:3: ( rule__Lib__Group__0 )
            {
             before(grammarAccess.getLibAccess().getGroup()); 
            // InternalRestfulWebservice.g:194:3: ( rule__Lib__Group__0 )
            // InternalRestfulWebservice.g:194:4: rule__Lib__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Lib__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getLibAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLib"


    // $ANTLR start "entryRuleSoftware"
    // InternalRestfulWebservice.g:203:1: entryRuleSoftware : ruleSoftware EOF ;
    public final void entryRuleSoftware() throws RecognitionException {
        try {
            // InternalRestfulWebservice.g:204:1: ( ruleSoftware EOF )
            // InternalRestfulWebservice.g:205:1: ruleSoftware EOF
            {
             before(grammarAccess.getSoftwareRule()); 
            pushFollow(FOLLOW_1);
            ruleSoftware();

            state._fsp--;

             after(grammarAccess.getSoftwareRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSoftware"


    // $ANTLR start "ruleSoftware"
    // InternalRestfulWebservice.g:212:1: ruleSoftware : ( ( rule__Software__Group__0 ) ) ;
    public final void ruleSoftware() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:216:2: ( ( ( rule__Software__Group__0 ) ) )
            // InternalRestfulWebservice.g:217:2: ( ( rule__Software__Group__0 ) )
            {
            // InternalRestfulWebservice.g:217:2: ( ( rule__Software__Group__0 ) )
            // InternalRestfulWebservice.g:218:3: ( rule__Software__Group__0 )
            {
             before(grammarAccess.getSoftwareAccess().getGroup()); 
            // InternalRestfulWebservice.g:219:3: ( rule__Software__Group__0 )
            // InternalRestfulWebservice.g:219:4: rule__Software__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Software__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getSoftwareAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSoftware"


    // $ANTLR start "entryRuleAbout"
    // InternalRestfulWebservice.g:228:1: entryRuleAbout : ruleAbout EOF ;
    public final void entryRuleAbout() throws RecognitionException {
        try {
            // InternalRestfulWebservice.g:229:1: ( ruleAbout EOF )
            // InternalRestfulWebservice.g:230:1: ruleAbout EOF
            {
             before(grammarAccess.getAboutRule()); 
            pushFollow(FOLLOW_1);
            ruleAbout();

            state._fsp--;

             after(grammarAccess.getAboutRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAbout"


    // $ANTLR start "ruleAbout"
    // InternalRestfulWebservice.g:237:1: ruleAbout : ( ( rule__About__Group__0 ) ) ;
    public final void ruleAbout() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:241:2: ( ( ( rule__About__Group__0 ) ) )
            // InternalRestfulWebservice.g:242:2: ( ( rule__About__Group__0 ) )
            {
            // InternalRestfulWebservice.g:242:2: ( ( rule__About__Group__0 ) )
            // InternalRestfulWebservice.g:243:3: ( rule__About__Group__0 )
            {
             before(grammarAccess.getAboutAccess().getGroup()); 
            // InternalRestfulWebservice.g:244:3: ( rule__About__Group__0 )
            // InternalRestfulWebservice.g:244:4: rule__About__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__About__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAboutAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAbout"


    // $ANTLR start "entryRuleDescription"
    // InternalRestfulWebservice.g:253:1: entryRuleDescription : ruleDescription EOF ;
    public final void entryRuleDescription() throws RecognitionException {
        try {
            // InternalRestfulWebservice.g:254:1: ( ruleDescription EOF )
            // InternalRestfulWebservice.g:255:1: ruleDescription EOF
            {
             before(grammarAccess.getDescriptionRule()); 
            pushFollow(FOLLOW_1);
            ruleDescription();

            state._fsp--;

             after(grammarAccess.getDescriptionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDescription"


    // $ANTLR start "ruleDescription"
    // InternalRestfulWebservice.g:262:1: ruleDescription : ( ( rule__Description__Group__0 ) ) ;
    public final void ruleDescription() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:266:2: ( ( ( rule__Description__Group__0 ) ) )
            // InternalRestfulWebservice.g:267:2: ( ( rule__Description__Group__0 ) )
            {
            // InternalRestfulWebservice.g:267:2: ( ( rule__Description__Group__0 ) )
            // InternalRestfulWebservice.g:268:3: ( rule__Description__Group__0 )
            {
             before(grammarAccess.getDescriptionAccess().getGroup()); 
            // InternalRestfulWebservice.g:269:3: ( rule__Description__Group__0 )
            // InternalRestfulWebservice.g:269:4: rule__Description__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Description__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDescriptionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDescription"


    // $ANTLR start "entryRuleEntity_Type"
    // InternalRestfulWebservice.g:278:1: entryRuleEntity_Type : ruleEntity_Type EOF ;
    public final void entryRuleEntity_Type() throws RecognitionException {
        try {
            // InternalRestfulWebservice.g:279:1: ( ruleEntity_Type EOF )
            // InternalRestfulWebservice.g:280:1: ruleEntity_Type EOF
            {
             before(grammarAccess.getEntity_TypeRule()); 
            pushFollow(FOLLOW_1);
            ruleEntity_Type();

            state._fsp--;

             after(grammarAccess.getEntity_TypeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEntity_Type"


    // $ANTLR start "ruleEntity_Type"
    // InternalRestfulWebservice.g:287:1: ruleEntity_Type : ( ( rule__Entity_Type__Group__0 ) ) ;
    public final void ruleEntity_Type() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:291:2: ( ( ( rule__Entity_Type__Group__0 ) ) )
            // InternalRestfulWebservice.g:292:2: ( ( rule__Entity_Type__Group__0 ) )
            {
            // InternalRestfulWebservice.g:292:2: ( ( rule__Entity_Type__Group__0 ) )
            // InternalRestfulWebservice.g:293:3: ( rule__Entity_Type__Group__0 )
            {
             before(grammarAccess.getEntity_TypeAccess().getGroup()); 
            // InternalRestfulWebservice.g:294:3: ( rule__Entity_Type__Group__0 )
            // InternalRestfulWebservice.g:294:4: rule__Entity_Type__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Entity_Type__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getEntity_TypeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEntity_Type"


    // $ANTLR start "entryRuleModule"
    // InternalRestfulWebservice.g:303:1: entryRuleModule : ruleModule EOF ;
    public final void entryRuleModule() throws RecognitionException {
        try {
            // InternalRestfulWebservice.g:304:1: ( ruleModule EOF )
            // InternalRestfulWebservice.g:305:1: ruleModule EOF
            {
             before(grammarAccess.getModuleRule()); 
            pushFollow(FOLLOW_1);
            ruleModule();

            state._fsp--;

             after(grammarAccess.getModuleRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleModule"


    // $ANTLR start "ruleModule"
    // InternalRestfulWebservice.g:312:1: ruleModule : ( ( rule__Module__Group__0 ) ) ;
    public final void ruleModule() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:316:2: ( ( ( rule__Module__Group__0 ) ) )
            // InternalRestfulWebservice.g:317:2: ( ( rule__Module__Group__0 ) )
            {
            // InternalRestfulWebservice.g:317:2: ( ( rule__Module__Group__0 ) )
            // InternalRestfulWebservice.g:318:3: ( rule__Module__Group__0 )
            {
             before(grammarAccess.getModuleAccess().getGroup()); 
            // InternalRestfulWebservice.g:319:3: ( rule__Module__Group__0 )
            // InternalRestfulWebservice.g:319:4: rule__Module__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Module__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getModuleAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleModule"


    // $ANTLR start "entryRuleAbstractElement"
    // InternalRestfulWebservice.g:328:1: entryRuleAbstractElement : ruleAbstractElement EOF ;
    public final void entryRuleAbstractElement() throws RecognitionException {
        try {
            // InternalRestfulWebservice.g:329:1: ( ruleAbstractElement EOF )
            // InternalRestfulWebservice.g:330:1: ruleAbstractElement EOF
            {
             before(grammarAccess.getAbstractElementRule()); 
            pushFollow(FOLLOW_1);
            ruleAbstractElement();

            state._fsp--;

             after(grammarAccess.getAbstractElementRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAbstractElement"


    // $ANTLR start "ruleAbstractElement"
    // InternalRestfulWebservice.g:337:1: ruleAbstractElement : ( ( rule__AbstractElement__Alternatives ) ) ;
    public final void ruleAbstractElement() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:341:2: ( ( ( rule__AbstractElement__Alternatives ) ) )
            // InternalRestfulWebservice.g:342:2: ( ( rule__AbstractElement__Alternatives ) )
            {
            // InternalRestfulWebservice.g:342:2: ( ( rule__AbstractElement__Alternatives ) )
            // InternalRestfulWebservice.g:343:3: ( rule__AbstractElement__Alternatives )
            {
             before(grammarAccess.getAbstractElementAccess().getAlternatives()); 
            // InternalRestfulWebservice.g:344:3: ( rule__AbstractElement__Alternatives )
            // InternalRestfulWebservice.g:344:4: rule__AbstractElement__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__AbstractElement__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getAbstractElementAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAbstractElement"


    // $ANTLR start "entryRuleQualifiedName"
    // InternalRestfulWebservice.g:353:1: entryRuleQualifiedName : ruleQualifiedName EOF ;
    public final void entryRuleQualifiedName() throws RecognitionException {
        try {
            // InternalRestfulWebservice.g:354:1: ( ruleQualifiedName EOF )
            // InternalRestfulWebservice.g:355:1: ruleQualifiedName EOF
            {
             before(grammarAccess.getQualifiedNameRule()); 
            pushFollow(FOLLOW_1);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getQualifiedNameRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQualifiedName"


    // $ANTLR start "ruleQualifiedName"
    // InternalRestfulWebservice.g:362:1: ruleQualifiedName : ( ( rule__QualifiedName__Group__0 ) ) ;
    public final void ruleQualifiedName() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:366:2: ( ( ( rule__QualifiedName__Group__0 ) ) )
            // InternalRestfulWebservice.g:367:2: ( ( rule__QualifiedName__Group__0 ) )
            {
            // InternalRestfulWebservice.g:367:2: ( ( rule__QualifiedName__Group__0 ) )
            // InternalRestfulWebservice.g:368:3: ( rule__QualifiedName__Group__0 )
            {
             before(grammarAccess.getQualifiedNameAccess().getGroup()); 
            // InternalRestfulWebservice.g:369:3: ( rule__QualifiedName__Group__0 )
            // InternalRestfulWebservice.g:369:4: rule__QualifiedName__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQualifiedNameAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQualifiedName"


    // $ANTLR start "entryRuleImport"
    // InternalRestfulWebservice.g:378:1: entryRuleImport : ruleImport EOF ;
    public final void entryRuleImport() throws RecognitionException {
        try {
            // InternalRestfulWebservice.g:379:1: ( ruleImport EOF )
            // InternalRestfulWebservice.g:380:1: ruleImport EOF
            {
             before(grammarAccess.getImportRule()); 
            pushFollow(FOLLOW_1);
            ruleImport();

            state._fsp--;

             after(grammarAccess.getImportRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleImport"


    // $ANTLR start "ruleImport"
    // InternalRestfulWebservice.g:387:1: ruleImport : ( ( rule__Import__Group__0 ) ) ;
    public final void ruleImport() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:391:2: ( ( ( rule__Import__Group__0 ) ) )
            // InternalRestfulWebservice.g:392:2: ( ( rule__Import__Group__0 ) )
            {
            // InternalRestfulWebservice.g:392:2: ( ( rule__Import__Group__0 ) )
            // InternalRestfulWebservice.g:393:3: ( rule__Import__Group__0 )
            {
             before(grammarAccess.getImportAccess().getGroup()); 
            // InternalRestfulWebservice.g:394:3: ( rule__Import__Group__0 )
            // InternalRestfulWebservice.g:394:4: rule__Import__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Import__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getImportAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleImport"


    // $ANTLR start "entryRuleQualifiedNameWithWildcard"
    // InternalRestfulWebservice.g:403:1: entryRuleQualifiedNameWithWildcard : ruleQualifiedNameWithWildcard EOF ;
    public final void entryRuleQualifiedNameWithWildcard() throws RecognitionException {
        try {
            // InternalRestfulWebservice.g:404:1: ( ruleQualifiedNameWithWildcard EOF )
            // InternalRestfulWebservice.g:405:1: ruleQualifiedNameWithWildcard EOF
            {
             before(grammarAccess.getQualifiedNameWithWildcardRule()); 
            pushFollow(FOLLOW_1);
            ruleQualifiedNameWithWildcard();

            state._fsp--;

             after(grammarAccess.getQualifiedNameWithWildcardRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQualifiedNameWithWildcard"


    // $ANTLR start "ruleQualifiedNameWithWildcard"
    // InternalRestfulWebservice.g:412:1: ruleQualifiedNameWithWildcard : ( ( rule__QualifiedNameWithWildcard__Group__0 ) ) ;
    public final void ruleQualifiedNameWithWildcard() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:416:2: ( ( ( rule__QualifiedNameWithWildcard__Group__0 ) ) )
            // InternalRestfulWebservice.g:417:2: ( ( rule__QualifiedNameWithWildcard__Group__0 ) )
            {
            // InternalRestfulWebservice.g:417:2: ( ( rule__QualifiedNameWithWildcard__Group__0 ) )
            // InternalRestfulWebservice.g:418:3: ( rule__QualifiedNameWithWildcard__Group__0 )
            {
             before(grammarAccess.getQualifiedNameWithWildcardAccess().getGroup()); 
            // InternalRestfulWebservice.g:419:3: ( rule__QualifiedNameWithWildcard__Group__0 )
            // InternalRestfulWebservice.g:419:4: rule__QualifiedNameWithWildcard__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__QualifiedNameWithWildcard__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQualifiedNameWithWildcardAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQualifiedNameWithWildcard"


    // $ANTLR start "entryRuleEntity"
    // InternalRestfulWebservice.g:428:1: entryRuleEntity : ruleEntity EOF ;
    public final void entryRuleEntity() throws RecognitionException {
        try {
            // InternalRestfulWebservice.g:429:1: ( ruleEntity EOF )
            // InternalRestfulWebservice.g:430:1: ruleEntity EOF
            {
             before(grammarAccess.getEntityRule()); 
            pushFollow(FOLLOW_1);
            ruleEntity();

            state._fsp--;

             after(grammarAccess.getEntityRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEntity"


    // $ANTLR start "ruleEntity"
    // InternalRestfulWebservice.g:437:1: ruleEntity : ( ( rule__Entity__Group__0 ) ) ;
    public final void ruleEntity() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:441:2: ( ( ( rule__Entity__Group__0 ) ) )
            // InternalRestfulWebservice.g:442:2: ( ( rule__Entity__Group__0 ) )
            {
            // InternalRestfulWebservice.g:442:2: ( ( rule__Entity__Group__0 ) )
            // InternalRestfulWebservice.g:443:3: ( rule__Entity__Group__0 )
            {
             before(grammarAccess.getEntityAccess().getGroup()); 
            // InternalRestfulWebservice.g:444:3: ( rule__Entity__Group__0 )
            // InternalRestfulWebservice.g:444:4: rule__Entity__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Entity__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getEntityAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEntity"


    // $ANTLR start "entryRuleAttribute"
    // InternalRestfulWebservice.g:453:1: entryRuleAttribute : ruleAttribute EOF ;
    public final void entryRuleAttribute() throws RecognitionException {
        try {
            // InternalRestfulWebservice.g:454:1: ( ruleAttribute EOF )
            // InternalRestfulWebservice.g:455:1: ruleAttribute EOF
            {
             before(grammarAccess.getAttributeRule()); 
            pushFollow(FOLLOW_1);
            ruleAttribute();

            state._fsp--;

             after(grammarAccess.getAttributeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAttribute"


    // $ANTLR start "ruleAttribute"
    // InternalRestfulWebservice.g:462:1: ruleAttribute : ( ( rule__Attribute__Group__0 ) ) ;
    public final void ruleAttribute() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:466:2: ( ( ( rule__Attribute__Group__0 ) ) )
            // InternalRestfulWebservice.g:467:2: ( ( rule__Attribute__Group__0 ) )
            {
            // InternalRestfulWebservice.g:467:2: ( ( rule__Attribute__Group__0 ) )
            // InternalRestfulWebservice.g:468:3: ( rule__Attribute__Group__0 )
            {
             before(grammarAccess.getAttributeAccess().getGroup()); 
            // InternalRestfulWebservice.g:469:3: ( rule__Attribute__Group__0 )
            // InternalRestfulWebservice.g:469:4: rule__Attribute__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Attribute__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAttributeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAttribute"


    // $ANTLR start "entryRuleRelation"
    // InternalRestfulWebservice.g:478:1: entryRuleRelation : ruleRelation EOF ;
    public final void entryRuleRelation() throws RecognitionException {
        try {
            // InternalRestfulWebservice.g:479:1: ( ruleRelation EOF )
            // InternalRestfulWebservice.g:480:1: ruleRelation EOF
            {
             before(grammarAccess.getRelationRule()); 
            pushFollow(FOLLOW_1);
            ruleRelation();

            state._fsp--;

             after(grammarAccess.getRelationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRelation"


    // $ANTLR start "ruleRelation"
    // InternalRestfulWebservice.g:487:1: ruleRelation : ( ( rule__Relation__Alternatives ) ) ;
    public final void ruleRelation() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:491:2: ( ( ( rule__Relation__Alternatives ) ) )
            // InternalRestfulWebservice.g:492:2: ( ( rule__Relation__Alternatives ) )
            {
            // InternalRestfulWebservice.g:492:2: ( ( rule__Relation__Alternatives ) )
            // InternalRestfulWebservice.g:493:3: ( rule__Relation__Alternatives )
            {
             before(grammarAccess.getRelationAccess().getAlternatives()); 
            // InternalRestfulWebservice.g:494:3: ( rule__Relation__Alternatives )
            // InternalRestfulWebservice.g:494:4: rule__Relation__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Relation__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getRelationAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRelation"


    // $ANTLR start "entryRuleOneToOne"
    // InternalRestfulWebservice.g:503:1: entryRuleOneToOne : ruleOneToOne EOF ;
    public final void entryRuleOneToOne() throws RecognitionException {
        try {
            // InternalRestfulWebservice.g:504:1: ( ruleOneToOne EOF )
            // InternalRestfulWebservice.g:505:1: ruleOneToOne EOF
            {
             before(grammarAccess.getOneToOneRule()); 
            pushFollow(FOLLOW_1);
            ruleOneToOne();

            state._fsp--;

             after(grammarAccess.getOneToOneRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOneToOne"


    // $ANTLR start "ruleOneToOne"
    // InternalRestfulWebservice.g:512:1: ruleOneToOne : ( ( rule__OneToOne__Group__0 ) ) ;
    public final void ruleOneToOne() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:516:2: ( ( ( rule__OneToOne__Group__0 ) ) )
            // InternalRestfulWebservice.g:517:2: ( ( rule__OneToOne__Group__0 ) )
            {
            // InternalRestfulWebservice.g:517:2: ( ( rule__OneToOne__Group__0 ) )
            // InternalRestfulWebservice.g:518:3: ( rule__OneToOne__Group__0 )
            {
             before(grammarAccess.getOneToOneAccess().getGroup()); 
            // InternalRestfulWebservice.g:519:3: ( rule__OneToOne__Group__0 )
            // InternalRestfulWebservice.g:519:4: rule__OneToOne__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__OneToOne__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getOneToOneAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOneToOne"


    // $ANTLR start "entryRuleManyToMany"
    // InternalRestfulWebservice.g:528:1: entryRuleManyToMany : ruleManyToMany EOF ;
    public final void entryRuleManyToMany() throws RecognitionException {
        try {
            // InternalRestfulWebservice.g:529:1: ( ruleManyToMany EOF )
            // InternalRestfulWebservice.g:530:1: ruleManyToMany EOF
            {
             before(grammarAccess.getManyToManyRule()); 
            pushFollow(FOLLOW_1);
            ruleManyToMany();

            state._fsp--;

             after(grammarAccess.getManyToManyRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleManyToMany"


    // $ANTLR start "ruleManyToMany"
    // InternalRestfulWebservice.g:537:1: ruleManyToMany : ( ( rule__ManyToMany__Group__0 ) ) ;
    public final void ruleManyToMany() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:541:2: ( ( ( rule__ManyToMany__Group__0 ) ) )
            // InternalRestfulWebservice.g:542:2: ( ( rule__ManyToMany__Group__0 ) )
            {
            // InternalRestfulWebservice.g:542:2: ( ( rule__ManyToMany__Group__0 ) )
            // InternalRestfulWebservice.g:543:3: ( rule__ManyToMany__Group__0 )
            {
             before(grammarAccess.getManyToManyAccess().getGroup()); 
            // InternalRestfulWebservice.g:544:3: ( rule__ManyToMany__Group__0 )
            // InternalRestfulWebservice.g:544:4: rule__ManyToMany__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ManyToMany__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getManyToManyAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleManyToMany"


    // $ANTLR start "entryRuleOneToMany"
    // InternalRestfulWebservice.g:553:1: entryRuleOneToMany : ruleOneToMany EOF ;
    public final void entryRuleOneToMany() throws RecognitionException {
        try {
            // InternalRestfulWebservice.g:554:1: ( ruleOneToMany EOF )
            // InternalRestfulWebservice.g:555:1: ruleOneToMany EOF
            {
             before(grammarAccess.getOneToManyRule()); 
            pushFollow(FOLLOW_1);
            ruleOneToMany();

            state._fsp--;

             after(grammarAccess.getOneToManyRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOneToMany"


    // $ANTLR start "ruleOneToMany"
    // InternalRestfulWebservice.g:562:1: ruleOneToMany : ( ( rule__OneToMany__Group__0 ) ) ;
    public final void ruleOneToMany() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:566:2: ( ( ( rule__OneToMany__Group__0 ) ) )
            // InternalRestfulWebservice.g:567:2: ( ( rule__OneToMany__Group__0 ) )
            {
            // InternalRestfulWebservice.g:567:2: ( ( rule__OneToMany__Group__0 ) )
            // InternalRestfulWebservice.g:568:3: ( rule__OneToMany__Group__0 )
            {
             before(grammarAccess.getOneToManyAccess().getGroup()); 
            // InternalRestfulWebservice.g:569:3: ( rule__OneToMany__Group__0 )
            // InternalRestfulWebservice.g:569:4: rule__OneToMany__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__OneToMany__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getOneToManyAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOneToMany"


    // $ANTLR start "entryRuleFunction"
    // InternalRestfulWebservice.g:578:1: entryRuleFunction : ruleFunction EOF ;
    public final void entryRuleFunction() throws RecognitionException {
        try {
            // InternalRestfulWebservice.g:579:1: ( ruleFunction EOF )
            // InternalRestfulWebservice.g:580:1: ruleFunction EOF
            {
             before(grammarAccess.getFunctionRule()); 
            pushFollow(FOLLOW_1);
            ruleFunction();

            state._fsp--;

             after(grammarAccess.getFunctionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFunction"


    // $ANTLR start "ruleFunction"
    // InternalRestfulWebservice.g:587:1: ruleFunction : ( ( rule__Function__Group__0 ) ) ;
    public final void ruleFunction() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:591:2: ( ( ( rule__Function__Group__0 ) ) )
            // InternalRestfulWebservice.g:592:2: ( ( rule__Function__Group__0 ) )
            {
            // InternalRestfulWebservice.g:592:2: ( ( rule__Function__Group__0 ) )
            // InternalRestfulWebservice.g:593:3: ( rule__Function__Group__0 )
            {
             before(grammarAccess.getFunctionAccess().getGroup()); 
            // InternalRestfulWebservice.g:594:3: ( rule__Function__Group__0 )
            // InternalRestfulWebservice.g:594:4: rule__Function__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Function__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getFunctionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFunction"


    // $ANTLR start "rule__AbstractElement__Alternatives"
    // InternalRestfulWebservice.g:602:1: rule__AbstractElement__Alternatives : ( ( ruleModule ) | ( ruleEntity ) | ( ruleImport ) );
    public final void rule__AbstractElement__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:606:1: ( ( ruleModule ) | ( ruleEntity ) | ( ruleImport ) )
            int alt1=3;
            switch ( input.LA(1) ) {
            case 20:
                {
                int LA1_1 = input.LA(2);

                if ( (LA1_1==RULE_STRING) ) {
                    int LA1_5 = input.LA(3);

                    if ( (LA1_5==22) ) {
                        alt1=1;
                    }
                    else if ( (LA1_5==26) ) {
                        alt1=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 1, 5, input);

                        throw nvae;
                    }
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 1, 1, input);

                    throw nvae;
                }
                }
                break;
            case 22:
                {
                alt1=1;
                }
                break;
            case 26:
                {
                alt1=2;
                }
                break;
            case 24:
                {
                alt1=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }

            switch (alt1) {
                case 1 :
                    // InternalRestfulWebservice.g:607:2: ( ruleModule )
                    {
                    // InternalRestfulWebservice.g:607:2: ( ruleModule )
                    // InternalRestfulWebservice.g:608:3: ruleModule
                    {
                     before(grammarAccess.getAbstractElementAccess().getModuleParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleModule();

                    state._fsp--;

                     after(grammarAccess.getAbstractElementAccess().getModuleParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalRestfulWebservice.g:613:2: ( ruleEntity )
                    {
                    // InternalRestfulWebservice.g:613:2: ( ruleEntity )
                    // InternalRestfulWebservice.g:614:3: ruleEntity
                    {
                     before(grammarAccess.getAbstractElementAccess().getEntityParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleEntity();

                    state._fsp--;

                     after(grammarAccess.getAbstractElementAccess().getEntityParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalRestfulWebservice.g:619:2: ( ruleImport )
                    {
                    // InternalRestfulWebservice.g:619:2: ( ruleImport )
                    // InternalRestfulWebservice.g:620:3: ruleImport
                    {
                     before(grammarAccess.getAbstractElementAccess().getImportParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    ruleImport();

                    state._fsp--;

                     after(grammarAccess.getAbstractElementAccess().getImportParserRuleCall_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AbstractElement__Alternatives"


    // $ANTLR start "rule__Relation__Alternatives"
    // InternalRestfulWebservice.g:629:1: rule__Relation__Alternatives : ( ( ruleOneToOne ) | ( ruleManyToMany ) | ( ruleOneToMany ) );
    public final void rule__Relation__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:633:1: ( ( ruleOneToOne ) | ( ruleManyToMany ) | ( ruleOneToMany ) )
            int alt2=3;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==RULE_ID) ) {
                switch ( input.LA(2) ) {
                case 31:
                    {
                    alt2=3;
                    }
                    break;
                case 30:
                    {
                    alt2=2;
                    }
                    break;
                case 29:
                    {
                    alt2=1;
                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 2, 1, input);

                    throw nvae;
                }

            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // InternalRestfulWebservice.g:634:2: ( ruleOneToOne )
                    {
                    // InternalRestfulWebservice.g:634:2: ( ruleOneToOne )
                    // InternalRestfulWebservice.g:635:3: ruleOneToOne
                    {
                     before(grammarAccess.getRelationAccess().getOneToOneParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleOneToOne();

                    state._fsp--;

                     after(grammarAccess.getRelationAccess().getOneToOneParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalRestfulWebservice.g:640:2: ( ruleManyToMany )
                    {
                    // InternalRestfulWebservice.g:640:2: ( ruleManyToMany )
                    // InternalRestfulWebservice.g:641:3: ruleManyToMany
                    {
                     before(grammarAccess.getRelationAccess().getManyToManyParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleManyToMany();

                    state._fsp--;

                     after(grammarAccess.getRelationAccess().getManyToManyParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalRestfulWebservice.g:646:2: ( ruleOneToMany )
                    {
                    // InternalRestfulWebservice.g:646:2: ( ruleOneToMany )
                    // InternalRestfulWebservice.g:647:3: ruleOneToMany
                    {
                     before(grammarAccess.getRelationAccess().getOneToManyParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    ruleOneToMany();

                    state._fsp--;

                     after(grammarAccess.getRelationAccess().getOneToManyParserRuleCall_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relation__Alternatives"


    // $ANTLR start "rule__Application__Group__0"
    // InternalRestfulWebservice.g:656:1: rule__Application__Group__0 : rule__Application__Group__0__Impl rule__Application__Group__1 ;
    public final void rule__Application__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:660:1: ( rule__Application__Group__0__Impl rule__Application__Group__1 )
            // InternalRestfulWebservice.g:661:2: rule__Application__Group__0__Impl rule__Application__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Application__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Application__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Application__Group__0"


    // $ANTLR start "rule__Application__Group__0__Impl"
    // InternalRestfulWebservice.g:668:1: rule__Application__Group__0__Impl : ( ( rule__Application__ConfigurationAssignment_0 )? ) ;
    public final void rule__Application__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:672:1: ( ( ( rule__Application__ConfigurationAssignment_0 )? ) )
            // InternalRestfulWebservice.g:673:1: ( ( rule__Application__ConfigurationAssignment_0 )? )
            {
            // InternalRestfulWebservice.g:673:1: ( ( rule__Application__ConfigurationAssignment_0 )? )
            // InternalRestfulWebservice.g:674:2: ( rule__Application__ConfigurationAssignment_0 )?
            {
             before(grammarAccess.getApplicationAccess().getConfigurationAssignment_0()); 
            // InternalRestfulWebservice.g:675:2: ( rule__Application__ConfigurationAssignment_0 )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==11) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // InternalRestfulWebservice.g:675:3: rule__Application__ConfigurationAssignment_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Application__ConfigurationAssignment_0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getApplicationAccess().getConfigurationAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Application__Group__0__Impl"


    // $ANTLR start "rule__Application__Group__1"
    // InternalRestfulWebservice.g:683:1: rule__Application__Group__1 : rule__Application__Group__1__Impl ;
    public final void rule__Application__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:687:1: ( rule__Application__Group__1__Impl )
            // InternalRestfulWebservice.g:688:2: rule__Application__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Application__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Application__Group__1"


    // $ANTLR start "rule__Application__Group__1__Impl"
    // InternalRestfulWebservice.g:694:1: rule__Application__Group__1__Impl : ( ( rule__Application__AbstractElementsAssignment_1 )* ) ;
    public final void rule__Application__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:698:1: ( ( ( rule__Application__AbstractElementsAssignment_1 )* ) )
            // InternalRestfulWebservice.g:699:1: ( ( rule__Application__AbstractElementsAssignment_1 )* )
            {
            // InternalRestfulWebservice.g:699:1: ( ( rule__Application__AbstractElementsAssignment_1 )* )
            // InternalRestfulWebservice.g:700:2: ( rule__Application__AbstractElementsAssignment_1 )*
            {
             before(grammarAccess.getApplicationAccess().getAbstractElementsAssignment_1()); 
            // InternalRestfulWebservice.g:701:2: ( rule__Application__AbstractElementsAssignment_1 )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==20||LA4_0==22||LA4_0==24||LA4_0==26) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalRestfulWebservice.g:701:3: rule__Application__AbstractElementsAssignment_1
            	    {
            	    pushFollow(FOLLOW_4);
            	    rule__Application__AbstractElementsAssignment_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

             after(grammarAccess.getApplicationAccess().getAbstractElementsAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Application__Group__1__Impl"


    // $ANTLR start "rule__Configuration__Group__0"
    // InternalRestfulWebservice.g:710:1: rule__Configuration__Group__0 : rule__Configuration__Group__0__Impl rule__Configuration__Group__1 ;
    public final void rule__Configuration__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:714:1: ( rule__Configuration__Group__0__Impl rule__Configuration__Group__1 )
            // InternalRestfulWebservice.g:715:2: rule__Configuration__Group__0__Impl rule__Configuration__Group__1
            {
            pushFollow(FOLLOW_5);
            rule__Configuration__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Configuration__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__0"


    // $ANTLR start "rule__Configuration__Group__0__Impl"
    // InternalRestfulWebservice.g:722:1: rule__Configuration__Group__0__Impl : ( 'Configuration' ) ;
    public final void rule__Configuration__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:726:1: ( ( 'Configuration' ) )
            // InternalRestfulWebservice.g:727:1: ( 'Configuration' )
            {
            // InternalRestfulWebservice.g:727:1: ( 'Configuration' )
            // InternalRestfulWebservice.g:728:2: 'Configuration'
            {
             before(grammarAccess.getConfigurationAccess().getConfigurationKeyword_0()); 
            match(input,11,FOLLOW_2); 
             after(grammarAccess.getConfigurationAccess().getConfigurationKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__0__Impl"


    // $ANTLR start "rule__Configuration__Group__1"
    // InternalRestfulWebservice.g:737:1: rule__Configuration__Group__1 : rule__Configuration__Group__1__Impl rule__Configuration__Group__2 ;
    public final void rule__Configuration__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:741:1: ( rule__Configuration__Group__1__Impl rule__Configuration__Group__2 )
            // InternalRestfulWebservice.g:742:2: rule__Configuration__Group__1__Impl rule__Configuration__Group__2
            {
            pushFollow(FOLLOW_6);
            rule__Configuration__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Configuration__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__1"


    // $ANTLR start "rule__Configuration__Group__1__Impl"
    // InternalRestfulWebservice.g:749:1: rule__Configuration__Group__1__Impl : ( '{' ) ;
    public final void rule__Configuration__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:753:1: ( ( '{' ) )
            // InternalRestfulWebservice.g:754:1: ( '{' )
            {
            // InternalRestfulWebservice.g:754:1: ( '{' )
            // InternalRestfulWebservice.g:755:2: '{'
            {
             before(grammarAccess.getConfigurationAccess().getLeftCurlyBracketKeyword_1()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getConfigurationAccess().getLeftCurlyBracketKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__1__Impl"


    // $ANTLR start "rule__Configuration__Group__2"
    // InternalRestfulWebservice.g:764:1: rule__Configuration__Group__2 : rule__Configuration__Group__2__Impl rule__Configuration__Group__3 ;
    public final void rule__Configuration__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:768:1: ( rule__Configuration__Group__2__Impl rule__Configuration__Group__3 )
            // InternalRestfulWebservice.g:769:2: rule__Configuration__Group__2__Impl rule__Configuration__Group__3
            {
            pushFollow(FOLLOW_7);
            rule__Configuration__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Configuration__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__2"


    // $ANTLR start "rule__Configuration__Group__2__Impl"
    // InternalRestfulWebservice.g:776:1: rule__Configuration__Group__2__Impl : ( ( rule__Configuration__SoftwareAssignment_2 ) ) ;
    public final void rule__Configuration__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:780:1: ( ( ( rule__Configuration__SoftwareAssignment_2 ) ) )
            // InternalRestfulWebservice.g:781:1: ( ( rule__Configuration__SoftwareAssignment_2 ) )
            {
            // InternalRestfulWebservice.g:781:1: ( ( rule__Configuration__SoftwareAssignment_2 ) )
            // InternalRestfulWebservice.g:782:2: ( rule__Configuration__SoftwareAssignment_2 )
            {
             before(grammarAccess.getConfigurationAccess().getSoftwareAssignment_2()); 
            // InternalRestfulWebservice.g:783:2: ( rule__Configuration__SoftwareAssignment_2 )
            // InternalRestfulWebservice.g:783:3: rule__Configuration__SoftwareAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Configuration__SoftwareAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getConfigurationAccess().getSoftwareAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__2__Impl"


    // $ANTLR start "rule__Configuration__Group__3"
    // InternalRestfulWebservice.g:791:1: rule__Configuration__Group__3 : rule__Configuration__Group__3__Impl rule__Configuration__Group__4 ;
    public final void rule__Configuration__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:795:1: ( rule__Configuration__Group__3__Impl rule__Configuration__Group__4 )
            // InternalRestfulWebservice.g:796:2: rule__Configuration__Group__3__Impl rule__Configuration__Group__4
            {
            pushFollow(FOLLOW_8);
            rule__Configuration__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Configuration__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__3"


    // $ANTLR start "rule__Configuration__Group__3__Impl"
    // InternalRestfulWebservice.g:803:1: rule__Configuration__Group__3__Impl : ( ( rule__Configuration__AboutAssignment_3 ) ) ;
    public final void rule__Configuration__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:807:1: ( ( ( rule__Configuration__AboutAssignment_3 ) ) )
            // InternalRestfulWebservice.g:808:1: ( ( rule__Configuration__AboutAssignment_3 ) )
            {
            // InternalRestfulWebservice.g:808:1: ( ( rule__Configuration__AboutAssignment_3 ) )
            // InternalRestfulWebservice.g:809:2: ( rule__Configuration__AboutAssignment_3 )
            {
             before(grammarAccess.getConfigurationAccess().getAboutAssignment_3()); 
            // InternalRestfulWebservice.g:810:2: ( rule__Configuration__AboutAssignment_3 )
            // InternalRestfulWebservice.g:810:3: rule__Configuration__AboutAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Configuration__AboutAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getConfigurationAccess().getAboutAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__3__Impl"


    // $ANTLR start "rule__Configuration__Group__4"
    // InternalRestfulWebservice.g:818:1: rule__Configuration__Group__4 : rule__Configuration__Group__4__Impl rule__Configuration__Group__5 ;
    public final void rule__Configuration__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:822:1: ( rule__Configuration__Group__4__Impl rule__Configuration__Group__5 )
            // InternalRestfulWebservice.g:823:2: rule__Configuration__Group__4__Impl rule__Configuration__Group__5
            {
            pushFollow(FOLLOW_9);
            rule__Configuration__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Configuration__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__4"


    // $ANTLR start "rule__Configuration__Group__4__Impl"
    // InternalRestfulWebservice.g:830:1: rule__Configuration__Group__4__Impl : ( ( rule__Configuration__LibAssignment_4 ) ) ;
    public final void rule__Configuration__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:834:1: ( ( ( rule__Configuration__LibAssignment_4 ) ) )
            // InternalRestfulWebservice.g:835:1: ( ( rule__Configuration__LibAssignment_4 ) )
            {
            // InternalRestfulWebservice.g:835:1: ( ( rule__Configuration__LibAssignment_4 ) )
            // InternalRestfulWebservice.g:836:2: ( rule__Configuration__LibAssignment_4 )
            {
             before(grammarAccess.getConfigurationAccess().getLibAssignment_4()); 
            // InternalRestfulWebservice.g:837:2: ( rule__Configuration__LibAssignment_4 )
            // InternalRestfulWebservice.g:837:3: rule__Configuration__LibAssignment_4
            {
            pushFollow(FOLLOW_2);
            rule__Configuration__LibAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getConfigurationAccess().getLibAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__4__Impl"


    // $ANTLR start "rule__Configuration__Group__5"
    // InternalRestfulWebservice.g:845:1: rule__Configuration__Group__5 : rule__Configuration__Group__5__Impl rule__Configuration__Group__6 ;
    public final void rule__Configuration__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:849:1: ( rule__Configuration__Group__5__Impl rule__Configuration__Group__6 )
            // InternalRestfulWebservice.g:850:2: rule__Configuration__Group__5__Impl rule__Configuration__Group__6
            {
            pushFollow(FOLLOW_10);
            rule__Configuration__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Configuration__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__5"


    // $ANTLR start "rule__Configuration__Group__5__Impl"
    // InternalRestfulWebservice.g:857:1: rule__Configuration__Group__5__Impl : ( ( rule__Configuration__AuthorAssignment_5 ) ) ;
    public final void rule__Configuration__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:861:1: ( ( ( rule__Configuration__AuthorAssignment_5 ) ) )
            // InternalRestfulWebservice.g:862:1: ( ( rule__Configuration__AuthorAssignment_5 ) )
            {
            // InternalRestfulWebservice.g:862:1: ( ( rule__Configuration__AuthorAssignment_5 ) )
            // InternalRestfulWebservice.g:863:2: ( rule__Configuration__AuthorAssignment_5 )
            {
             before(grammarAccess.getConfigurationAccess().getAuthorAssignment_5()); 
            // InternalRestfulWebservice.g:864:2: ( rule__Configuration__AuthorAssignment_5 )
            // InternalRestfulWebservice.g:864:3: rule__Configuration__AuthorAssignment_5
            {
            pushFollow(FOLLOW_2);
            rule__Configuration__AuthorAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getConfigurationAccess().getAuthorAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__5__Impl"


    // $ANTLR start "rule__Configuration__Group__6"
    // InternalRestfulWebservice.g:872:1: rule__Configuration__Group__6 : rule__Configuration__Group__6__Impl rule__Configuration__Group__7 ;
    public final void rule__Configuration__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:876:1: ( rule__Configuration__Group__6__Impl rule__Configuration__Group__7 )
            // InternalRestfulWebservice.g:877:2: rule__Configuration__Group__6__Impl rule__Configuration__Group__7
            {
            pushFollow(FOLLOW_11);
            rule__Configuration__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Configuration__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__6"


    // $ANTLR start "rule__Configuration__Group__6__Impl"
    // InternalRestfulWebservice.g:884:1: rule__Configuration__Group__6__Impl : ( ( rule__Configuration__Author_emailAssignment_6 ) ) ;
    public final void rule__Configuration__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:888:1: ( ( ( rule__Configuration__Author_emailAssignment_6 ) ) )
            // InternalRestfulWebservice.g:889:1: ( ( rule__Configuration__Author_emailAssignment_6 ) )
            {
            // InternalRestfulWebservice.g:889:1: ( ( rule__Configuration__Author_emailAssignment_6 ) )
            // InternalRestfulWebservice.g:890:2: ( rule__Configuration__Author_emailAssignment_6 )
            {
             before(grammarAccess.getConfigurationAccess().getAuthor_emailAssignment_6()); 
            // InternalRestfulWebservice.g:891:2: ( rule__Configuration__Author_emailAssignment_6 )
            // InternalRestfulWebservice.g:891:3: rule__Configuration__Author_emailAssignment_6
            {
            pushFollow(FOLLOW_2);
            rule__Configuration__Author_emailAssignment_6();

            state._fsp--;


            }

             after(grammarAccess.getConfigurationAccess().getAuthor_emailAssignment_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__6__Impl"


    // $ANTLR start "rule__Configuration__Group__7"
    // InternalRestfulWebservice.g:899:1: rule__Configuration__Group__7 : rule__Configuration__Group__7__Impl rule__Configuration__Group__8 ;
    public final void rule__Configuration__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:903:1: ( rule__Configuration__Group__7__Impl rule__Configuration__Group__8 )
            // InternalRestfulWebservice.g:904:2: rule__Configuration__Group__7__Impl rule__Configuration__Group__8
            {
            pushFollow(FOLLOW_12);
            rule__Configuration__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Configuration__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__7"


    // $ANTLR start "rule__Configuration__Group__7__Impl"
    // InternalRestfulWebservice.g:911:1: rule__Configuration__Group__7__Impl : ( ( rule__Configuration__RepositoryAssignment_7 ) ) ;
    public final void rule__Configuration__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:915:1: ( ( ( rule__Configuration__RepositoryAssignment_7 ) ) )
            // InternalRestfulWebservice.g:916:1: ( ( rule__Configuration__RepositoryAssignment_7 ) )
            {
            // InternalRestfulWebservice.g:916:1: ( ( rule__Configuration__RepositoryAssignment_7 ) )
            // InternalRestfulWebservice.g:917:2: ( rule__Configuration__RepositoryAssignment_7 )
            {
             before(grammarAccess.getConfigurationAccess().getRepositoryAssignment_7()); 
            // InternalRestfulWebservice.g:918:2: ( rule__Configuration__RepositoryAssignment_7 )
            // InternalRestfulWebservice.g:918:3: rule__Configuration__RepositoryAssignment_7
            {
            pushFollow(FOLLOW_2);
            rule__Configuration__RepositoryAssignment_7();

            state._fsp--;


            }

             after(grammarAccess.getConfigurationAccess().getRepositoryAssignment_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__7__Impl"


    // $ANTLR start "rule__Configuration__Group__8"
    // InternalRestfulWebservice.g:926:1: rule__Configuration__Group__8 : rule__Configuration__Group__8__Impl ;
    public final void rule__Configuration__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:930:1: ( rule__Configuration__Group__8__Impl )
            // InternalRestfulWebservice.g:931:2: rule__Configuration__Group__8__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Configuration__Group__8__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__8"


    // $ANTLR start "rule__Configuration__Group__8__Impl"
    // InternalRestfulWebservice.g:937:1: rule__Configuration__Group__8__Impl : ( '}' ) ;
    public final void rule__Configuration__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:941:1: ( ( '}' ) )
            // InternalRestfulWebservice.g:942:1: ( '}' )
            {
            // InternalRestfulWebservice.g:942:1: ( '}' )
            // InternalRestfulWebservice.g:943:2: '}'
            {
             before(grammarAccess.getConfigurationAccess().getRightCurlyBracketKeyword_8()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getConfigurationAccess().getRightCurlyBracketKeyword_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__8__Impl"


    // $ANTLR start "rule__Author__Group__0"
    // InternalRestfulWebservice.g:953:1: rule__Author__Group__0 : rule__Author__Group__0__Impl rule__Author__Group__1 ;
    public final void rule__Author__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:957:1: ( rule__Author__Group__0__Impl rule__Author__Group__1 )
            // InternalRestfulWebservice.g:958:2: rule__Author__Group__0__Impl rule__Author__Group__1
            {
            pushFollow(FOLLOW_13);
            rule__Author__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Author__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Author__Group__0"


    // $ANTLR start "rule__Author__Group__0__Impl"
    // InternalRestfulWebservice.g:965:1: rule__Author__Group__0__Impl : ( 'author:' ) ;
    public final void rule__Author__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:969:1: ( ( 'author:' ) )
            // InternalRestfulWebservice.g:970:1: ( 'author:' )
            {
            // InternalRestfulWebservice.g:970:1: ( 'author:' )
            // InternalRestfulWebservice.g:971:2: 'author:'
            {
             before(grammarAccess.getAuthorAccess().getAuthorKeyword_0()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getAuthorAccess().getAuthorKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Author__Group__0__Impl"


    // $ANTLR start "rule__Author__Group__1"
    // InternalRestfulWebservice.g:980:1: rule__Author__Group__1 : rule__Author__Group__1__Impl ;
    public final void rule__Author__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:984:1: ( rule__Author__Group__1__Impl )
            // InternalRestfulWebservice.g:985:2: rule__Author__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Author__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Author__Group__1"


    // $ANTLR start "rule__Author__Group__1__Impl"
    // InternalRestfulWebservice.g:991:1: rule__Author__Group__1__Impl : ( ( rule__Author__NameAssignment_1 ) ) ;
    public final void rule__Author__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:995:1: ( ( ( rule__Author__NameAssignment_1 ) ) )
            // InternalRestfulWebservice.g:996:1: ( ( rule__Author__NameAssignment_1 ) )
            {
            // InternalRestfulWebservice.g:996:1: ( ( rule__Author__NameAssignment_1 ) )
            // InternalRestfulWebservice.g:997:2: ( rule__Author__NameAssignment_1 )
            {
             before(grammarAccess.getAuthorAccess().getNameAssignment_1()); 
            // InternalRestfulWebservice.g:998:2: ( rule__Author__NameAssignment_1 )
            // InternalRestfulWebservice.g:998:3: rule__Author__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Author__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getAuthorAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Author__Group__1__Impl"


    // $ANTLR start "rule__Author_Email__Group__0"
    // InternalRestfulWebservice.g:1007:1: rule__Author_Email__Group__0 : rule__Author_Email__Group__0__Impl rule__Author_Email__Group__1 ;
    public final void rule__Author_Email__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1011:1: ( rule__Author_Email__Group__0__Impl rule__Author_Email__Group__1 )
            // InternalRestfulWebservice.g:1012:2: rule__Author_Email__Group__0__Impl rule__Author_Email__Group__1
            {
            pushFollow(FOLLOW_13);
            rule__Author_Email__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Author_Email__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Author_Email__Group__0"


    // $ANTLR start "rule__Author_Email__Group__0__Impl"
    // InternalRestfulWebservice.g:1019:1: rule__Author_Email__Group__0__Impl : ( 'author_email:' ) ;
    public final void rule__Author_Email__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1023:1: ( ( 'author_email:' ) )
            // InternalRestfulWebservice.g:1024:1: ( 'author_email:' )
            {
            // InternalRestfulWebservice.g:1024:1: ( 'author_email:' )
            // InternalRestfulWebservice.g:1025:2: 'author_email:'
            {
             before(grammarAccess.getAuthor_EmailAccess().getAuthor_emailKeyword_0()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getAuthor_EmailAccess().getAuthor_emailKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Author_Email__Group__0__Impl"


    // $ANTLR start "rule__Author_Email__Group__1"
    // InternalRestfulWebservice.g:1034:1: rule__Author_Email__Group__1 : rule__Author_Email__Group__1__Impl ;
    public final void rule__Author_Email__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1038:1: ( rule__Author_Email__Group__1__Impl )
            // InternalRestfulWebservice.g:1039:2: rule__Author_Email__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Author_Email__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Author_Email__Group__1"


    // $ANTLR start "rule__Author_Email__Group__1__Impl"
    // InternalRestfulWebservice.g:1045:1: rule__Author_Email__Group__1__Impl : ( ( rule__Author_Email__NameAssignment_1 ) ) ;
    public final void rule__Author_Email__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1049:1: ( ( ( rule__Author_Email__NameAssignment_1 ) ) )
            // InternalRestfulWebservice.g:1050:1: ( ( rule__Author_Email__NameAssignment_1 ) )
            {
            // InternalRestfulWebservice.g:1050:1: ( ( rule__Author_Email__NameAssignment_1 ) )
            // InternalRestfulWebservice.g:1051:2: ( rule__Author_Email__NameAssignment_1 )
            {
             before(grammarAccess.getAuthor_EmailAccess().getNameAssignment_1()); 
            // InternalRestfulWebservice.g:1052:2: ( rule__Author_Email__NameAssignment_1 )
            // InternalRestfulWebservice.g:1052:3: rule__Author_Email__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Author_Email__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getAuthor_EmailAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Author_Email__Group__1__Impl"


    // $ANTLR start "rule__Repository__Group__0"
    // InternalRestfulWebservice.g:1061:1: rule__Repository__Group__0 : rule__Repository__Group__0__Impl rule__Repository__Group__1 ;
    public final void rule__Repository__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1065:1: ( rule__Repository__Group__0__Impl rule__Repository__Group__1 )
            // InternalRestfulWebservice.g:1066:2: rule__Repository__Group__0__Impl rule__Repository__Group__1
            {
            pushFollow(FOLLOW_13);
            rule__Repository__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Repository__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Repository__Group__0"


    // $ANTLR start "rule__Repository__Group__0__Impl"
    // InternalRestfulWebservice.g:1073:1: rule__Repository__Group__0__Impl : ( 'repository:' ) ;
    public final void rule__Repository__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1077:1: ( ( 'repository:' ) )
            // InternalRestfulWebservice.g:1078:1: ( 'repository:' )
            {
            // InternalRestfulWebservice.g:1078:1: ( 'repository:' )
            // InternalRestfulWebservice.g:1079:2: 'repository:'
            {
             before(grammarAccess.getRepositoryAccess().getRepositoryKeyword_0()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getRepositoryAccess().getRepositoryKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Repository__Group__0__Impl"


    // $ANTLR start "rule__Repository__Group__1"
    // InternalRestfulWebservice.g:1088:1: rule__Repository__Group__1 : rule__Repository__Group__1__Impl ;
    public final void rule__Repository__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1092:1: ( rule__Repository__Group__1__Impl )
            // InternalRestfulWebservice.g:1093:2: rule__Repository__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Repository__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Repository__Group__1"


    // $ANTLR start "rule__Repository__Group__1__Impl"
    // InternalRestfulWebservice.g:1099:1: rule__Repository__Group__1__Impl : ( ( rule__Repository__NameAssignment_1 ) ) ;
    public final void rule__Repository__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1103:1: ( ( ( rule__Repository__NameAssignment_1 ) ) )
            // InternalRestfulWebservice.g:1104:1: ( ( rule__Repository__NameAssignment_1 ) )
            {
            // InternalRestfulWebservice.g:1104:1: ( ( rule__Repository__NameAssignment_1 ) )
            // InternalRestfulWebservice.g:1105:2: ( rule__Repository__NameAssignment_1 )
            {
             before(grammarAccess.getRepositoryAccess().getNameAssignment_1()); 
            // InternalRestfulWebservice.g:1106:2: ( rule__Repository__NameAssignment_1 )
            // InternalRestfulWebservice.g:1106:3: rule__Repository__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Repository__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getRepositoryAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Repository__Group__1__Impl"


    // $ANTLR start "rule__Lib__Group__0"
    // InternalRestfulWebservice.g:1115:1: rule__Lib__Group__0 : rule__Lib__Group__0__Impl rule__Lib__Group__1 ;
    public final void rule__Lib__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1119:1: ( rule__Lib__Group__0__Impl rule__Lib__Group__1 )
            // InternalRestfulWebservice.g:1120:2: rule__Lib__Group__0__Impl rule__Lib__Group__1
            {
            pushFollow(FOLLOW_13);
            rule__Lib__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Lib__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lib__Group__0"


    // $ANTLR start "rule__Lib__Group__0__Impl"
    // InternalRestfulWebservice.g:1127:1: rule__Lib__Group__0__Impl : ( 'lib_name:' ) ;
    public final void rule__Lib__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1131:1: ( ( 'lib_name:' ) )
            // InternalRestfulWebservice.g:1132:1: ( 'lib_name:' )
            {
            // InternalRestfulWebservice.g:1132:1: ( 'lib_name:' )
            // InternalRestfulWebservice.g:1133:2: 'lib_name:'
            {
             before(grammarAccess.getLibAccess().getLib_nameKeyword_0()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getLibAccess().getLib_nameKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lib__Group__0__Impl"


    // $ANTLR start "rule__Lib__Group__1"
    // InternalRestfulWebservice.g:1142:1: rule__Lib__Group__1 : rule__Lib__Group__1__Impl ;
    public final void rule__Lib__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1146:1: ( rule__Lib__Group__1__Impl )
            // InternalRestfulWebservice.g:1147:2: rule__Lib__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Lib__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lib__Group__1"


    // $ANTLR start "rule__Lib__Group__1__Impl"
    // InternalRestfulWebservice.g:1153:1: rule__Lib__Group__1__Impl : ( ( rule__Lib__NameAssignment_1 ) ) ;
    public final void rule__Lib__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1157:1: ( ( ( rule__Lib__NameAssignment_1 ) ) )
            // InternalRestfulWebservice.g:1158:1: ( ( rule__Lib__NameAssignment_1 ) )
            {
            // InternalRestfulWebservice.g:1158:1: ( ( rule__Lib__NameAssignment_1 ) )
            // InternalRestfulWebservice.g:1159:2: ( rule__Lib__NameAssignment_1 )
            {
             before(grammarAccess.getLibAccess().getNameAssignment_1()); 
            // InternalRestfulWebservice.g:1160:2: ( rule__Lib__NameAssignment_1 )
            // InternalRestfulWebservice.g:1160:3: rule__Lib__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Lib__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getLibAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lib__Group__1__Impl"


    // $ANTLR start "rule__Software__Group__0"
    // InternalRestfulWebservice.g:1169:1: rule__Software__Group__0 : rule__Software__Group__0__Impl rule__Software__Group__1 ;
    public final void rule__Software__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1173:1: ( rule__Software__Group__0__Impl rule__Software__Group__1 )
            // InternalRestfulWebservice.g:1174:2: rule__Software__Group__0__Impl rule__Software__Group__1
            {
            pushFollow(FOLLOW_13);
            rule__Software__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Software__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Software__Group__0"


    // $ANTLR start "rule__Software__Group__0__Impl"
    // InternalRestfulWebservice.g:1181:1: rule__Software__Group__0__Impl : ( 'software:' ) ;
    public final void rule__Software__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1185:1: ( ( 'software:' ) )
            // InternalRestfulWebservice.g:1186:1: ( 'software:' )
            {
            // InternalRestfulWebservice.g:1186:1: ( 'software:' )
            // InternalRestfulWebservice.g:1187:2: 'software:'
            {
             before(grammarAccess.getSoftwareAccess().getSoftwareKeyword_0()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getSoftwareAccess().getSoftwareKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Software__Group__0__Impl"


    // $ANTLR start "rule__Software__Group__1"
    // InternalRestfulWebservice.g:1196:1: rule__Software__Group__1 : rule__Software__Group__1__Impl ;
    public final void rule__Software__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1200:1: ( rule__Software__Group__1__Impl )
            // InternalRestfulWebservice.g:1201:2: rule__Software__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Software__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Software__Group__1"


    // $ANTLR start "rule__Software__Group__1__Impl"
    // InternalRestfulWebservice.g:1207:1: rule__Software__Group__1__Impl : ( ( rule__Software__NameAssignment_1 ) ) ;
    public final void rule__Software__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1211:1: ( ( ( rule__Software__NameAssignment_1 ) ) )
            // InternalRestfulWebservice.g:1212:1: ( ( rule__Software__NameAssignment_1 ) )
            {
            // InternalRestfulWebservice.g:1212:1: ( ( rule__Software__NameAssignment_1 ) )
            // InternalRestfulWebservice.g:1213:2: ( rule__Software__NameAssignment_1 )
            {
             before(grammarAccess.getSoftwareAccess().getNameAssignment_1()); 
            // InternalRestfulWebservice.g:1214:2: ( rule__Software__NameAssignment_1 )
            // InternalRestfulWebservice.g:1214:3: rule__Software__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Software__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getSoftwareAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Software__Group__1__Impl"


    // $ANTLR start "rule__About__Group__0"
    // InternalRestfulWebservice.g:1223:1: rule__About__Group__0 : rule__About__Group__0__Impl rule__About__Group__1 ;
    public final void rule__About__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1227:1: ( rule__About__Group__0__Impl rule__About__Group__1 )
            // InternalRestfulWebservice.g:1228:2: rule__About__Group__0__Impl rule__About__Group__1
            {
            pushFollow(FOLLOW_13);
            rule__About__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__About__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__About__Group__0"


    // $ANTLR start "rule__About__Group__0__Impl"
    // InternalRestfulWebservice.g:1235:1: rule__About__Group__0__Impl : ( 'about:' ) ;
    public final void rule__About__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1239:1: ( ( 'about:' ) )
            // InternalRestfulWebservice.g:1240:1: ( 'about:' )
            {
            // InternalRestfulWebservice.g:1240:1: ( 'about:' )
            // InternalRestfulWebservice.g:1241:2: 'about:'
            {
             before(grammarAccess.getAboutAccess().getAboutKeyword_0()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getAboutAccess().getAboutKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__About__Group__0__Impl"


    // $ANTLR start "rule__About__Group__1"
    // InternalRestfulWebservice.g:1250:1: rule__About__Group__1 : rule__About__Group__1__Impl ;
    public final void rule__About__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1254:1: ( rule__About__Group__1__Impl )
            // InternalRestfulWebservice.g:1255:2: rule__About__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__About__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__About__Group__1"


    // $ANTLR start "rule__About__Group__1__Impl"
    // InternalRestfulWebservice.g:1261:1: rule__About__Group__1__Impl : ( ( rule__About__NameAssignment_1 ) ) ;
    public final void rule__About__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1265:1: ( ( ( rule__About__NameAssignment_1 ) ) )
            // InternalRestfulWebservice.g:1266:1: ( ( rule__About__NameAssignment_1 ) )
            {
            // InternalRestfulWebservice.g:1266:1: ( ( rule__About__NameAssignment_1 ) )
            // InternalRestfulWebservice.g:1267:2: ( rule__About__NameAssignment_1 )
            {
             before(grammarAccess.getAboutAccess().getNameAssignment_1()); 
            // InternalRestfulWebservice.g:1268:2: ( rule__About__NameAssignment_1 )
            // InternalRestfulWebservice.g:1268:3: rule__About__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__About__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getAboutAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__About__Group__1__Impl"


    // $ANTLR start "rule__Description__Group__0"
    // InternalRestfulWebservice.g:1277:1: rule__Description__Group__0 : rule__Description__Group__0__Impl rule__Description__Group__1 ;
    public final void rule__Description__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1281:1: ( rule__Description__Group__0__Impl rule__Description__Group__1 )
            // InternalRestfulWebservice.g:1282:2: rule__Description__Group__0__Impl rule__Description__Group__1
            {
            pushFollow(FOLLOW_13);
            rule__Description__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Description__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Description__Group__0"


    // $ANTLR start "rule__Description__Group__0__Impl"
    // InternalRestfulWebservice.g:1289:1: rule__Description__Group__0__Impl : ( '#' ) ;
    public final void rule__Description__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1293:1: ( ( '#' ) )
            // InternalRestfulWebservice.g:1294:1: ( '#' )
            {
            // InternalRestfulWebservice.g:1294:1: ( '#' )
            // InternalRestfulWebservice.g:1295:2: '#'
            {
             before(grammarAccess.getDescriptionAccess().getNumberSignKeyword_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getDescriptionAccess().getNumberSignKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Description__Group__0__Impl"


    // $ANTLR start "rule__Description__Group__1"
    // InternalRestfulWebservice.g:1304:1: rule__Description__Group__1 : rule__Description__Group__1__Impl ;
    public final void rule__Description__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1308:1: ( rule__Description__Group__1__Impl )
            // InternalRestfulWebservice.g:1309:2: rule__Description__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Description__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Description__Group__1"


    // $ANTLR start "rule__Description__Group__1__Impl"
    // InternalRestfulWebservice.g:1315:1: rule__Description__Group__1__Impl : ( ( rule__Description__TextfieldAssignment_1 ) ) ;
    public final void rule__Description__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1319:1: ( ( ( rule__Description__TextfieldAssignment_1 ) ) )
            // InternalRestfulWebservice.g:1320:1: ( ( rule__Description__TextfieldAssignment_1 ) )
            {
            // InternalRestfulWebservice.g:1320:1: ( ( rule__Description__TextfieldAssignment_1 ) )
            // InternalRestfulWebservice.g:1321:2: ( rule__Description__TextfieldAssignment_1 )
            {
             before(grammarAccess.getDescriptionAccess().getTextfieldAssignment_1()); 
            // InternalRestfulWebservice.g:1322:2: ( rule__Description__TextfieldAssignment_1 )
            // InternalRestfulWebservice.g:1322:3: rule__Description__TextfieldAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Description__TextfieldAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getDescriptionAccess().getTextfieldAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Description__Group__1__Impl"


    // $ANTLR start "rule__Entity_Type__Group__0"
    // InternalRestfulWebservice.g:1331:1: rule__Entity_Type__Group__0 : rule__Entity_Type__Group__0__Impl rule__Entity_Type__Group__1 ;
    public final void rule__Entity_Type__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1335:1: ( rule__Entity_Type__Group__0__Impl rule__Entity_Type__Group__1 )
            // InternalRestfulWebservice.g:1336:2: rule__Entity_Type__Group__0__Impl rule__Entity_Type__Group__1
            {
            pushFollow(FOLLOW_13);
            rule__Entity_Type__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Entity_Type__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity_Type__Group__0"


    // $ANTLR start "rule__Entity_Type__Group__0__Impl"
    // InternalRestfulWebservice.g:1343:1: rule__Entity_Type__Group__0__Impl : ( 'is:' ) ;
    public final void rule__Entity_Type__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1347:1: ( ( 'is:' ) )
            // InternalRestfulWebservice.g:1348:1: ( 'is:' )
            {
            // InternalRestfulWebservice.g:1348:1: ( 'is:' )
            // InternalRestfulWebservice.g:1349:2: 'is:'
            {
             before(grammarAccess.getEntity_TypeAccess().getIsKeyword_0()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getEntity_TypeAccess().getIsKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity_Type__Group__0__Impl"


    // $ANTLR start "rule__Entity_Type__Group__1"
    // InternalRestfulWebservice.g:1358:1: rule__Entity_Type__Group__1 : rule__Entity_Type__Group__1__Impl ;
    public final void rule__Entity_Type__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1362:1: ( rule__Entity_Type__Group__1__Impl )
            // InternalRestfulWebservice.g:1363:2: rule__Entity_Type__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Entity_Type__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity_Type__Group__1"


    // $ANTLR start "rule__Entity_Type__Group__1__Impl"
    // InternalRestfulWebservice.g:1369:1: rule__Entity_Type__Group__1__Impl : ( ( rule__Entity_Type__NameAssignment_1 ) ) ;
    public final void rule__Entity_Type__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1373:1: ( ( ( rule__Entity_Type__NameAssignment_1 ) ) )
            // InternalRestfulWebservice.g:1374:1: ( ( rule__Entity_Type__NameAssignment_1 ) )
            {
            // InternalRestfulWebservice.g:1374:1: ( ( rule__Entity_Type__NameAssignment_1 ) )
            // InternalRestfulWebservice.g:1375:2: ( rule__Entity_Type__NameAssignment_1 )
            {
             before(grammarAccess.getEntity_TypeAccess().getNameAssignment_1()); 
            // InternalRestfulWebservice.g:1376:2: ( rule__Entity_Type__NameAssignment_1 )
            // InternalRestfulWebservice.g:1376:3: rule__Entity_Type__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Entity_Type__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getEntity_TypeAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity_Type__Group__1__Impl"


    // $ANTLR start "rule__Module__Group__0"
    // InternalRestfulWebservice.g:1385:1: rule__Module__Group__0 : rule__Module__Group__0__Impl rule__Module__Group__1 ;
    public final void rule__Module__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1389:1: ( rule__Module__Group__0__Impl rule__Module__Group__1 )
            // InternalRestfulWebservice.g:1390:2: rule__Module__Group__0__Impl rule__Module__Group__1
            {
            pushFollow(FOLLOW_14);
            rule__Module__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Module__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__Group__0"


    // $ANTLR start "rule__Module__Group__0__Impl"
    // InternalRestfulWebservice.g:1397:1: rule__Module__Group__0__Impl : ( ( rule__Module__DescriptionAssignment_0 )? ) ;
    public final void rule__Module__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1401:1: ( ( ( rule__Module__DescriptionAssignment_0 )? ) )
            // InternalRestfulWebservice.g:1402:1: ( ( rule__Module__DescriptionAssignment_0 )? )
            {
            // InternalRestfulWebservice.g:1402:1: ( ( rule__Module__DescriptionAssignment_0 )? )
            // InternalRestfulWebservice.g:1403:2: ( rule__Module__DescriptionAssignment_0 )?
            {
             before(grammarAccess.getModuleAccess().getDescriptionAssignment_0()); 
            // InternalRestfulWebservice.g:1404:2: ( rule__Module__DescriptionAssignment_0 )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==20) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // InternalRestfulWebservice.g:1404:3: rule__Module__DescriptionAssignment_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Module__DescriptionAssignment_0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getModuleAccess().getDescriptionAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__Group__0__Impl"


    // $ANTLR start "rule__Module__Group__1"
    // InternalRestfulWebservice.g:1412:1: rule__Module__Group__1 : rule__Module__Group__1__Impl rule__Module__Group__2 ;
    public final void rule__Module__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1416:1: ( rule__Module__Group__1__Impl rule__Module__Group__2 )
            // InternalRestfulWebservice.g:1417:2: rule__Module__Group__1__Impl rule__Module__Group__2
            {
            pushFollow(FOLLOW_15);
            rule__Module__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Module__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__Group__1"


    // $ANTLR start "rule__Module__Group__1__Impl"
    // InternalRestfulWebservice.g:1424:1: rule__Module__Group__1__Impl : ( 'module' ) ;
    public final void rule__Module__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1428:1: ( ( 'module' ) )
            // InternalRestfulWebservice.g:1429:1: ( 'module' )
            {
            // InternalRestfulWebservice.g:1429:1: ( 'module' )
            // InternalRestfulWebservice.g:1430:2: 'module'
            {
             before(grammarAccess.getModuleAccess().getModuleKeyword_1()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getModuleAccess().getModuleKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__Group__1__Impl"


    // $ANTLR start "rule__Module__Group__2"
    // InternalRestfulWebservice.g:1439:1: rule__Module__Group__2 : rule__Module__Group__2__Impl rule__Module__Group__3 ;
    public final void rule__Module__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1443:1: ( rule__Module__Group__2__Impl rule__Module__Group__3 )
            // InternalRestfulWebservice.g:1444:2: rule__Module__Group__2__Impl rule__Module__Group__3
            {
            pushFollow(FOLLOW_5);
            rule__Module__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Module__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__Group__2"


    // $ANTLR start "rule__Module__Group__2__Impl"
    // InternalRestfulWebservice.g:1451:1: rule__Module__Group__2__Impl : ( ( rule__Module__NameAssignment_2 ) ) ;
    public final void rule__Module__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1455:1: ( ( ( rule__Module__NameAssignment_2 ) ) )
            // InternalRestfulWebservice.g:1456:1: ( ( rule__Module__NameAssignment_2 ) )
            {
            // InternalRestfulWebservice.g:1456:1: ( ( rule__Module__NameAssignment_2 ) )
            // InternalRestfulWebservice.g:1457:2: ( rule__Module__NameAssignment_2 )
            {
             before(grammarAccess.getModuleAccess().getNameAssignment_2()); 
            // InternalRestfulWebservice.g:1458:2: ( rule__Module__NameAssignment_2 )
            // InternalRestfulWebservice.g:1458:3: rule__Module__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Module__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getModuleAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__Group__2__Impl"


    // $ANTLR start "rule__Module__Group__3"
    // InternalRestfulWebservice.g:1466:1: rule__Module__Group__3 : rule__Module__Group__3__Impl rule__Module__Group__4 ;
    public final void rule__Module__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1470:1: ( rule__Module__Group__3__Impl rule__Module__Group__4 )
            // InternalRestfulWebservice.g:1471:2: rule__Module__Group__3__Impl rule__Module__Group__4
            {
            pushFollow(FOLLOW_16);
            rule__Module__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Module__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__Group__3"


    // $ANTLR start "rule__Module__Group__3__Impl"
    // InternalRestfulWebservice.g:1478:1: rule__Module__Group__3__Impl : ( '{' ) ;
    public final void rule__Module__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1482:1: ( ( '{' ) )
            // InternalRestfulWebservice.g:1483:1: ( '{' )
            {
            // InternalRestfulWebservice.g:1483:1: ( '{' )
            // InternalRestfulWebservice.g:1484:2: '{'
            {
             before(grammarAccess.getModuleAccess().getLeftCurlyBracketKeyword_3()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getModuleAccess().getLeftCurlyBracketKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__Group__3__Impl"


    // $ANTLR start "rule__Module__Group__4"
    // InternalRestfulWebservice.g:1493:1: rule__Module__Group__4 : rule__Module__Group__4__Impl rule__Module__Group__5 ;
    public final void rule__Module__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1497:1: ( rule__Module__Group__4__Impl rule__Module__Group__5 )
            // InternalRestfulWebservice.g:1498:2: rule__Module__Group__4__Impl rule__Module__Group__5
            {
            pushFollow(FOLLOW_16);
            rule__Module__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Module__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__Group__4"


    // $ANTLR start "rule__Module__Group__4__Impl"
    // InternalRestfulWebservice.g:1505:1: rule__Module__Group__4__Impl : ( ( rule__Module__ElementsAssignment_4 )* ) ;
    public final void rule__Module__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1509:1: ( ( ( rule__Module__ElementsAssignment_4 )* ) )
            // InternalRestfulWebservice.g:1510:1: ( ( rule__Module__ElementsAssignment_4 )* )
            {
            // InternalRestfulWebservice.g:1510:1: ( ( rule__Module__ElementsAssignment_4 )* )
            // InternalRestfulWebservice.g:1511:2: ( rule__Module__ElementsAssignment_4 )*
            {
             before(grammarAccess.getModuleAccess().getElementsAssignment_4()); 
            // InternalRestfulWebservice.g:1512:2: ( rule__Module__ElementsAssignment_4 )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==20||LA6_0==22||LA6_0==24||LA6_0==26) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalRestfulWebservice.g:1512:3: rule__Module__ElementsAssignment_4
            	    {
            	    pushFollow(FOLLOW_4);
            	    rule__Module__ElementsAssignment_4();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

             after(grammarAccess.getModuleAccess().getElementsAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__Group__4__Impl"


    // $ANTLR start "rule__Module__Group__5"
    // InternalRestfulWebservice.g:1520:1: rule__Module__Group__5 : rule__Module__Group__5__Impl ;
    public final void rule__Module__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1524:1: ( rule__Module__Group__5__Impl )
            // InternalRestfulWebservice.g:1525:2: rule__Module__Group__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Module__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__Group__5"


    // $ANTLR start "rule__Module__Group__5__Impl"
    // InternalRestfulWebservice.g:1531:1: rule__Module__Group__5__Impl : ( '}' ) ;
    public final void rule__Module__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1535:1: ( ( '}' ) )
            // InternalRestfulWebservice.g:1536:1: ( '}' )
            {
            // InternalRestfulWebservice.g:1536:1: ( '}' )
            // InternalRestfulWebservice.g:1537:2: '}'
            {
             before(grammarAccess.getModuleAccess().getRightCurlyBracketKeyword_5()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getModuleAccess().getRightCurlyBracketKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__Group__5__Impl"


    // $ANTLR start "rule__QualifiedName__Group__0"
    // InternalRestfulWebservice.g:1547:1: rule__QualifiedName__Group__0 : rule__QualifiedName__Group__0__Impl rule__QualifiedName__Group__1 ;
    public final void rule__QualifiedName__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1551:1: ( rule__QualifiedName__Group__0__Impl rule__QualifiedName__Group__1 )
            // InternalRestfulWebservice.g:1552:2: rule__QualifiedName__Group__0__Impl rule__QualifiedName__Group__1
            {
            pushFollow(FOLLOW_17);
            rule__QualifiedName__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__0"


    // $ANTLR start "rule__QualifiedName__Group__0__Impl"
    // InternalRestfulWebservice.g:1559:1: rule__QualifiedName__Group__0__Impl : ( RULE_ID ) ;
    public final void rule__QualifiedName__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1563:1: ( ( RULE_ID ) )
            // InternalRestfulWebservice.g:1564:1: ( RULE_ID )
            {
            // InternalRestfulWebservice.g:1564:1: ( RULE_ID )
            // InternalRestfulWebservice.g:1565:2: RULE_ID
            {
             before(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__0__Impl"


    // $ANTLR start "rule__QualifiedName__Group__1"
    // InternalRestfulWebservice.g:1574:1: rule__QualifiedName__Group__1 : rule__QualifiedName__Group__1__Impl ;
    public final void rule__QualifiedName__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1578:1: ( rule__QualifiedName__Group__1__Impl )
            // InternalRestfulWebservice.g:1579:2: rule__QualifiedName__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__1"


    // $ANTLR start "rule__QualifiedName__Group__1__Impl"
    // InternalRestfulWebservice.g:1585:1: rule__QualifiedName__Group__1__Impl : ( ( rule__QualifiedName__Group_1__0 )* ) ;
    public final void rule__QualifiedName__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1589:1: ( ( ( rule__QualifiedName__Group_1__0 )* ) )
            // InternalRestfulWebservice.g:1590:1: ( ( rule__QualifiedName__Group_1__0 )* )
            {
            // InternalRestfulWebservice.g:1590:1: ( ( rule__QualifiedName__Group_1__0 )* )
            // InternalRestfulWebservice.g:1591:2: ( rule__QualifiedName__Group_1__0 )*
            {
             before(grammarAccess.getQualifiedNameAccess().getGroup_1()); 
            // InternalRestfulWebservice.g:1592:2: ( rule__QualifiedName__Group_1__0 )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==23) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // InternalRestfulWebservice.g:1592:3: rule__QualifiedName__Group_1__0
            	    {
            	    pushFollow(FOLLOW_18);
            	    rule__QualifiedName__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

             after(grammarAccess.getQualifiedNameAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__1__Impl"


    // $ANTLR start "rule__QualifiedName__Group_1__0"
    // InternalRestfulWebservice.g:1601:1: rule__QualifiedName__Group_1__0 : rule__QualifiedName__Group_1__0__Impl rule__QualifiedName__Group_1__1 ;
    public final void rule__QualifiedName__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1605:1: ( rule__QualifiedName__Group_1__0__Impl rule__QualifiedName__Group_1__1 )
            // InternalRestfulWebservice.g:1606:2: rule__QualifiedName__Group_1__0__Impl rule__QualifiedName__Group_1__1
            {
            pushFollow(FOLLOW_15);
            rule__QualifiedName__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__0"


    // $ANTLR start "rule__QualifiedName__Group_1__0__Impl"
    // InternalRestfulWebservice.g:1613:1: rule__QualifiedName__Group_1__0__Impl : ( '.' ) ;
    public final void rule__QualifiedName__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1617:1: ( ( '.' ) )
            // InternalRestfulWebservice.g:1618:1: ( '.' )
            {
            // InternalRestfulWebservice.g:1618:1: ( '.' )
            // InternalRestfulWebservice.g:1619:2: '.'
            {
             before(grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__0__Impl"


    // $ANTLR start "rule__QualifiedName__Group_1__1"
    // InternalRestfulWebservice.g:1628:1: rule__QualifiedName__Group_1__1 : rule__QualifiedName__Group_1__1__Impl ;
    public final void rule__QualifiedName__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1632:1: ( rule__QualifiedName__Group_1__1__Impl )
            // InternalRestfulWebservice.g:1633:2: rule__QualifiedName__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__1"


    // $ANTLR start "rule__QualifiedName__Group_1__1__Impl"
    // InternalRestfulWebservice.g:1639:1: rule__QualifiedName__Group_1__1__Impl : ( RULE_ID ) ;
    public final void rule__QualifiedName__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1643:1: ( ( RULE_ID ) )
            // InternalRestfulWebservice.g:1644:1: ( RULE_ID )
            {
            // InternalRestfulWebservice.g:1644:1: ( RULE_ID )
            // InternalRestfulWebservice.g:1645:2: RULE_ID
            {
             before(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__1__Impl"


    // $ANTLR start "rule__Import__Group__0"
    // InternalRestfulWebservice.g:1655:1: rule__Import__Group__0 : rule__Import__Group__0__Impl rule__Import__Group__1 ;
    public final void rule__Import__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1659:1: ( rule__Import__Group__0__Impl rule__Import__Group__1 )
            // InternalRestfulWebservice.g:1660:2: rule__Import__Group__0__Impl rule__Import__Group__1
            {
            pushFollow(FOLLOW_15);
            rule__Import__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Import__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__0"


    // $ANTLR start "rule__Import__Group__0__Impl"
    // InternalRestfulWebservice.g:1667:1: rule__Import__Group__0__Impl : ( 'import' ) ;
    public final void rule__Import__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1671:1: ( ( 'import' ) )
            // InternalRestfulWebservice.g:1672:1: ( 'import' )
            {
            // InternalRestfulWebservice.g:1672:1: ( 'import' )
            // InternalRestfulWebservice.g:1673:2: 'import'
            {
             before(grammarAccess.getImportAccess().getImportKeyword_0()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getImportAccess().getImportKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__0__Impl"


    // $ANTLR start "rule__Import__Group__1"
    // InternalRestfulWebservice.g:1682:1: rule__Import__Group__1 : rule__Import__Group__1__Impl ;
    public final void rule__Import__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1686:1: ( rule__Import__Group__1__Impl )
            // InternalRestfulWebservice.g:1687:2: rule__Import__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Import__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__1"


    // $ANTLR start "rule__Import__Group__1__Impl"
    // InternalRestfulWebservice.g:1693:1: rule__Import__Group__1__Impl : ( ( rule__Import__ImportedNamespaceAssignment_1 ) ) ;
    public final void rule__Import__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1697:1: ( ( ( rule__Import__ImportedNamespaceAssignment_1 ) ) )
            // InternalRestfulWebservice.g:1698:1: ( ( rule__Import__ImportedNamespaceAssignment_1 ) )
            {
            // InternalRestfulWebservice.g:1698:1: ( ( rule__Import__ImportedNamespaceAssignment_1 ) )
            // InternalRestfulWebservice.g:1699:2: ( rule__Import__ImportedNamespaceAssignment_1 )
            {
             before(grammarAccess.getImportAccess().getImportedNamespaceAssignment_1()); 
            // InternalRestfulWebservice.g:1700:2: ( rule__Import__ImportedNamespaceAssignment_1 )
            // InternalRestfulWebservice.g:1700:3: rule__Import__ImportedNamespaceAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Import__ImportedNamespaceAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getImportAccess().getImportedNamespaceAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__1__Impl"


    // $ANTLR start "rule__QualifiedNameWithWildcard__Group__0"
    // InternalRestfulWebservice.g:1709:1: rule__QualifiedNameWithWildcard__Group__0 : rule__QualifiedNameWithWildcard__Group__0__Impl rule__QualifiedNameWithWildcard__Group__1 ;
    public final void rule__QualifiedNameWithWildcard__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1713:1: ( rule__QualifiedNameWithWildcard__Group__0__Impl rule__QualifiedNameWithWildcard__Group__1 )
            // InternalRestfulWebservice.g:1714:2: rule__QualifiedNameWithWildcard__Group__0__Impl rule__QualifiedNameWithWildcard__Group__1
            {
            pushFollow(FOLLOW_19);
            rule__QualifiedNameWithWildcard__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QualifiedNameWithWildcard__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedNameWithWildcard__Group__0"


    // $ANTLR start "rule__QualifiedNameWithWildcard__Group__0__Impl"
    // InternalRestfulWebservice.g:1721:1: rule__QualifiedNameWithWildcard__Group__0__Impl : ( ruleQualifiedName ) ;
    public final void rule__QualifiedNameWithWildcard__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1725:1: ( ( ruleQualifiedName ) )
            // InternalRestfulWebservice.g:1726:1: ( ruleQualifiedName )
            {
            // InternalRestfulWebservice.g:1726:1: ( ruleQualifiedName )
            // InternalRestfulWebservice.g:1727:2: ruleQualifiedName
            {
             before(grammarAccess.getQualifiedNameWithWildcardAccess().getQualifiedNameParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getQualifiedNameWithWildcardAccess().getQualifiedNameParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedNameWithWildcard__Group__0__Impl"


    // $ANTLR start "rule__QualifiedNameWithWildcard__Group__1"
    // InternalRestfulWebservice.g:1736:1: rule__QualifiedNameWithWildcard__Group__1 : rule__QualifiedNameWithWildcard__Group__1__Impl ;
    public final void rule__QualifiedNameWithWildcard__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1740:1: ( rule__QualifiedNameWithWildcard__Group__1__Impl )
            // InternalRestfulWebservice.g:1741:2: rule__QualifiedNameWithWildcard__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QualifiedNameWithWildcard__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedNameWithWildcard__Group__1"


    // $ANTLR start "rule__QualifiedNameWithWildcard__Group__1__Impl"
    // InternalRestfulWebservice.g:1747:1: rule__QualifiedNameWithWildcard__Group__1__Impl : ( ( '.*' )? ) ;
    public final void rule__QualifiedNameWithWildcard__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1751:1: ( ( ( '.*' )? ) )
            // InternalRestfulWebservice.g:1752:1: ( ( '.*' )? )
            {
            // InternalRestfulWebservice.g:1752:1: ( ( '.*' )? )
            // InternalRestfulWebservice.g:1753:2: ( '.*' )?
            {
             before(grammarAccess.getQualifiedNameWithWildcardAccess().getFullStopAsteriskKeyword_1()); 
            // InternalRestfulWebservice.g:1754:2: ( '.*' )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==25) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // InternalRestfulWebservice.g:1754:3: '.*'
                    {
                    match(input,25,FOLLOW_2); 

                    }
                    break;

            }

             after(grammarAccess.getQualifiedNameWithWildcardAccess().getFullStopAsteriskKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedNameWithWildcard__Group__1__Impl"


    // $ANTLR start "rule__Entity__Group__0"
    // InternalRestfulWebservice.g:1763:1: rule__Entity__Group__0 : rule__Entity__Group__0__Impl rule__Entity__Group__1 ;
    public final void rule__Entity__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1767:1: ( rule__Entity__Group__0__Impl rule__Entity__Group__1 )
            // InternalRestfulWebservice.g:1768:2: rule__Entity__Group__0__Impl rule__Entity__Group__1
            {
            pushFollow(FOLLOW_20);
            rule__Entity__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Entity__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__0"


    // $ANTLR start "rule__Entity__Group__0__Impl"
    // InternalRestfulWebservice.g:1775:1: rule__Entity__Group__0__Impl : ( ( rule__Entity__DescriptionAssignment_0 )? ) ;
    public final void rule__Entity__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1779:1: ( ( ( rule__Entity__DescriptionAssignment_0 )? ) )
            // InternalRestfulWebservice.g:1780:1: ( ( rule__Entity__DescriptionAssignment_0 )? )
            {
            // InternalRestfulWebservice.g:1780:1: ( ( rule__Entity__DescriptionAssignment_0 )? )
            // InternalRestfulWebservice.g:1781:2: ( rule__Entity__DescriptionAssignment_0 )?
            {
             before(grammarAccess.getEntityAccess().getDescriptionAssignment_0()); 
            // InternalRestfulWebservice.g:1782:2: ( rule__Entity__DescriptionAssignment_0 )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==20) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // InternalRestfulWebservice.g:1782:3: rule__Entity__DescriptionAssignment_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Entity__DescriptionAssignment_0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getEntityAccess().getDescriptionAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__0__Impl"


    // $ANTLR start "rule__Entity__Group__1"
    // InternalRestfulWebservice.g:1790:1: rule__Entity__Group__1 : rule__Entity__Group__1__Impl rule__Entity__Group__2 ;
    public final void rule__Entity__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1794:1: ( rule__Entity__Group__1__Impl rule__Entity__Group__2 )
            // InternalRestfulWebservice.g:1795:2: rule__Entity__Group__1__Impl rule__Entity__Group__2
            {
            pushFollow(FOLLOW_15);
            rule__Entity__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Entity__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__1"


    // $ANTLR start "rule__Entity__Group__1__Impl"
    // InternalRestfulWebservice.g:1802:1: rule__Entity__Group__1__Impl : ( 'entity' ) ;
    public final void rule__Entity__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1806:1: ( ( 'entity' ) )
            // InternalRestfulWebservice.g:1807:1: ( 'entity' )
            {
            // InternalRestfulWebservice.g:1807:1: ( 'entity' )
            // InternalRestfulWebservice.g:1808:2: 'entity'
            {
             before(grammarAccess.getEntityAccess().getEntityKeyword_1()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getEntityAccess().getEntityKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__1__Impl"


    // $ANTLR start "rule__Entity__Group__2"
    // InternalRestfulWebservice.g:1817:1: rule__Entity__Group__2 : rule__Entity__Group__2__Impl rule__Entity__Group__3 ;
    public final void rule__Entity__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1821:1: ( rule__Entity__Group__2__Impl rule__Entity__Group__3 )
            // InternalRestfulWebservice.g:1822:2: rule__Entity__Group__2__Impl rule__Entity__Group__3
            {
            pushFollow(FOLLOW_21);
            rule__Entity__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Entity__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__2"


    // $ANTLR start "rule__Entity__Group__2__Impl"
    // InternalRestfulWebservice.g:1829:1: rule__Entity__Group__2__Impl : ( ( rule__Entity__NameAssignment_2 ) ) ;
    public final void rule__Entity__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1833:1: ( ( ( rule__Entity__NameAssignment_2 ) ) )
            // InternalRestfulWebservice.g:1834:1: ( ( rule__Entity__NameAssignment_2 ) )
            {
            // InternalRestfulWebservice.g:1834:1: ( ( rule__Entity__NameAssignment_2 ) )
            // InternalRestfulWebservice.g:1835:2: ( rule__Entity__NameAssignment_2 )
            {
             before(grammarAccess.getEntityAccess().getNameAssignment_2()); 
            // InternalRestfulWebservice.g:1836:2: ( rule__Entity__NameAssignment_2 )
            // InternalRestfulWebservice.g:1836:3: rule__Entity__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Entity__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getEntityAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__2__Impl"


    // $ANTLR start "rule__Entity__Group__3"
    // InternalRestfulWebservice.g:1844:1: rule__Entity__Group__3 : rule__Entity__Group__3__Impl rule__Entity__Group__4 ;
    public final void rule__Entity__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1848:1: ( rule__Entity__Group__3__Impl rule__Entity__Group__4 )
            // InternalRestfulWebservice.g:1849:2: rule__Entity__Group__3__Impl rule__Entity__Group__4
            {
            pushFollow(FOLLOW_21);
            rule__Entity__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Entity__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__3"


    // $ANTLR start "rule__Entity__Group__3__Impl"
    // InternalRestfulWebservice.g:1856:1: rule__Entity__Group__3__Impl : ( ( rule__Entity__Group_3__0 )? ) ;
    public final void rule__Entity__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1860:1: ( ( ( rule__Entity__Group_3__0 )? ) )
            // InternalRestfulWebservice.g:1861:1: ( ( rule__Entity__Group_3__0 )? )
            {
            // InternalRestfulWebservice.g:1861:1: ( ( rule__Entity__Group_3__0 )? )
            // InternalRestfulWebservice.g:1862:2: ( rule__Entity__Group_3__0 )?
            {
             before(grammarAccess.getEntityAccess().getGroup_3()); 
            // InternalRestfulWebservice.g:1863:2: ( rule__Entity__Group_3__0 )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==27) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // InternalRestfulWebservice.g:1863:3: rule__Entity__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Entity__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getEntityAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__3__Impl"


    // $ANTLR start "rule__Entity__Group__4"
    // InternalRestfulWebservice.g:1871:1: rule__Entity__Group__4 : rule__Entity__Group__4__Impl rule__Entity__Group__5 ;
    public final void rule__Entity__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1875:1: ( rule__Entity__Group__4__Impl rule__Entity__Group__5 )
            // InternalRestfulWebservice.g:1876:2: rule__Entity__Group__4__Impl rule__Entity__Group__5
            {
            pushFollow(FOLLOW_22);
            rule__Entity__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Entity__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__4"


    // $ANTLR start "rule__Entity__Group__4__Impl"
    // InternalRestfulWebservice.g:1883:1: rule__Entity__Group__4__Impl : ( '{' ) ;
    public final void rule__Entity__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1887:1: ( ( '{' ) )
            // InternalRestfulWebservice.g:1888:1: ( '{' )
            {
            // InternalRestfulWebservice.g:1888:1: ( '{' )
            // InternalRestfulWebservice.g:1889:2: '{'
            {
             before(grammarAccess.getEntityAccess().getLeftCurlyBracketKeyword_4()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getEntityAccess().getLeftCurlyBracketKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__4__Impl"


    // $ANTLR start "rule__Entity__Group__5"
    // InternalRestfulWebservice.g:1898:1: rule__Entity__Group__5 : rule__Entity__Group__5__Impl rule__Entity__Group__6 ;
    public final void rule__Entity__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1902:1: ( rule__Entity__Group__5__Impl rule__Entity__Group__6 )
            // InternalRestfulWebservice.g:1903:2: rule__Entity__Group__5__Impl rule__Entity__Group__6
            {
            pushFollow(FOLLOW_23);
            rule__Entity__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Entity__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__5"


    // $ANTLR start "rule__Entity__Group__5__Impl"
    // InternalRestfulWebservice.g:1910:1: rule__Entity__Group__5__Impl : ( ( rule__Entity__Entity_typeAssignment_5 ) ) ;
    public final void rule__Entity__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1914:1: ( ( ( rule__Entity__Entity_typeAssignment_5 ) ) )
            // InternalRestfulWebservice.g:1915:1: ( ( rule__Entity__Entity_typeAssignment_5 ) )
            {
            // InternalRestfulWebservice.g:1915:1: ( ( rule__Entity__Entity_typeAssignment_5 ) )
            // InternalRestfulWebservice.g:1916:2: ( rule__Entity__Entity_typeAssignment_5 )
            {
             before(grammarAccess.getEntityAccess().getEntity_typeAssignment_5()); 
            // InternalRestfulWebservice.g:1917:2: ( rule__Entity__Entity_typeAssignment_5 )
            // InternalRestfulWebservice.g:1917:3: rule__Entity__Entity_typeAssignment_5
            {
            pushFollow(FOLLOW_2);
            rule__Entity__Entity_typeAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getEntityAccess().getEntity_typeAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__5__Impl"


    // $ANTLR start "rule__Entity__Group__6"
    // InternalRestfulWebservice.g:1925:1: rule__Entity__Group__6 : rule__Entity__Group__6__Impl rule__Entity__Group__7 ;
    public final void rule__Entity__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1929:1: ( rule__Entity__Group__6__Impl rule__Entity__Group__7 )
            // InternalRestfulWebservice.g:1930:2: rule__Entity__Group__6__Impl rule__Entity__Group__7
            {
            pushFollow(FOLLOW_23);
            rule__Entity__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Entity__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__6"


    // $ANTLR start "rule__Entity__Group__6__Impl"
    // InternalRestfulWebservice.g:1937:1: rule__Entity__Group__6__Impl : ( ( rule__Entity__AttributesAssignment_6 )* ) ;
    public final void rule__Entity__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1941:1: ( ( ( rule__Entity__AttributesAssignment_6 )* ) )
            // InternalRestfulWebservice.g:1942:1: ( ( rule__Entity__AttributesAssignment_6 )* )
            {
            // InternalRestfulWebservice.g:1942:1: ( ( rule__Entity__AttributesAssignment_6 )* )
            // InternalRestfulWebservice.g:1943:2: ( rule__Entity__AttributesAssignment_6 )*
            {
             before(grammarAccess.getEntityAccess().getAttributesAssignment_6()); 
            // InternalRestfulWebservice.g:1944:2: ( rule__Entity__AttributesAssignment_6 )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==20) ) {
                    int LA11_1 = input.LA(2);

                    if ( (LA11_1==RULE_STRING) ) {
                        int LA11_4 = input.LA(3);

                        if ( (LA11_4==RULE_ID) ) {
                            alt11=1;
                        }


                    }


                }
                else if ( (LA11_0==RULE_ID) ) {
                    int LA11_3 = input.LA(2);

                    if ( (LA11_3==28) ) {
                        alt11=1;
                    }


                }


                switch (alt11) {
            	case 1 :
            	    // InternalRestfulWebservice.g:1944:3: rule__Entity__AttributesAssignment_6
            	    {
            	    pushFollow(FOLLOW_24);
            	    rule__Entity__AttributesAssignment_6();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

             after(grammarAccess.getEntityAccess().getAttributesAssignment_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__6__Impl"


    // $ANTLR start "rule__Entity__Group__7"
    // InternalRestfulWebservice.g:1952:1: rule__Entity__Group__7 : rule__Entity__Group__7__Impl rule__Entity__Group__8 ;
    public final void rule__Entity__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1956:1: ( rule__Entity__Group__7__Impl rule__Entity__Group__8 )
            // InternalRestfulWebservice.g:1957:2: rule__Entity__Group__7__Impl rule__Entity__Group__8
            {
            pushFollow(FOLLOW_23);
            rule__Entity__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Entity__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__7"


    // $ANTLR start "rule__Entity__Group__7__Impl"
    // InternalRestfulWebservice.g:1964:1: rule__Entity__Group__7__Impl : ( ( rule__Entity__FunctionsAssignment_7 )* ) ;
    public final void rule__Entity__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1968:1: ( ( ( rule__Entity__FunctionsAssignment_7 )* ) )
            // InternalRestfulWebservice.g:1969:1: ( ( rule__Entity__FunctionsAssignment_7 )* )
            {
            // InternalRestfulWebservice.g:1969:1: ( ( rule__Entity__FunctionsAssignment_7 )* )
            // InternalRestfulWebservice.g:1970:2: ( rule__Entity__FunctionsAssignment_7 )*
            {
             before(grammarAccess.getEntityAccess().getFunctionsAssignment_7()); 
            // InternalRestfulWebservice.g:1971:2: ( rule__Entity__FunctionsAssignment_7 )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( (LA12_0==20||LA12_0==32) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // InternalRestfulWebservice.g:1971:3: rule__Entity__FunctionsAssignment_7
            	    {
            	    pushFollow(FOLLOW_25);
            	    rule__Entity__FunctionsAssignment_7();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);

             after(grammarAccess.getEntityAccess().getFunctionsAssignment_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__7__Impl"


    // $ANTLR start "rule__Entity__Group__8"
    // InternalRestfulWebservice.g:1979:1: rule__Entity__Group__8 : rule__Entity__Group__8__Impl rule__Entity__Group__9 ;
    public final void rule__Entity__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1983:1: ( rule__Entity__Group__8__Impl rule__Entity__Group__9 )
            // InternalRestfulWebservice.g:1984:2: rule__Entity__Group__8__Impl rule__Entity__Group__9
            {
            pushFollow(FOLLOW_23);
            rule__Entity__Group__8__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Entity__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__8"


    // $ANTLR start "rule__Entity__Group__8__Impl"
    // InternalRestfulWebservice.g:1991:1: rule__Entity__Group__8__Impl : ( ( rule__Entity__RelationsAssignment_8 )* ) ;
    public final void rule__Entity__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:1995:1: ( ( ( rule__Entity__RelationsAssignment_8 )* ) )
            // InternalRestfulWebservice.g:1996:1: ( ( rule__Entity__RelationsAssignment_8 )* )
            {
            // InternalRestfulWebservice.g:1996:1: ( ( rule__Entity__RelationsAssignment_8 )* )
            // InternalRestfulWebservice.g:1997:2: ( rule__Entity__RelationsAssignment_8 )*
            {
             before(grammarAccess.getEntityAccess().getRelationsAssignment_8()); 
            // InternalRestfulWebservice.g:1998:2: ( rule__Entity__RelationsAssignment_8 )*
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( (LA13_0==RULE_ID) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // InternalRestfulWebservice.g:1998:3: rule__Entity__RelationsAssignment_8
            	    {
            	    pushFollow(FOLLOW_26);
            	    rule__Entity__RelationsAssignment_8();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);

             after(grammarAccess.getEntityAccess().getRelationsAssignment_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__8__Impl"


    // $ANTLR start "rule__Entity__Group__9"
    // InternalRestfulWebservice.g:2006:1: rule__Entity__Group__9 : rule__Entity__Group__9__Impl ;
    public final void rule__Entity__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2010:1: ( rule__Entity__Group__9__Impl )
            // InternalRestfulWebservice.g:2011:2: rule__Entity__Group__9__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Entity__Group__9__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__9"


    // $ANTLR start "rule__Entity__Group__9__Impl"
    // InternalRestfulWebservice.g:2017:1: rule__Entity__Group__9__Impl : ( '}' ) ;
    public final void rule__Entity__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2021:1: ( ( '}' ) )
            // InternalRestfulWebservice.g:2022:1: ( '}' )
            {
            // InternalRestfulWebservice.g:2022:1: ( '}' )
            // InternalRestfulWebservice.g:2023:2: '}'
            {
             before(grammarAccess.getEntityAccess().getRightCurlyBracketKeyword_9()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getEntityAccess().getRightCurlyBracketKeyword_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__9__Impl"


    // $ANTLR start "rule__Entity__Group_3__0"
    // InternalRestfulWebservice.g:2033:1: rule__Entity__Group_3__0 : rule__Entity__Group_3__0__Impl rule__Entity__Group_3__1 ;
    public final void rule__Entity__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2037:1: ( rule__Entity__Group_3__0__Impl rule__Entity__Group_3__1 )
            // InternalRestfulWebservice.g:2038:2: rule__Entity__Group_3__0__Impl rule__Entity__Group_3__1
            {
            pushFollow(FOLLOW_15);
            rule__Entity__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Entity__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group_3__0"


    // $ANTLR start "rule__Entity__Group_3__0__Impl"
    // InternalRestfulWebservice.g:2045:1: rule__Entity__Group_3__0__Impl : ( 'extends' ) ;
    public final void rule__Entity__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2049:1: ( ( 'extends' ) )
            // InternalRestfulWebservice.g:2050:1: ( 'extends' )
            {
            // InternalRestfulWebservice.g:2050:1: ( 'extends' )
            // InternalRestfulWebservice.g:2051:2: 'extends'
            {
             before(grammarAccess.getEntityAccess().getExtendsKeyword_3_0()); 
            match(input,27,FOLLOW_2); 
             after(grammarAccess.getEntityAccess().getExtendsKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group_3__0__Impl"


    // $ANTLR start "rule__Entity__Group_3__1"
    // InternalRestfulWebservice.g:2060:1: rule__Entity__Group_3__1 : rule__Entity__Group_3__1__Impl ;
    public final void rule__Entity__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2064:1: ( rule__Entity__Group_3__1__Impl )
            // InternalRestfulWebservice.g:2065:2: rule__Entity__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Entity__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group_3__1"


    // $ANTLR start "rule__Entity__Group_3__1__Impl"
    // InternalRestfulWebservice.g:2071:1: rule__Entity__Group_3__1__Impl : ( ( rule__Entity__SuperTypeAssignment_3_1 ) ) ;
    public final void rule__Entity__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2075:1: ( ( ( rule__Entity__SuperTypeAssignment_3_1 ) ) )
            // InternalRestfulWebservice.g:2076:1: ( ( rule__Entity__SuperTypeAssignment_3_1 ) )
            {
            // InternalRestfulWebservice.g:2076:1: ( ( rule__Entity__SuperTypeAssignment_3_1 ) )
            // InternalRestfulWebservice.g:2077:2: ( rule__Entity__SuperTypeAssignment_3_1 )
            {
             before(grammarAccess.getEntityAccess().getSuperTypeAssignment_3_1()); 
            // InternalRestfulWebservice.g:2078:2: ( rule__Entity__SuperTypeAssignment_3_1 )
            // InternalRestfulWebservice.g:2078:3: rule__Entity__SuperTypeAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Entity__SuperTypeAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getEntityAccess().getSuperTypeAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group_3__1__Impl"


    // $ANTLR start "rule__Attribute__Group__0"
    // InternalRestfulWebservice.g:2087:1: rule__Attribute__Group__0 : rule__Attribute__Group__0__Impl rule__Attribute__Group__1 ;
    public final void rule__Attribute__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2091:1: ( rule__Attribute__Group__0__Impl rule__Attribute__Group__1 )
            // InternalRestfulWebservice.g:2092:2: rule__Attribute__Group__0__Impl rule__Attribute__Group__1
            {
            pushFollow(FOLLOW_27);
            rule__Attribute__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Attribute__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__0"


    // $ANTLR start "rule__Attribute__Group__0__Impl"
    // InternalRestfulWebservice.g:2099:1: rule__Attribute__Group__0__Impl : ( ( rule__Attribute__DescriptionAssignment_0 )? ) ;
    public final void rule__Attribute__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2103:1: ( ( ( rule__Attribute__DescriptionAssignment_0 )? ) )
            // InternalRestfulWebservice.g:2104:1: ( ( rule__Attribute__DescriptionAssignment_0 )? )
            {
            // InternalRestfulWebservice.g:2104:1: ( ( rule__Attribute__DescriptionAssignment_0 )? )
            // InternalRestfulWebservice.g:2105:2: ( rule__Attribute__DescriptionAssignment_0 )?
            {
             before(grammarAccess.getAttributeAccess().getDescriptionAssignment_0()); 
            // InternalRestfulWebservice.g:2106:2: ( rule__Attribute__DescriptionAssignment_0 )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==20) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // InternalRestfulWebservice.g:2106:3: rule__Attribute__DescriptionAssignment_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Attribute__DescriptionAssignment_0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getAttributeAccess().getDescriptionAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__0__Impl"


    // $ANTLR start "rule__Attribute__Group__1"
    // InternalRestfulWebservice.g:2114:1: rule__Attribute__Group__1 : rule__Attribute__Group__1__Impl rule__Attribute__Group__2 ;
    public final void rule__Attribute__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2118:1: ( rule__Attribute__Group__1__Impl rule__Attribute__Group__2 )
            // InternalRestfulWebservice.g:2119:2: rule__Attribute__Group__1__Impl rule__Attribute__Group__2
            {
            pushFollow(FOLLOW_28);
            rule__Attribute__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Attribute__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__1"


    // $ANTLR start "rule__Attribute__Group__1__Impl"
    // InternalRestfulWebservice.g:2126:1: rule__Attribute__Group__1__Impl : ( ( rule__Attribute__NameAssignment_1 ) ) ;
    public final void rule__Attribute__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2130:1: ( ( ( rule__Attribute__NameAssignment_1 ) ) )
            // InternalRestfulWebservice.g:2131:1: ( ( rule__Attribute__NameAssignment_1 ) )
            {
            // InternalRestfulWebservice.g:2131:1: ( ( rule__Attribute__NameAssignment_1 ) )
            // InternalRestfulWebservice.g:2132:2: ( rule__Attribute__NameAssignment_1 )
            {
             before(grammarAccess.getAttributeAccess().getNameAssignment_1()); 
            // InternalRestfulWebservice.g:2133:2: ( rule__Attribute__NameAssignment_1 )
            // InternalRestfulWebservice.g:2133:3: rule__Attribute__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Attribute__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getAttributeAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__1__Impl"


    // $ANTLR start "rule__Attribute__Group__2"
    // InternalRestfulWebservice.g:2141:1: rule__Attribute__Group__2 : rule__Attribute__Group__2__Impl rule__Attribute__Group__3 ;
    public final void rule__Attribute__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2145:1: ( rule__Attribute__Group__2__Impl rule__Attribute__Group__3 )
            // InternalRestfulWebservice.g:2146:2: rule__Attribute__Group__2__Impl rule__Attribute__Group__3
            {
            pushFollow(FOLLOW_15);
            rule__Attribute__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Attribute__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__2"


    // $ANTLR start "rule__Attribute__Group__2__Impl"
    // InternalRestfulWebservice.g:2153:1: rule__Attribute__Group__2__Impl : ( ':' ) ;
    public final void rule__Attribute__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2157:1: ( ( ':' ) )
            // InternalRestfulWebservice.g:2158:1: ( ':' )
            {
            // InternalRestfulWebservice.g:2158:1: ( ':' )
            // InternalRestfulWebservice.g:2159:2: ':'
            {
             before(grammarAccess.getAttributeAccess().getColonKeyword_2()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getAttributeAccess().getColonKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__2__Impl"


    // $ANTLR start "rule__Attribute__Group__3"
    // InternalRestfulWebservice.g:2168:1: rule__Attribute__Group__3 : rule__Attribute__Group__3__Impl ;
    public final void rule__Attribute__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2172:1: ( rule__Attribute__Group__3__Impl )
            // InternalRestfulWebservice.g:2173:2: rule__Attribute__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Attribute__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__3"


    // $ANTLR start "rule__Attribute__Group__3__Impl"
    // InternalRestfulWebservice.g:2179:1: rule__Attribute__Group__3__Impl : ( ( rule__Attribute__TypeAssignment_3 ) ) ;
    public final void rule__Attribute__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2183:1: ( ( ( rule__Attribute__TypeAssignment_3 ) ) )
            // InternalRestfulWebservice.g:2184:1: ( ( rule__Attribute__TypeAssignment_3 ) )
            {
            // InternalRestfulWebservice.g:2184:1: ( ( rule__Attribute__TypeAssignment_3 ) )
            // InternalRestfulWebservice.g:2185:2: ( rule__Attribute__TypeAssignment_3 )
            {
             before(grammarAccess.getAttributeAccess().getTypeAssignment_3()); 
            // InternalRestfulWebservice.g:2186:2: ( rule__Attribute__TypeAssignment_3 )
            // InternalRestfulWebservice.g:2186:3: rule__Attribute__TypeAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Attribute__TypeAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getAttributeAccess().getTypeAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__3__Impl"


    // $ANTLR start "rule__OneToOne__Group__0"
    // InternalRestfulWebservice.g:2195:1: rule__OneToOne__Group__0 : rule__OneToOne__Group__0__Impl rule__OneToOne__Group__1 ;
    public final void rule__OneToOne__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2199:1: ( rule__OneToOne__Group__0__Impl rule__OneToOne__Group__1 )
            // InternalRestfulWebservice.g:2200:2: rule__OneToOne__Group__0__Impl rule__OneToOne__Group__1
            {
            pushFollow(FOLLOW_29);
            rule__OneToOne__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OneToOne__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OneToOne__Group__0"


    // $ANTLR start "rule__OneToOne__Group__0__Impl"
    // InternalRestfulWebservice.g:2207:1: rule__OneToOne__Group__0__Impl : ( ( rule__OneToOne__NameAssignment_0 ) ) ;
    public final void rule__OneToOne__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2211:1: ( ( ( rule__OneToOne__NameAssignment_0 ) ) )
            // InternalRestfulWebservice.g:2212:1: ( ( rule__OneToOne__NameAssignment_0 ) )
            {
            // InternalRestfulWebservice.g:2212:1: ( ( rule__OneToOne__NameAssignment_0 ) )
            // InternalRestfulWebservice.g:2213:2: ( rule__OneToOne__NameAssignment_0 )
            {
             before(grammarAccess.getOneToOneAccess().getNameAssignment_0()); 
            // InternalRestfulWebservice.g:2214:2: ( rule__OneToOne__NameAssignment_0 )
            // InternalRestfulWebservice.g:2214:3: rule__OneToOne__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__OneToOne__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getOneToOneAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OneToOne__Group__0__Impl"


    // $ANTLR start "rule__OneToOne__Group__1"
    // InternalRestfulWebservice.g:2222:1: rule__OneToOne__Group__1 : rule__OneToOne__Group__1__Impl rule__OneToOne__Group__2 ;
    public final void rule__OneToOne__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2226:1: ( rule__OneToOne__Group__1__Impl rule__OneToOne__Group__2 )
            // InternalRestfulWebservice.g:2227:2: rule__OneToOne__Group__1__Impl rule__OneToOne__Group__2
            {
            pushFollow(FOLLOW_15);
            rule__OneToOne__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OneToOne__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OneToOne__Group__1"


    // $ANTLR start "rule__OneToOne__Group__1__Impl"
    // InternalRestfulWebservice.g:2234:1: rule__OneToOne__Group__1__Impl : ( 'OneToOne' ) ;
    public final void rule__OneToOne__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2238:1: ( ( 'OneToOne' ) )
            // InternalRestfulWebservice.g:2239:1: ( 'OneToOne' )
            {
            // InternalRestfulWebservice.g:2239:1: ( 'OneToOne' )
            // InternalRestfulWebservice.g:2240:2: 'OneToOne'
            {
             before(grammarAccess.getOneToOneAccess().getOneToOneKeyword_1()); 
            match(input,29,FOLLOW_2); 
             after(grammarAccess.getOneToOneAccess().getOneToOneKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OneToOne__Group__1__Impl"


    // $ANTLR start "rule__OneToOne__Group__2"
    // InternalRestfulWebservice.g:2249:1: rule__OneToOne__Group__2 : rule__OneToOne__Group__2__Impl ;
    public final void rule__OneToOne__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2253:1: ( rule__OneToOne__Group__2__Impl )
            // InternalRestfulWebservice.g:2254:2: rule__OneToOne__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__OneToOne__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OneToOne__Group__2"


    // $ANTLR start "rule__OneToOne__Group__2__Impl"
    // InternalRestfulWebservice.g:2260:1: rule__OneToOne__Group__2__Impl : ( ( rule__OneToOne__TypeAssignment_2 ) ) ;
    public final void rule__OneToOne__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2264:1: ( ( ( rule__OneToOne__TypeAssignment_2 ) ) )
            // InternalRestfulWebservice.g:2265:1: ( ( rule__OneToOne__TypeAssignment_2 ) )
            {
            // InternalRestfulWebservice.g:2265:1: ( ( rule__OneToOne__TypeAssignment_2 ) )
            // InternalRestfulWebservice.g:2266:2: ( rule__OneToOne__TypeAssignment_2 )
            {
             before(grammarAccess.getOneToOneAccess().getTypeAssignment_2()); 
            // InternalRestfulWebservice.g:2267:2: ( rule__OneToOne__TypeAssignment_2 )
            // InternalRestfulWebservice.g:2267:3: rule__OneToOne__TypeAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__OneToOne__TypeAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getOneToOneAccess().getTypeAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OneToOne__Group__2__Impl"


    // $ANTLR start "rule__ManyToMany__Group__0"
    // InternalRestfulWebservice.g:2276:1: rule__ManyToMany__Group__0 : rule__ManyToMany__Group__0__Impl rule__ManyToMany__Group__1 ;
    public final void rule__ManyToMany__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2280:1: ( rule__ManyToMany__Group__0__Impl rule__ManyToMany__Group__1 )
            // InternalRestfulWebservice.g:2281:2: rule__ManyToMany__Group__0__Impl rule__ManyToMany__Group__1
            {
            pushFollow(FOLLOW_30);
            rule__ManyToMany__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ManyToMany__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ManyToMany__Group__0"


    // $ANTLR start "rule__ManyToMany__Group__0__Impl"
    // InternalRestfulWebservice.g:2288:1: rule__ManyToMany__Group__0__Impl : ( ( rule__ManyToMany__NameAssignment_0 ) ) ;
    public final void rule__ManyToMany__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2292:1: ( ( ( rule__ManyToMany__NameAssignment_0 ) ) )
            // InternalRestfulWebservice.g:2293:1: ( ( rule__ManyToMany__NameAssignment_0 ) )
            {
            // InternalRestfulWebservice.g:2293:1: ( ( rule__ManyToMany__NameAssignment_0 ) )
            // InternalRestfulWebservice.g:2294:2: ( rule__ManyToMany__NameAssignment_0 )
            {
             before(grammarAccess.getManyToManyAccess().getNameAssignment_0()); 
            // InternalRestfulWebservice.g:2295:2: ( rule__ManyToMany__NameAssignment_0 )
            // InternalRestfulWebservice.g:2295:3: rule__ManyToMany__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__ManyToMany__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getManyToManyAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ManyToMany__Group__0__Impl"


    // $ANTLR start "rule__ManyToMany__Group__1"
    // InternalRestfulWebservice.g:2303:1: rule__ManyToMany__Group__1 : rule__ManyToMany__Group__1__Impl rule__ManyToMany__Group__2 ;
    public final void rule__ManyToMany__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2307:1: ( rule__ManyToMany__Group__1__Impl rule__ManyToMany__Group__2 )
            // InternalRestfulWebservice.g:2308:2: rule__ManyToMany__Group__1__Impl rule__ManyToMany__Group__2
            {
            pushFollow(FOLLOW_15);
            rule__ManyToMany__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ManyToMany__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ManyToMany__Group__1"


    // $ANTLR start "rule__ManyToMany__Group__1__Impl"
    // InternalRestfulWebservice.g:2315:1: rule__ManyToMany__Group__1__Impl : ( 'ManyToMany' ) ;
    public final void rule__ManyToMany__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2319:1: ( ( 'ManyToMany' ) )
            // InternalRestfulWebservice.g:2320:1: ( 'ManyToMany' )
            {
            // InternalRestfulWebservice.g:2320:1: ( 'ManyToMany' )
            // InternalRestfulWebservice.g:2321:2: 'ManyToMany'
            {
             before(grammarAccess.getManyToManyAccess().getManyToManyKeyword_1()); 
            match(input,30,FOLLOW_2); 
             after(grammarAccess.getManyToManyAccess().getManyToManyKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ManyToMany__Group__1__Impl"


    // $ANTLR start "rule__ManyToMany__Group__2"
    // InternalRestfulWebservice.g:2330:1: rule__ManyToMany__Group__2 : rule__ManyToMany__Group__2__Impl ;
    public final void rule__ManyToMany__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2334:1: ( rule__ManyToMany__Group__2__Impl )
            // InternalRestfulWebservice.g:2335:2: rule__ManyToMany__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ManyToMany__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ManyToMany__Group__2"


    // $ANTLR start "rule__ManyToMany__Group__2__Impl"
    // InternalRestfulWebservice.g:2341:1: rule__ManyToMany__Group__2__Impl : ( ( rule__ManyToMany__TypeAssignment_2 ) ) ;
    public final void rule__ManyToMany__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2345:1: ( ( ( rule__ManyToMany__TypeAssignment_2 ) ) )
            // InternalRestfulWebservice.g:2346:1: ( ( rule__ManyToMany__TypeAssignment_2 ) )
            {
            // InternalRestfulWebservice.g:2346:1: ( ( rule__ManyToMany__TypeAssignment_2 ) )
            // InternalRestfulWebservice.g:2347:2: ( rule__ManyToMany__TypeAssignment_2 )
            {
             before(grammarAccess.getManyToManyAccess().getTypeAssignment_2()); 
            // InternalRestfulWebservice.g:2348:2: ( rule__ManyToMany__TypeAssignment_2 )
            // InternalRestfulWebservice.g:2348:3: rule__ManyToMany__TypeAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__ManyToMany__TypeAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getManyToManyAccess().getTypeAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ManyToMany__Group__2__Impl"


    // $ANTLR start "rule__OneToMany__Group__0"
    // InternalRestfulWebservice.g:2357:1: rule__OneToMany__Group__0 : rule__OneToMany__Group__0__Impl rule__OneToMany__Group__1 ;
    public final void rule__OneToMany__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2361:1: ( rule__OneToMany__Group__0__Impl rule__OneToMany__Group__1 )
            // InternalRestfulWebservice.g:2362:2: rule__OneToMany__Group__0__Impl rule__OneToMany__Group__1
            {
            pushFollow(FOLLOW_31);
            rule__OneToMany__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OneToMany__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OneToMany__Group__0"


    // $ANTLR start "rule__OneToMany__Group__0__Impl"
    // InternalRestfulWebservice.g:2369:1: rule__OneToMany__Group__0__Impl : ( ( rule__OneToMany__NameAssignment_0 ) ) ;
    public final void rule__OneToMany__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2373:1: ( ( ( rule__OneToMany__NameAssignment_0 ) ) )
            // InternalRestfulWebservice.g:2374:1: ( ( rule__OneToMany__NameAssignment_0 ) )
            {
            // InternalRestfulWebservice.g:2374:1: ( ( rule__OneToMany__NameAssignment_0 ) )
            // InternalRestfulWebservice.g:2375:2: ( rule__OneToMany__NameAssignment_0 )
            {
             before(grammarAccess.getOneToManyAccess().getNameAssignment_0()); 
            // InternalRestfulWebservice.g:2376:2: ( rule__OneToMany__NameAssignment_0 )
            // InternalRestfulWebservice.g:2376:3: rule__OneToMany__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__OneToMany__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getOneToManyAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OneToMany__Group__0__Impl"


    // $ANTLR start "rule__OneToMany__Group__1"
    // InternalRestfulWebservice.g:2384:1: rule__OneToMany__Group__1 : rule__OneToMany__Group__1__Impl rule__OneToMany__Group__2 ;
    public final void rule__OneToMany__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2388:1: ( rule__OneToMany__Group__1__Impl rule__OneToMany__Group__2 )
            // InternalRestfulWebservice.g:2389:2: rule__OneToMany__Group__1__Impl rule__OneToMany__Group__2
            {
            pushFollow(FOLLOW_15);
            rule__OneToMany__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OneToMany__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OneToMany__Group__1"


    // $ANTLR start "rule__OneToMany__Group__1__Impl"
    // InternalRestfulWebservice.g:2396:1: rule__OneToMany__Group__1__Impl : ( 'OneToMany' ) ;
    public final void rule__OneToMany__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2400:1: ( ( 'OneToMany' ) )
            // InternalRestfulWebservice.g:2401:1: ( 'OneToMany' )
            {
            // InternalRestfulWebservice.g:2401:1: ( 'OneToMany' )
            // InternalRestfulWebservice.g:2402:2: 'OneToMany'
            {
             before(grammarAccess.getOneToManyAccess().getOneToManyKeyword_1()); 
            match(input,31,FOLLOW_2); 
             after(grammarAccess.getOneToManyAccess().getOneToManyKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OneToMany__Group__1__Impl"


    // $ANTLR start "rule__OneToMany__Group__2"
    // InternalRestfulWebservice.g:2411:1: rule__OneToMany__Group__2 : rule__OneToMany__Group__2__Impl ;
    public final void rule__OneToMany__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2415:1: ( rule__OneToMany__Group__2__Impl )
            // InternalRestfulWebservice.g:2416:2: rule__OneToMany__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__OneToMany__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OneToMany__Group__2"


    // $ANTLR start "rule__OneToMany__Group__2__Impl"
    // InternalRestfulWebservice.g:2422:1: rule__OneToMany__Group__2__Impl : ( ( rule__OneToMany__TypeAssignment_2 ) ) ;
    public final void rule__OneToMany__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2426:1: ( ( ( rule__OneToMany__TypeAssignment_2 ) ) )
            // InternalRestfulWebservice.g:2427:1: ( ( rule__OneToMany__TypeAssignment_2 ) )
            {
            // InternalRestfulWebservice.g:2427:1: ( ( rule__OneToMany__TypeAssignment_2 ) )
            // InternalRestfulWebservice.g:2428:2: ( rule__OneToMany__TypeAssignment_2 )
            {
             before(grammarAccess.getOneToManyAccess().getTypeAssignment_2()); 
            // InternalRestfulWebservice.g:2429:2: ( rule__OneToMany__TypeAssignment_2 )
            // InternalRestfulWebservice.g:2429:3: rule__OneToMany__TypeAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__OneToMany__TypeAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getOneToManyAccess().getTypeAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OneToMany__Group__2__Impl"


    // $ANTLR start "rule__Function__Group__0"
    // InternalRestfulWebservice.g:2438:1: rule__Function__Group__0 : rule__Function__Group__0__Impl rule__Function__Group__1 ;
    public final void rule__Function__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2442:1: ( rule__Function__Group__0__Impl rule__Function__Group__1 )
            // InternalRestfulWebservice.g:2443:2: rule__Function__Group__0__Impl rule__Function__Group__1
            {
            pushFollow(FOLLOW_32);
            rule__Function__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Function__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__0"


    // $ANTLR start "rule__Function__Group__0__Impl"
    // InternalRestfulWebservice.g:2450:1: rule__Function__Group__0__Impl : ( ( rule__Function__DescriptionAssignment_0 )? ) ;
    public final void rule__Function__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2454:1: ( ( ( rule__Function__DescriptionAssignment_0 )? ) )
            // InternalRestfulWebservice.g:2455:1: ( ( rule__Function__DescriptionAssignment_0 )? )
            {
            // InternalRestfulWebservice.g:2455:1: ( ( rule__Function__DescriptionAssignment_0 )? )
            // InternalRestfulWebservice.g:2456:2: ( rule__Function__DescriptionAssignment_0 )?
            {
             before(grammarAccess.getFunctionAccess().getDescriptionAssignment_0()); 
            // InternalRestfulWebservice.g:2457:2: ( rule__Function__DescriptionAssignment_0 )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==20) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // InternalRestfulWebservice.g:2457:3: rule__Function__DescriptionAssignment_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Function__DescriptionAssignment_0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getFunctionAccess().getDescriptionAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__0__Impl"


    // $ANTLR start "rule__Function__Group__1"
    // InternalRestfulWebservice.g:2465:1: rule__Function__Group__1 : rule__Function__Group__1__Impl rule__Function__Group__2 ;
    public final void rule__Function__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2469:1: ( rule__Function__Group__1__Impl rule__Function__Group__2 )
            // InternalRestfulWebservice.g:2470:2: rule__Function__Group__1__Impl rule__Function__Group__2
            {
            pushFollow(FOLLOW_15);
            rule__Function__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Function__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__1"


    // $ANTLR start "rule__Function__Group__1__Impl"
    // InternalRestfulWebservice.g:2477:1: rule__Function__Group__1__Impl : ( 'function' ) ;
    public final void rule__Function__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2481:1: ( ( 'function' ) )
            // InternalRestfulWebservice.g:2482:1: ( 'function' )
            {
            // InternalRestfulWebservice.g:2482:1: ( 'function' )
            // InternalRestfulWebservice.g:2483:2: 'function'
            {
             before(grammarAccess.getFunctionAccess().getFunctionKeyword_1()); 
            match(input,32,FOLLOW_2); 
             after(grammarAccess.getFunctionAccess().getFunctionKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__1__Impl"


    // $ANTLR start "rule__Function__Group__2"
    // InternalRestfulWebservice.g:2492:1: rule__Function__Group__2 : rule__Function__Group__2__Impl rule__Function__Group__3 ;
    public final void rule__Function__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2496:1: ( rule__Function__Group__2__Impl rule__Function__Group__3 )
            // InternalRestfulWebservice.g:2497:2: rule__Function__Group__2__Impl rule__Function__Group__3
            {
            pushFollow(FOLLOW_33);
            rule__Function__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Function__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__2"


    // $ANTLR start "rule__Function__Group__2__Impl"
    // InternalRestfulWebservice.g:2504:1: rule__Function__Group__2__Impl : ( ( rule__Function__NameAssignment_2 ) ) ;
    public final void rule__Function__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2508:1: ( ( ( rule__Function__NameAssignment_2 ) ) )
            // InternalRestfulWebservice.g:2509:1: ( ( rule__Function__NameAssignment_2 ) )
            {
            // InternalRestfulWebservice.g:2509:1: ( ( rule__Function__NameAssignment_2 ) )
            // InternalRestfulWebservice.g:2510:2: ( rule__Function__NameAssignment_2 )
            {
             before(grammarAccess.getFunctionAccess().getNameAssignment_2()); 
            // InternalRestfulWebservice.g:2511:2: ( rule__Function__NameAssignment_2 )
            // InternalRestfulWebservice.g:2511:3: rule__Function__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Function__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getFunctionAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__2__Impl"


    // $ANTLR start "rule__Function__Group__3"
    // InternalRestfulWebservice.g:2519:1: rule__Function__Group__3 : rule__Function__Group__3__Impl rule__Function__Group__4 ;
    public final void rule__Function__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2523:1: ( rule__Function__Group__3__Impl rule__Function__Group__4 )
            // InternalRestfulWebservice.g:2524:2: rule__Function__Group__3__Impl rule__Function__Group__4
            {
            pushFollow(FOLLOW_34);
            rule__Function__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Function__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__3"


    // $ANTLR start "rule__Function__Group__3__Impl"
    // InternalRestfulWebservice.g:2531:1: rule__Function__Group__3__Impl : ( '(' ) ;
    public final void rule__Function__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2535:1: ( ( '(' ) )
            // InternalRestfulWebservice.g:2536:1: ( '(' )
            {
            // InternalRestfulWebservice.g:2536:1: ( '(' )
            // InternalRestfulWebservice.g:2537:2: '('
            {
             before(grammarAccess.getFunctionAccess().getLeftParenthesisKeyword_3()); 
            match(input,33,FOLLOW_2); 
             after(grammarAccess.getFunctionAccess().getLeftParenthesisKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__3__Impl"


    // $ANTLR start "rule__Function__Group__4"
    // InternalRestfulWebservice.g:2546:1: rule__Function__Group__4 : rule__Function__Group__4__Impl rule__Function__Group__5 ;
    public final void rule__Function__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2550:1: ( rule__Function__Group__4__Impl rule__Function__Group__5 )
            // InternalRestfulWebservice.g:2551:2: rule__Function__Group__4__Impl rule__Function__Group__5
            {
            pushFollow(FOLLOW_34);
            rule__Function__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Function__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__4"


    // $ANTLR start "rule__Function__Group__4__Impl"
    // InternalRestfulWebservice.g:2558:1: rule__Function__Group__4__Impl : ( ( rule__Function__Group_4__0 )? ) ;
    public final void rule__Function__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2562:1: ( ( ( rule__Function__Group_4__0 )? ) )
            // InternalRestfulWebservice.g:2563:1: ( ( rule__Function__Group_4__0 )? )
            {
            // InternalRestfulWebservice.g:2563:1: ( ( rule__Function__Group_4__0 )? )
            // InternalRestfulWebservice.g:2564:2: ( rule__Function__Group_4__0 )?
            {
             before(grammarAccess.getFunctionAccess().getGroup_4()); 
            // InternalRestfulWebservice.g:2565:2: ( rule__Function__Group_4__0 )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==RULE_ID) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // InternalRestfulWebservice.g:2565:3: rule__Function__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Function__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getFunctionAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__4__Impl"


    // $ANTLR start "rule__Function__Group__5"
    // InternalRestfulWebservice.g:2573:1: rule__Function__Group__5 : rule__Function__Group__5__Impl rule__Function__Group__6 ;
    public final void rule__Function__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2577:1: ( rule__Function__Group__5__Impl rule__Function__Group__6 )
            // InternalRestfulWebservice.g:2578:2: rule__Function__Group__5__Impl rule__Function__Group__6
            {
            pushFollow(FOLLOW_28);
            rule__Function__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Function__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__5"


    // $ANTLR start "rule__Function__Group__5__Impl"
    // InternalRestfulWebservice.g:2585:1: rule__Function__Group__5__Impl : ( ')' ) ;
    public final void rule__Function__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2589:1: ( ( ')' ) )
            // InternalRestfulWebservice.g:2590:1: ( ')' )
            {
            // InternalRestfulWebservice.g:2590:1: ( ')' )
            // InternalRestfulWebservice.g:2591:2: ')'
            {
             before(grammarAccess.getFunctionAccess().getRightParenthesisKeyword_5()); 
            match(input,34,FOLLOW_2); 
             after(grammarAccess.getFunctionAccess().getRightParenthesisKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__5__Impl"


    // $ANTLR start "rule__Function__Group__6"
    // InternalRestfulWebservice.g:2600:1: rule__Function__Group__6 : rule__Function__Group__6__Impl rule__Function__Group__7 ;
    public final void rule__Function__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2604:1: ( rule__Function__Group__6__Impl rule__Function__Group__7 )
            // InternalRestfulWebservice.g:2605:2: rule__Function__Group__6__Impl rule__Function__Group__7
            {
            pushFollow(FOLLOW_15);
            rule__Function__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Function__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__6"


    // $ANTLR start "rule__Function__Group__6__Impl"
    // InternalRestfulWebservice.g:2612:1: rule__Function__Group__6__Impl : ( ':' ) ;
    public final void rule__Function__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2616:1: ( ( ':' ) )
            // InternalRestfulWebservice.g:2617:1: ( ':' )
            {
            // InternalRestfulWebservice.g:2617:1: ( ':' )
            // InternalRestfulWebservice.g:2618:2: ':'
            {
             before(grammarAccess.getFunctionAccess().getColonKeyword_6()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getFunctionAccess().getColonKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__6__Impl"


    // $ANTLR start "rule__Function__Group__7"
    // InternalRestfulWebservice.g:2627:1: rule__Function__Group__7 : rule__Function__Group__7__Impl ;
    public final void rule__Function__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2631:1: ( rule__Function__Group__7__Impl )
            // InternalRestfulWebservice.g:2632:2: rule__Function__Group__7__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Function__Group__7__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__7"


    // $ANTLR start "rule__Function__Group__7__Impl"
    // InternalRestfulWebservice.g:2638:1: rule__Function__Group__7__Impl : ( ( rule__Function__TypeAssignment_7 ) ) ;
    public final void rule__Function__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2642:1: ( ( ( rule__Function__TypeAssignment_7 ) ) )
            // InternalRestfulWebservice.g:2643:1: ( ( rule__Function__TypeAssignment_7 ) )
            {
            // InternalRestfulWebservice.g:2643:1: ( ( rule__Function__TypeAssignment_7 ) )
            // InternalRestfulWebservice.g:2644:2: ( rule__Function__TypeAssignment_7 )
            {
             before(grammarAccess.getFunctionAccess().getTypeAssignment_7()); 
            // InternalRestfulWebservice.g:2645:2: ( rule__Function__TypeAssignment_7 )
            // InternalRestfulWebservice.g:2645:3: rule__Function__TypeAssignment_7
            {
            pushFollow(FOLLOW_2);
            rule__Function__TypeAssignment_7();

            state._fsp--;


            }

             after(grammarAccess.getFunctionAccess().getTypeAssignment_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__7__Impl"


    // $ANTLR start "rule__Function__Group_4__0"
    // InternalRestfulWebservice.g:2654:1: rule__Function__Group_4__0 : rule__Function__Group_4__0__Impl rule__Function__Group_4__1 ;
    public final void rule__Function__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2658:1: ( rule__Function__Group_4__0__Impl rule__Function__Group_4__1 )
            // InternalRestfulWebservice.g:2659:2: rule__Function__Group_4__0__Impl rule__Function__Group_4__1
            {
            pushFollow(FOLLOW_35);
            rule__Function__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Function__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group_4__0"


    // $ANTLR start "rule__Function__Group_4__0__Impl"
    // InternalRestfulWebservice.g:2666:1: rule__Function__Group_4__0__Impl : ( ( rule__Function__ParamsAssignment_4_0 ) ) ;
    public final void rule__Function__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2670:1: ( ( ( rule__Function__ParamsAssignment_4_0 ) ) )
            // InternalRestfulWebservice.g:2671:1: ( ( rule__Function__ParamsAssignment_4_0 ) )
            {
            // InternalRestfulWebservice.g:2671:1: ( ( rule__Function__ParamsAssignment_4_0 ) )
            // InternalRestfulWebservice.g:2672:2: ( rule__Function__ParamsAssignment_4_0 )
            {
             before(grammarAccess.getFunctionAccess().getParamsAssignment_4_0()); 
            // InternalRestfulWebservice.g:2673:2: ( rule__Function__ParamsAssignment_4_0 )
            // InternalRestfulWebservice.g:2673:3: rule__Function__ParamsAssignment_4_0
            {
            pushFollow(FOLLOW_2);
            rule__Function__ParamsAssignment_4_0();

            state._fsp--;


            }

             after(grammarAccess.getFunctionAccess().getParamsAssignment_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group_4__0__Impl"


    // $ANTLR start "rule__Function__Group_4__1"
    // InternalRestfulWebservice.g:2681:1: rule__Function__Group_4__1 : rule__Function__Group_4__1__Impl ;
    public final void rule__Function__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2685:1: ( rule__Function__Group_4__1__Impl )
            // InternalRestfulWebservice.g:2686:2: rule__Function__Group_4__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Function__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group_4__1"


    // $ANTLR start "rule__Function__Group_4__1__Impl"
    // InternalRestfulWebservice.g:2692:1: rule__Function__Group_4__1__Impl : ( ( rule__Function__Group_4_1__0 )* ) ;
    public final void rule__Function__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2696:1: ( ( ( rule__Function__Group_4_1__0 )* ) )
            // InternalRestfulWebservice.g:2697:1: ( ( rule__Function__Group_4_1__0 )* )
            {
            // InternalRestfulWebservice.g:2697:1: ( ( rule__Function__Group_4_1__0 )* )
            // InternalRestfulWebservice.g:2698:2: ( rule__Function__Group_4_1__0 )*
            {
             before(grammarAccess.getFunctionAccess().getGroup_4_1()); 
            // InternalRestfulWebservice.g:2699:2: ( rule__Function__Group_4_1__0 )*
            loop17:
            do {
                int alt17=2;
                int LA17_0 = input.LA(1);

                if ( (LA17_0==35) ) {
                    alt17=1;
                }


                switch (alt17) {
            	case 1 :
            	    // InternalRestfulWebservice.g:2699:3: rule__Function__Group_4_1__0
            	    {
            	    pushFollow(FOLLOW_36);
            	    rule__Function__Group_4_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop17;
                }
            } while (true);

             after(grammarAccess.getFunctionAccess().getGroup_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group_4__1__Impl"


    // $ANTLR start "rule__Function__Group_4_1__0"
    // InternalRestfulWebservice.g:2708:1: rule__Function__Group_4_1__0 : rule__Function__Group_4_1__0__Impl rule__Function__Group_4_1__1 ;
    public final void rule__Function__Group_4_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2712:1: ( rule__Function__Group_4_1__0__Impl rule__Function__Group_4_1__1 )
            // InternalRestfulWebservice.g:2713:2: rule__Function__Group_4_1__0__Impl rule__Function__Group_4_1__1
            {
            pushFollow(FOLLOW_15);
            rule__Function__Group_4_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Function__Group_4_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group_4_1__0"


    // $ANTLR start "rule__Function__Group_4_1__0__Impl"
    // InternalRestfulWebservice.g:2720:1: rule__Function__Group_4_1__0__Impl : ( ',' ) ;
    public final void rule__Function__Group_4_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2724:1: ( ( ',' ) )
            // InternalRestfulWebservice.g:2725:1: ( ',' )
            {
            // InternalRestfulWebservice.g:2725:1: ( ',' )
            // InternalRestfulWebservice.g:2726:2: ','
            {
             before(grammarAccess.getFunctionAccess().getCommaKeyword_4_1_0()); 
            match(input,35,FOLLOW_2); 
             after(grammarAccess.getFunctionAccess().getCommaKeyword_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group_4_1__0__Impl"


    // $ANTLR start "rule__Function__Group_4_1__1"
    // InternalRestfulWebservice.g:2735:1: rule__Function__Group_4_1__1 : rule__Function__Group_4_1__1__Impl ;
    public final void rule__Function__Group_4_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2739:1: ( rule__Function__Group_4_1__1__Impl )
            // InternalRestfulWebservice.g:2740:2: rule__Function__Group_4_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Function__Group_4_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group_4_1__1"


    // $ANTLR start "rule__Function__Group_4_1__1__Impl"
    // InternalRestfulWebservice.g:2746:1: rule__Function__Group_4_1__1__Impl : ( ( rule__Function__ParamsAssignment_4_1_1 ) ) ;
    public final void rule__Function__Group_4_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2750:1: ( ( ( rule__Function__ParamsAssignment_4_1_1 ) ) )
            // InternalRestfulWebservice.g:2751:1: ( ( rule__Function__ParamsAssignment_4_1_1 ) )
            {
            // InternalRestfulWebservice.g:2751:1: ( ( rule__Function__ParamsAssignment_4_1_1 ) )
            // InternalRestfulWebservice.g:2752:2: ( rule__Function__ParamsAssignment_4_1_1 )
            {
             before(grammarAccess.getFunctionAccess().getParamsAssignment_4_1_1()); 
            // InternalRestfulWebservice.g:2753:2: ( rule__Function__ParamsAssignment_4_1_1 )
            // InternalRestfulWebservice.g:2753:3: rule__Function__ParamsAssignment_4_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Function__ParamsAssignment_4_1_1();

            state._fsp--;


            }

             after(grammarAccess.getFunctionAccess().getParamsAssignment_4_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group_4_1__1__Impl"


    // $ANTLR start "rule__Application__ConfigurationAssignment_0"
    // InternalRestfulWebservice.g:2762:1: rule__Application__ConfigurationAssignment_0 : ( ruleConfiguration ) ;
    public final void rule__Application__ConfigurationAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2766:1: ( ( ruleConfiguration ) )
            // InternalRestfulWebservice.g:2767:2: ( ruleConfiguration )
            {
            // InternalRestfulWebservice.g:2767:2: ( ruleConfiguration )
            // InternalRestfulWebservice.g:2768:3: ruleConfiguration
            {
             before(grammarAccess.getApplicationAccess().getConfigurationConfigurationParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleConfiguration();

            state._fsp--;

             after(grammarAccess.getApplicationAccess().getConfigurationConfigurationParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Application__ConfigurationAssignment_0"


    // $ANTLR start "rule__Application__AbstractElementsAssignment_1"
    // InternalRestfulWebservice.g:2777:1: rule__Application__AbstractElementsAssignment_1 : ( ruleAbstractElement ) ;
    public final void rule__Application__AbstractElementsAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2781:1: ( ( ruleAbstractElement ) )
            // InternalRestfulWebservice.g:2782:2: ( ruleAbstractElement )
            {
            // InternalRestfulWebservice.g:2782:2: ( ruleAbstractElement )
            // InternalRestfulWebservice.g:2783:3: ruleAbstractElement
            {
             before(grammarAccess.getApplicationAccess().getAbstractElementsAbstractElementParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleAbstractElement();

            state._fsp--;

             after(grammarAccess.getApplicationAccess().getAbstractElementsAbstractElementParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Application__AbstractElementsAssignment_1"


    // $ANTLR start "rule__Configuration__SoftwareAssignment_2"
    // InternalRestfulWebservice.g:2792:1: rule__Configuration__SoftwareAssignment_2 : ( ruleSoftware ) ;
    public final void rule__Configuration__SoftwareAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2796:1: ( ( ruleSoftware ) )
            // InternalRestfulWebservice.g:2797:2: ( ruleSoftware )
            {
            // InternalRestfulWebservice.g:2797:2: ( ruleSoftware )
            // InternalRestfulWebservice.g:2798:3: ruleSoftware
            {
             before(grammarAccess.getConfigurationAccess().getSoftwareSoftwareParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleSoftware();

            state._fsp--;

             after(grammarAccess.getConfigurationAccess().getSoftwareSoftwareParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__SoftwareAssignment_2"


    // $ANTLR start "rule__Configuration__AboutAssignment_3"
    // InternalRestfulWebservice.g:2807:1: rule__Configuration__AboutAssignment_3 : ( ruleAbout ) ;
    public final void rule__Configuration__AboutAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2811:1: ( ( ruleAbout ) )
            // InternalRestfulWebservice.g:2812:2: ( ruleAbout )
            {
            // InternalRestfulWebservice.g:2812:2: ( ruleAbout )
            // InternalRestfulWebservice.g:2813:3: ruleAbout
            {
             before(grammarAccess.getConfigurationAccess().getAboutAboutParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleAbout();

            state._fsp--;

             after(grammarAccess.getConfigurationAccess().getAboutAboutParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__AboutAssignment_3"


    // $ANTLR start "rule__Configuration__LibAssignment_4"
    // InternalRestfulWebservice.g:2822:1: rule__Configuration__LibAssignment_4 : ( ruleLib ) ;
    public final void rule__Configuration__LibAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2826:1: ( ( ruleLib ) )
            // InternalRestfulWebservice.g:2827:2: ( ruleLib )
            {
            // InternalRestfulWebservice.g:2827:2: ( ruleLib )
            // InternalRestfulWebservice.g:2828:3: ruleLib
            {
             before(grammarAccess.getConfigurationAccess().getLibLibParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleLib();

            state._fsp--;

             after(grammarAccess.getConfigurationAccess().getLibLibParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__LibAssignment_4"


    // $ANTLR start "rule__Configuration__AuthorAssignment_5"
    // InternalRestfulWebservice.g:2837:1: rule__Configuration__AuthorAssignment_5 : ( ruleAuthor ) ;
    public final void rule__Configuration__AuthorAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2841:1: ( ( ruleAuthor ) )
            // InternalRestfulWebservice.g:2842:2: ( ruleAuthor )
            {
            // InternalRestfulWebservice.g:2842:2: ( ruleAuthor )
            // InternalRestfulWebservice.g:2843:3: ruleAuthor
            {
             before(grammarAccess.getConfigurationAccess().getAuthorAuthorParserRuleCall_5_0()); 
            pushFollow(FOLLOW_2);
            ruleAuthor();

            state._fsp--;

             after(grammarAccess.getConfigurationAccess().getAuthorAuthorParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__AuthorAssignment_5"


    // $ANTLR start "rule__Configuration__Author_emailAssignment_6"
    // InternalRestfulWebservice.g:2852:1: rule__Configuration__Author_emailAssignment_6 : ( ruleAuthor_Email ) ;
    public final void rule__Configuration__Author_emailAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2856:1: ( ( ruleAuthor_Email ) )
            // InternalRestfulWebservice.g:2857:2: ( ruleAuthor_Email )
            {
            // InternalRestfulWebservice.g:2857:2: ( ruleAuthor_Email )
            // InternalRestfulWebservice.g:2858:3: ruleAuthor_Email
            {
             before(grammarAccess.getConfigurationAccess().getAuthor_emailAuthor_EmailParserRuleCall_6_0()); 
            pushFollow(FOLLOW_2);
            ruleAuthor_Email();

            state._fsp--;

             after(grammarAccess.getConfigurationAccess().getAuthor_emailAuthor_EmailParserRuleCall_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Author_emailAssignment_6"


    // $ANTLR start "rule__Configuration__RepositoryAssignment_7"
    // InternalRestfulWebservice.g:2867:1: rule__Configuration__RepositoryAssignment_7 : ( ruleRepository ) ;
    public final void rule__Configuration__RepositoryAssignment_7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2871:1: ( ( ruleRepository ) )
            // InternalRestfulWebservice.g:2872:2: ( ruleRepository )
            {
            // InternalRestfulWebservice.g:2872:2: ( ruleRepository )
            // InternalRestfulWebservice.g:2873:3: ruleRepository
            {
             before(grammarAccess.getConfigurationAccess().getRepositoryRepositoryParserRuleCall_7_0()); 
            pushFollow(FOLLOW_2);
            ruleRepository();

            state._fsp--;

             after(grammarAccess.getConfigurationAccess().getRepositoryRepositoryParserRuleCall_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__RepositoryAssignment_7"


    // $ANTLR start "rule__Author__NameAssignment_1"
    // InternalRestfulWebservice.g:2882:1: rule__Author__NameAssignment_1 : ( RULE_STRING ) ;
    public final void rule__Author__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2886:1: ( ( RULE_STRING ) )
            // InternalRestfulWebservice.g:2887:2: ( RULE_STRING )
            {
            // InternalRestfulWebservice.g:2887:2: ( RULE_STRING )
            // InternalRestfulWebservice.g:2888:3: RULE_STRING
            {
             before(grammarAccess.getAuthorAccess().getNameSTRINGTerminalRuleCall_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getAuthorAccess().getNameSTRINGTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Author__NameAssignment_1"


    // $ANTLR start "rule__Author_Email__NameAssignment_1"
    // InternalRestfulWebservice.g:2897:1: rule__Author_Email__NameAssignment_1 : ( RULE_STRING ) ;
    public final void rule__Author_Email__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2901:1: ( ( RULE_STRING ) )
            // InternalRestfulWebservice.g:2902:2: ( RULE_STRING )
            {
            // InternalRestfulWebservice.g:2902:2: ( RULE_STRING )
            // InternalRestfulWebservice.g:2903:3: RULE_STRING
            {
             before(grammarAccess.getAuthor_EmailAccess().getNameSTRINGTerminalRuleCall_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getAuthor_EmailAccess().getNameSTRINGTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Author_Email__NameAssignment_1"


    // $ANTLR start "rule__Repository__NameAssignment_1"
    // InternalRestfulWebservice.g:2912:1: rule__Repository__NameAssignment_1 : ( RULE_STRING ) ;
    public final void rule__Repository__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2916:1: ( ( RULE_STRING ) )
            // InternalRestfulWebservice.g:2917:2: ( RULE_STRING )
            {
            // InternalRestfulWebservice.g:2917:2: ( RULE_STRING )
            // InternalRestfulWebservice.g:2918:3: RULE_STRING
            {
             before(grammarAccess.getRepositoryAccess().getNameSTRINGTerminalRuleCall_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getRepositoryAccess().getNameSTRINGTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Repository__NameAssignment_1"


    // $ANTLR start "rule__Lib__NameAssignment_1"
    // InternalRestfulWebservice.g:2927:1: rule__Lib__NameAssignment_1 : ( RULE_STRING ) ;
    public final void rule__Lib__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2931:1: ( ( RULE_STRING ) )
            // InternalRestfulWebservice.g:2932:2: ( RULE_STRING )
            {
            // InternalRestfulWebservice.g:2932:2: ( RULE_STRING )
            // InternalRestfulWebservice.g:2933:3: RULE_STRING
            {
             before(grammarAccess.getLibAccess().getNameSTRINGTerminalRuleCall_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getLibAccess().getNameSTRINGTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lib__NameAssignment_1"


    // $ANTLR start "rule__Software__NameAssignment_1"
    // InternalRestfulWebservice.g:2942:1: rule__Software__NameAssignment_1 : ( RULE_STRING ) ;
    public final void rule__Software__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2946:1: ( ( RULE_STRING ) )
            // InternalRestfulWebservice.g:2947:2: ( RULE_STRING )
            {
            // InternalRestfulWebservice.g:2947:2: ( RULE_STRING )
            // InternalRestfulWebservice.g:2948:3: RULE_STRING
            {
             before(grammarAccess.getSoftwareAccess().getNameSTRINGTerminalRuleCall_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getSoftwareAccess().getNameSTRINGTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Software__NameAssignment_1"


    // $ANTLR start "rule__About__NameAssignment_1"
    // InternalRestfulWebservice.g:2957:1: rule__About__NameAssignment_1 : ( RULE_STRING ) ;
    public final void rule__About__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2961:1: ( ( RULE_STRING ) )
            // InternalRestfulWebservice.g:2962:2: ( RULE_STRING )
            {
            // InternalRestfulWebservice.g:2962:2: ( RULE_STRING )
            // InternalRestfulWebservice.g:2963:3: RULE_STRING
            {
             before(grammarAccess.getAboutAccess().getNameSTRINGTerminalRuleCall_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getAboutAccess().getNameSTRINGTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__About__NameAssignment_1"


    // $ANTLR start "rule__Description__TextfieldAssignment_1"
    // InternalRestfulWebservice.g:2972:1: rule__Description__TextfieldAssignment_1 : ( RULE_STRING ) ;
    public final void rule__Description__TextfieldAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2976:1: ( ( RULE_STRING ) )
            // InternalRestfulWebservice.g:2977:2: ( RULE_STRING )
            {
            // InternalRestfulWebservice.g:2977:2: ( RULE_STRING )
            // InternalRestfulWebservice.g:2978:3: RULE_STRING
            {
             before(grammarAccess.getDescriptionAccess().getTextfieldSTRINGTerminalRuleCall_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getDescriptionAccess().getTextfieldSTRINGTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Description__TextfieldAssignment_1"


    // $ANTLR start "rule__Entity_Type__NameAssignment_1"
    // InternalRestfulWebservice.g:2987:1: rule__Entity_Type__NameAssignment_1 : ( RULE_STRING ) ;
    public final void rule__Entity_Type__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:2991:1: ( ( RULE_STRING ) )
            // InternalRestfulWebservice.g:2992:2: ( RULE_STRING )
            {
            // InternalRestfulWebservice.g:2992:2: ( RULE_STRING )
            // InternalRestfulWebservice.g:2993:3: RULE_STRING
            {
             before(grammarAccess.getEntity_TypeAccess().getNameSTRINGTerminalRuleCall_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getEntity_TypeAccess().getNameSTRINGTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity_Type__NameAssignment_1"


    // $ANTLR start "rule__Module__DescriptionAssignment_0"
    // InternalRestfulWebservice.g:3002:1: rule__Module__DescriptionAssignment_0 : ( ruleDescription ) ;
    public final void rule__Module__DescriptionAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:3006:1: ( ( ruleDescription ) )
            // InternalRestfulWebservice.g:3007:2: ( ruleDescription )
            {
            // InternalRestfulWebservice.g:3007:2: ( ruleDescription )
            // InternalRestfulWebservice.g:3008:3: ruleDescription
            {
             before(grammarAccess.getModuleAccess().getDescriptionDescriptionParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleDescription();

            state._fsp--;

             after(grammarAccess.getModuleAccess().getDescriptionDescriptionParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__DescriptionAssignment_0"


    // $ANTLR start "rule__Module__NameAssignment_2"
    // InternalRestfulWebservice.g:3017:1: rule__Module__NameAssignment_2 : ( ruleQualifiedName ) ;
    public final void rule__Module__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:3021:1: ( ( ruleQualifiedName ) )
            // InternalRestfulWebservice.g:3022:2: ( ruleQualifiedName )
            {
            // InternalRestfulWebservice.g:3022:2: ( ruleQualifiedName )
            // InternalRestfulWebservice.g:3023:3: ruleQualifiedName
            {
             before(grammarAccess.getModuleAccess().getNameQualifiedNameParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getModuleAccess().getNameQualifiedNameParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__NameAssignment_2"


    // $ANTLR start "rule__Module__ElementsAssignment_4"
    // InternalRestfulWebservice.g:3032:1: rule__Module__ElementsAssignment_4 : ( ruleAbstractElement ) ;
    public final void rule__Module__ElementsAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:3036:1: ( ( ruleAbstractElement ) )
            // InternalRestfulWebservice.g:3037:2: ( ruleAbstractElement )
            {
            // InternalRestfulWebservice.g:3037:2: ( ruleAbstractElement )
            // InternalRestfulWebservice.g:3038:3: ruleAbstractElement
            {
             before(grammarAccess.getModuleAccess().getElementsAbstractElementParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleAbstractElement();

            state._fsp--;

             after(grammarAccess.getModuleAccess().getElementsAbstractElementParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Module__ElementsAssignment_4"


    // $ANTLR start "rule__Import__ImportedNamespaceAssignment_1"
    // InternalRestfulWebservice.g:3047:1: rule__Import__ImportedNamespaceAssignment_1 : ( ruleQualifiedNameWithWildcard ) ;
    public final void rule__Import__ImportedNamespaceAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:3051:1: ( ( ruleQualifiedNameWithWildcard ) )
            // InternalRestfulWebservice.g:3052:2: ( ruleQualifiedNameWithWildcard )
            {
            // InternalRestfulWebservice.g:3052:2: ( ruleQualifiedNameWithWildcard )
            // InternalRestfulWebservice.g:3053:3: ruleQualifiedNameWithWildcard
            {
             before(grammarAccess.getImportAccess().getImportedNamespaceQualifiedNameWithWildcardParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleQualifiedNameWithWildcard();

            state._fsp--;

             after(grammarAccess.getImportAccess().getImportedNamespaceQualifiedNameWithWildcardParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__ImportedNamespaceAssignment_1"


    // $ANTLR start "rule__Entity__DescriptionAssignment_0"
    // InternalRestfulWebservice.g:3062:1: rule__Entity__DescriptionAssignment_0 : ( ruleDescription ) ;
    public final void rule__Entity__DescriptionAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:3066:1: ( ( ruleDescription ) )
            // InternalRestfulWebservice.g:3067:2: ( ruleDescription )
            {
            // InternalRestfulWebservice.g:3067:2: ( ruleDescription )
            // InternalRestfulWebservice.g:3068:3: ruleDescription
            {
             before(grammarAccess.getEntityAccess().getDescriptionDescriptionParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleDescription();

            state._fsp--;

             after(grammarAccess.getEntityAccess().getDescriptionDescriptionParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__DescriptionAssignment_0"


    // $ANTLR start "rule__Entity__NameAssignment_2"
    // InternalRestfulWebservice.g:3077:1: rule__Entity__NameAssignment_2 : ( RULE_ID ) ;
    public final void rule__Entity__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:3081:1: ( ( RULE_ID ) )
            // InternalRestfulWebservice.g:3082:2: ( RULE_ID )
            {
            // InternalRestfulWebservice.g:3082:2: ( RULE_ID )
            // InternalRestfulWebservice.g:3083:3: RULE_ID
            {
             before(grammarAccess.getEntityAccess().getNameIDTerminalRuleCall_2_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getEntityAccess().getNameIDTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__NameAssignment_2"


    // $ANTLR start "rule__Entity__SuperTypeAssignment_3_1"
    // InternalRestfulWebservice.g:3092:1: rule__Entity__SuperTypeAssignment_3_1 : ( ( ruleQualifiedName ) ) ;
    public final void rule__Entity__SuperTypeAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:3096:1: ( ( ( ruleQualifiedName ) ) )
            // InternalRestfulWebservice.g:3097:2: ( ( ruleQualifiedName ) )
            {
            // InternalRestfulWebservice.g:3097:2: ( ( ruleQualifiedName ) )
            // InternalRestfulWebservice.g:3098:3: ( ruleQualifiedName )
            {
             before(grammarAccess.getEntityAccess().getSuperTypeEntityCrossReference_3_1_0()); 
            // InternalRestfulWebservice.g:3099:3: ( ruleQualifiedName )
            // InternalRestfulWebservice.g:3100:4: ruleQualifiedName
            {
             before(grammarAccess.getEntityAccess().getSuperTypeEntityQualifiedNameParserRuleCall_3_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getEntityAccess().getSuperTypeEntityQualifiedNameParserRuleCall_3_1_0_1()); 

            }

             after(grammarAccess.getEntityAccess().getSuperTypeEntityCrossReference_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__SuperTypeAssignment_3_1"


    // $ANTLR start "rule__Entity__Entity_typeAssignment_5"
    // InternalRestfulWebservice.g:3111:1: rule__Entity__Entity_typeAssignment_5 : ( ruleEntity_Type ) ;
    public final void rule__Entity__Entity_typeAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:3115:1: ( ( ruleEntity_Type ) )
            // InternalRestfulWebservice.g:3116:2: ( ruleEntity_Type )
            {
            // InternalRestfulWebservice.g:3116:2: ( ruleEntity_Type )
            // InternalRestfulWebservice.g:3117:3: ruleEntity_Type
            {
             before(grammarAccess.getEntityAccess().getEntity_typeEntity_TypeParserRuleCall_5_0()); 
            pushFollow(FOLLOW_2);
            ruleEntity_Type();

            state._fsp--;

             after(grammarAccess.getEntityAccess().getEntity_typeEntity_TypeParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Entity_typeAssignment_5"


    // $ANTLR start "rule__Entity__AttributesAssignment_6"
    // InternalRestfulWebservice.g:3126:1: rule__Entity__AttributesAssignment_6 : ( ruleAttribute ) ;
    public final void rule__Entity__AttributesAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:3130:1: ( ( ruleAttribute ) )
            // InternalRestfulWebservice.g:3131:2: ( ruleAttribute )
            {
            // InternalRestfulWebservice.g:3131:2: ( ruleAttribute )
            // InternalRestfulWebservice.g:3132:3: ruleAttribute
            {
             before(grammarAccess.getEntityAccess().getAttributesAttributeParserRuleCall_6_0()); 
            pushFollow(FOLLOW_2);
            ruleAttribute();

            state._fsp--;

             after(grammarAccess.getEntityAccess().getAttributesAttributeParserRuleCall_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__AttributesAssignment_6"


    // $ANTLR start "rule__Entity__FunctionsAssignment_7"
    // InternalRestfulWebservice.g:3141:1: rule__Entity__FunctionsAssignment_7 : ( ruleFunction ) ;
    public final void rule__Entity__FunctionsAssignment_7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:3145:1: ( ( ruleFunction ) )
            // InternalRestfulWebservice.g:3146:2: ( ruleFunction )
            {
            // InternalRestfulWebservice.g:3146:2: ( ruleFunction )
            // InternalRestfulWebservice.g:3147:3: ruleFunction
            {
             before(grammarAccess.getEntityAccess().getFunctionsFunctionParserRuleCall_7_0()); 
            pushFollow(FOLLOW_2);
            ruleFunction();

            state._fsp--;

             after(grammarAccess.getEntityAccess().getFunctionsFunctionParserRuleCall_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__FunctionsAssignment_7"


    // $ANTLR start "rule__Entity__RelationsAssignment_8"
    // InternalRestfulWebservice.g:3156:1: rule__Entity__RelationsAssignment_8 : ( ruleRelation ) ;
    public final void rule__Entity__RelationsAssignment_8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:3160:1: ( ( ruleRelation ) )
            // InternalRestfulWebservice.g:3161:2: ( ruleRelation )
            {
            // InternalRestfulWebservice.g:3161:2: ( ruleRelation )
            // InternalRestfulWebservice.g:3162:3: ruleRelation
            {
             before(grammarAccess.getEntityAccess().getRelationsRelationParserRuleCall_8_0()); 
            pushFollow(FOLLOW_2);
            ruleRelation();

            state._fsp--;

             after(grammarAccess.getEntityAccess().getRelationsRelationParserRuleCall_8_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__RelationsAssignment_8"


    // $ANTLR start "rule__Attribute__DescriptionAssignment_0"
    // InternalRestfulWebservice.g:3171:1: rule__Attribute__DescriptionAssignment_0 : ( ruleDescription ) ;
    public final void rule__Attribute__DescriptionAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:3175:1: ( ( ruleDescription ) )
            // InternalRestfulWebservice.g:3176:2: ( ruleDescription )
            {
            // InternalRestfulWebservice.g:3176:2: ( ruleDescription )
            // InternalRestfulWebservice.g:3177:3: ruleDescription
            {
             before(grammarAccess.getAttributeAccess().getDescriptionDescriptionParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleDescription();

            state._fsp--;

             after(grammarAccess.getAttributeAccess().getDescriptionDescriptionParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__DescriptionAssignment_0"


    // $ANTLR start "rule__Attribute__NameAssignment_1"
    // InternalRestfulWebservice.g:3186:1: rule__Attribute__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Attribute__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:3190:1: ( ( RULE_ID ) )
            // InternalRestfulWebservice.g:3191:2: ( RULE_ID )
            {
            // InternalRestfulWebservice.g:3191:2: ( RULE_ID )
            // InternalRestfulWebservice.g:3192:3: RULE_ID
            {
             before(grammarAccess.getAttributeAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getAttributeAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__NameAssignment_1"


    // $ANTLR start "rule__Attribute__TypeAssignment_3"
    // InternalRestfulWebservice.g:3201:1: rule__Attribute__TypeAssignment_3 : ( RULE_ID ) ;
    public final void rule__Attribute__TypeAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:3205:1: ( ( RULE_ID ) )
            // InternalRestfulWebservice.g:3206:2: ( RULE_ID )
            {
            // InternalRestfulWebservice.g:3206:2: ( RULE_ID )
            // InternalRestfulWebservice.g:3207:3: RULE_ID
            {
             before(grammarAccess.getAttributeAccess().getTypeIDTerminalRuleCall_3_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getAttributeAccess().getTypeIDTerminalRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__TypeAssignment_3"


    // $ANTLR start "rule__OneToOne__NameAssignment_0"
    // InternalRestfulWebservice.g:3216:1: rule__OneToOne__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__OneToOne__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:3220:1: ( ( RULE_ID ) )
            // InternalRestfulWebservice.g:3221:2: ( RULE_ID )
            {
            // InternalRestfulWebservice.g:3221:2: ( RULE_ID )
            // InternalRestfulWebservice.g:3222:3: RULE_ID
            {
             before(grammarAccess.getOneToOneAccess().getNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getOneToOneAccess().getNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OneToOne__NameAssignment_0"


    // $ANTLR start "rule__OneToOne__TypeAssignment_2"
    // InternalRestfulWebservice.g:3231:1: rule__OneToOne__TypeAssignment_2 : ( ( RULE_ID ) ) ;
    public final void rule__OneToOne__TypeAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:3235:1: ( ( ( RULE_ID ) ) )
            // InternalRestfulWebservice.g:3236:2: ( ( RULE_ID ) )
            {
            // InternalRestfulWebservice.g:3236:2: ( ( RULE_ID ) )
            // InternalRestfulWebservice.g:3237:3: ( RULE_ID )
            {
             before(grammarAccess.getOneToOneAccess().getTypeEntityCrossReference_2_0()); 
            // InternalRestfulWebservice.g:3238:3: ( RULE_ID )
            // InternalRestfulWebservice.g:3239:4: RULE_ID
            {
             before(grammarAccess.getOneToOneAccess().getTypeEntityIDTerminalRuleCall_2_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getOneToOneAccess().getTypeEntityIDTerminalRuleCall_2_0_1()); 

            }

             after(grammarAccess.getOneToOneAccess().getTypeEntityCrossReference_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OneToOne__TypeAssignment_2"


    // $ANTLR start "rule__ManyToMany__NameAssignment_0"
    // InternalRestfulWebservice.g:3250:1: rule__ManyToMany__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__ManyToMany__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:3254:1: ( ( RULE_ID ) )
            // InternalRestfulWebservice.g:3255:2: ( RULE_ID )
            {
            // InternalRestfulWebservice.g:3255:2: ( RULE_ID )
            // InternalRestfulWebservice.g:3256:3: RULE_ID
            {
             before(grammarAccess.getManyToManyAccess().getNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getManyToManyAccess().getNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ManyToMany__NameAssignment_0"


    // $ANTLR start "rule__ManyToMany__TypeAssignment_2"
    // InternalRestfulWebservice.g:3265:1: rule__ManyToMany__TypeAssignment_2 : ( ( RULE_ID ) ) ;
    public final void rule__ManyToMany__TypeAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:3269:1: ( ( ( RULE_ID ) ) )
            // InternalRestfulWebservice.g:3270:2: ( ( RULE_ID ) )
            {
            // InternalRestfulWebservice.g:3270:2: ( ( RULE_ID ) )
            // InternalRestfulWebservice.g:3271:3: ( RULE_ID )
            {
             before(grammarAccess.getManyToManyAccess().getTypeEntityCrossReference_2_0()); 
            // InternalRestfulWebservice.g:3272:3: ( RULE_ID )
            // InternalRestfulWebservice.g:3273:4: RULE_ID
            {
             before(grammarAccess.getManyToManyAccess().getTypeEntityIDTerminalRuleCall_2_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getManyToManyAccess().getTypeEntityIDTerminalRuleCall_2_0_1()); 

            }

             after(grammarAccess.getManyToManyAccess().getTypeEntityCrossReference_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ManyToMany__TypeAssignment_2"


    // $ANTLR start "rule__OneToMany__NameAssignment_0"
    // InternalRestfulWebservice.g:3284:1: rule__OneToMany__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__OneToMany__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:3288:1: ( ( RULE_ID ) )
            // InternalRestfulWebservice.g:3289:2: ( RULE_ID )
            {
            // InternalRestfulWebservice.g:3289:2: ( RULE_ID )
            // InternalRestfulWebservice.g:3290:3: RULE_ID
            {
             before(grammarAccess.getOneToManyAccess().getNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getOneToManyAccess().getNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OneToMany__NameAssignment_0"


    // $ANTLR start "rule__OneToMany__TypeAssignment_2"
    // InternalRestfulWebservice.g:3299:1: rule__OneToMany__TypeAssignment_2 : ( ( RULE_ID ) ) ;
    public final void rule__OneToMany__TypeAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:3303:1: ( ( ( RULE_ID ) ) )
            // InternalRestfulWebservice.g:3304:2: ( ( RULE_ID ) )
            {
            // InternalRestfulWebservice.g:3304:2: ( ( RULE_ID ) )
            // InternalRestfulWebservice.g:3305:3: ( RULE_ID )
            {
             before(grammarAccess.getOneToManyAccess().getTypeEntityCrossReference_2_0()); 
            // InternalRestfulWebservice.g:3306:3: ( RULE_ID )
            // InternalRestfulWebservice.g:3307:4: RULE_ID
            {
             before(grammarAccess.getOneToManyAccess().getTypeEntityIDTerminalRuleCall_2_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getOneToManyAccess().getTypeEntityIDTerminalRuleCall_2_0_1()); 

            }

             after(grammarAccess.getOneToManyAccess().getTypeEntityCrossReference_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OneToMany__TypeAssignment_2"


    // $ANTLR start "rule__Function__DescriptionAssignment_0"
    // InternalRestfulWebservice.g:3318:1: rule__Function__DescriptionAssignment_0 : ( ruleDescription ) ;
    public final void rule__Function__DescriptionAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:3322:1: ( ( ruleDescription ) )
            // InternalRestfulWebservice.g:3323:2: ( ruleDescription )
            {
            // InternalRestfulWebservice.g:3323:2: ( ruleDescription )
            // InternalRestfulWebservice.g:3324:3: ruleDescription
            {
             before(grammarAccess.getFunctionAccess().getDescriptionDescriptionParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleDescription();

            state._fsp--;

             after(grammarAccess.getFunctionAccess().getDescriptionDescriptionParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__DescriptionAssignment_0"


    // $ANTLR start "rule__Function__NameAssignment_2"
    // InternalRestfulWebservice.g:3333:1: rule__Function__NameAssignment_2 : ( RULE_ID ) ;
    public final void rule__Function__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:3337:1: ( ( RULE_ID ) )
            // InternalRestfulWebservice.g:3338:2: ( RULE_ID )
            {
            // InternalRestfulWebservice.g:3338:2: ( RULE_ID )
            // InternalRestfulWebservice.g:3339:3: RULE_ID
            {
             before(grammarAccess.getFunctionAccess().getNameIDTerminalRuleCall_2_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getFunctionAccess().getNameIDTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__NameAssignment_2"


    // $ANTLR start "rule__Function__ParamsAssignment_4_0"
    // InternalRestfulWebservice.g:3348:1: rule__Function__ParamsAssignment_4_0 : ( RULE_ID ) ;
    public final void rule__Function__ParamsAssignment_4_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:3352:1: ( ( RULE_ID ) )
            // InternalRestfulWebservice.g:3353:2: ( RULE_ID )
            {
            // InternalRestfulWebservice.g:3353:2: ( RULE_ID )
            // InternalRestfulWebservice.g:3354:3: RULE_ID
            {
             before(grammarAccess.getFunctionAccess().getParamsIDTerminalRuleCall_4_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getFunctionAccess().getParamsIDTerminalRuleCall_4_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__ParamsAssignment_4_0"


    // $ANTLR start "rule__Function__ParamsAssignment_4_1_1"
    // InternalRestfulWebservice.g:3363:1: rule__Function__ParamsAssignment_4_1_1 : ( RULE_ID ) ;
    public final void rule__Function__ParamsAssignment_4_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:3367:1: ( ( RULE_ID ) )
            // InternalRestfulWebservice.g:3368:2: ( RULE_ID )
            {
            // InternalRestfulWebservice.g:3368:2: ( RULE_ID )
            // InternalRestfulWebservice.g:3369:3: RULE_ID
            {
             before(grammarAccess.getFunctionAccess().getParamsIDTerminalRuleCall_4_1_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getFunctionAccess().getParamsIDTerminalRuleCall_4_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__ParamsAssignment_4_1_1"


    // $ANTLR start "rule__Function__TypeAssignment_7"
    // InternalRestfulWebservice.g:3378:1: rule__Function__TypeAssignment_7 : ( RULE_ID ) ;
    public final void rule__Function__TypeAssignment_7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRestfulWebservice.g:3382:1: ( ( RULE_ID ) )
            // InternalRestfulWebservice.g:3383:2: ( RULE_ID )
            {
            // InternalRestfulWebservice.g:3383:2: ( RULE_ID )
            // InternalRestfulWebservice.g:3384:3: RULE_ID
            {
             before(grammarAccess.getFunctionAccess().getTypeIDTerminalRuleCall_7_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getFunctionAccess().getTypeIDTerminalRuleCall_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__TypeAssignment_7"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000005500000L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000005500002L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000500000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000005502000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000800002L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000004100000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000008001000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000100102010L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000000100012L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000100100002L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000000100010L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000000100100000L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x0000000400000010L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0000000800000000L});
    public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x0000000800000002L});

}