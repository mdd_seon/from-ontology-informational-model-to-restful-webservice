package br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.generator;

import br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.restfulWebservice.AbstractElement;
import br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.restfulWebservice.Attribute;
import br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.restfulWebservice.Configuration;
import br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.restfulWebservice.Description;
import br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.restfulWebservice.Entity;
import br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.restfulWebservice.Function;
import br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.restfulWebservice.ManyToMany;
import br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.restfulWebservice.OneToMany;
import br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.restfulWebservice.OneToOne;
import br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.restfulWebservice.Relation;
import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import java.util.ArrayList;
import java.util.List;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.generator.AbstractGenerator;
import org.eclipse.xtext.generator.IFileSystemAccess2;
import org.eclipse.xtext.generator.IGeneratorContext;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.IteratorExtensions;

@SuppressWarnings("all")
public class DocumentationGenerator extends AbstractGenerator {
  private String GITLAB_PATH = "https://gitlab.com/mdd_seon/from-ontology-informational-model-to-restful-webservice";
  
  @Override
  public void doGenerate(final Resource resource, final IFileSystemAccess2 fsa, final IGeneratorContext context) {
    List<Configuration> configurations = IterableExtensions.<Configuration>toList(Iterables.<Configuration>filter(IteratorExtensions.<EObject>toIterable(resource.getAllContents()), Configuration.class));
    final Configuration configuration = configurations.get(0);
    fsa.generateFile("/README.md", this.compile(configuration));
    Iterable<br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.restfulWebservice.Module> modules = Iterables.<br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.restfulWebservice.Module>filter(IteratorExtensions.<EObject>toIterable(resource.getAllContents()), br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.restfulWebservice.Module.class);
    fsa.generateFile("/docs/README.md", this.compile(modules));
    modules = Iterables.<br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.restfulWebservice.Module>filter(IteratorExtensions.<EObject>toIterable(resource.getAllContents()), br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.restfulWebservice.Module.class);
    fsa.generateFile("/docs/packagediagram.puml", this.createPackageDiagram(modules));
    Iterable<br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.restfulWebservice.Module> _filter = Iterables.<br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.restfulWebservice.Module>filter(IteratorExtensions.<EObject>toIterable(resource.getAllContents()), br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.restfulWebservice.Module.class);
    for (final br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.restfulWebservice.Module m : _filter) {
      {
        String _lowerCase = m.getName().toLowerCase();
        String _plus = ("/docs/" + _lowerCase);
        String _plus_1 = (_plus + "/README.md");
        fsa.generateFile(_plus_1, this.compile(m));
        String _lowerCase_1 = m.getName().toLowerCase();
        String _plus_2 = ("/docs/" + _lowerCase_1);
        String _plus_3 = (_plus_2 + "/classdiagram.puml");
        fsa.generateFile(_plus_3, this.createClassDiagram(m));
      }
    }
  }
  
  private CharSequence createPackageDiagram(final Iterable<br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.restfulWebservice.Module> modules) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("@startuml");
    _builder.newLine();
    {
      for(final br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.restfulWebservice.Module m : modules) {
        _builder.append("namespace ");
        String _name = m.getName();
        _builder.append(_name);
        _builder.append("{");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.newLine();
        _builder.append("}");
        _builder.newLine();
      }
    }
    _builder.append("@enduml");
    _builder.newLine();
    return _builder;
  }
  
  private CharSequence compile(final Iterable<br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.restfulWebservice.Module> modules) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("# Documentation");
    _builder.newLine();
    _builder.append("## Conceptual Model");
    _builder.newLine();
    _builder.append("![Domain Diagram](packagediagram.png)\t");
    _builder.newLine();
    _builder.append("\t\t\t\t\t\t");
    _builder.newLine();
    _builder.append("## Modules");
    _builder.newLine();
    {
      for(final br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.restfulWebservice.Module m : modules) {
        {
          Description _description = m.getDescription();
          boolean _tripleNotEquals = (_description != null);
          if (_tripleNotEquals) {
            _builder.append("* **[");
            String _name = m.getName();
            _builder.append(_name);
            _builder.append("](./");
            String _lowerCase = m.getName().toLowerCase();
            _builder.append(_lowerCase);
            _builder.append("/)** : ");
            String _textfield = m.getDescription().getTextfield();
            _builder.append(_textfield);
            _builder.append(" ");
            _builder.newLineIfNotEmpty();
            _builder.newLine();
          } else {
            _builder.append("* **[");
            String _name_1 = m.getName();
            _builder.append(_name_1);
            _builder.append("](./");
            String _lowerCase_1 = m.getName().toLowerCase();
            _builder.append(_lowerCase_1);
            _builder.append("/)** : - ");
            _builder.newLineIfNotEmpty();
            _builder.newLine();
          }
        }
      }
    }
    _builder.append("## Copyright");
    _builder.newLine();
    _builder.append("This Webservice was PowerRight by [SEON Application Lib Generator](");
    _builder.append(this.GITLAB_PATH);
    _builder.append(") ");
    _builder.newLineIfNotEmpty();
    return _builder;
  }
  
  private CharSequence createClassDiagram(final br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.restfulWebservice.Module m) {
    CharSequence _xblockexpression = null;
    {
      final List<Entity> entities = new ArrayList<Entity>();
      EList<AbstractElement> _elements = m.getElements();
      for (final AbstractElement e : _elements) {
        Class<?> _instanceClass = e.eClass().getInstanceClass();
        boolean _equals = Objects.equal(_instanceClass, Entity.class);
        if (_equals) {
          final Entity entity = ((Entity) e);
          entities.add(entity);
        }
      }
      StringConcatenation _builder = new StringConcatenation();
      CharSequence _createClassDiagram = this.createClassDiagram(entities);
      _builder.append(_createClassDiagram);
      _builder.newLineIfNotEmpty();
      _xblockexpression = _builder;
    }
    return _xblockexpression;
  }
  
  private CharSequence createClassDiagram(final List<Entity> entities) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("@startuml");
    _builder.newLine();
    {
      int _size = entities.size();
      boolean _greaterThan = (_size > 0);
      if (_greaterThan) {
        {
          for(final Entity e : entities) {
            _builder.append("class ");
            String _name = e.getName();
            _builder.append(_name);
            _builder.append("{");
            _builder.newLineIfNotEmpty();
            {
              EList<Attribute> _attributes = e.getAttributes();
              for(final Attribute a : _attributes) {
                _builder.append("\t");
                String _type = a.getType();
                _builder.append(_type, "\t");
                _builder.append(":");
                String _name_1 = a.getName();
                _builder.append(_name_1, "\t");
                _builder.newLineIfNotEmpty();
              }
            }
            {
              EList<Function> _functions = e.getFunctions();
              for(final Function f : _functions) {
                _builder.append("\t");
                int count = 0;
                _builder.newLineIfNotEmpty();
                _builder.append("\t");
                String _type_1 = f.getType();
                _builder.append(_type_1, "\t");
                _builder.append(": ");
                String _name_2 = f.getName();
                _builder.append(_name_2, "\t");
                _builder.append("(");
                {
                  EList<String> _params = f.getParams();
                  for(final String p : _params) {
                    _builder.append(p, "\t");
                    _builder.append(count = (count + 1), "\t");
                    {
                      int _size_1 = f.getParams().size();
                      boolean _lessThan = (count < _size_1);
                      if (_lessThan) {
                        _builder.append(",");
                      }
                    }
                  }
                }
                _builder.append(")");
                _builder.newLineIfNotEmpty();
              }
            }
            _builder.append("}");
            _builder.newLine();
            {
              Entity _superType = e.getSuperType();
              boolean _tripleNotEquals = (_superType != null);
              if (_tripleNotEquals) {
                String _name_3 = e.getSuperType().getName();
                _builder.append(_name_3);
                _builder.append(" <|-- ");
                String _name_4 = e.getName();
                _builder.append(_name_4);
              }
            }
            _builder.newLineIfNotEmpty();
            _builder.newLine();
            {
              EList<Relation> _relations = e.getRelations();
              for(final Relation r : _relations) {
                {
                  Class<?> _instanceClass = r.eClass().getInstanceClass();
                  boolean _equals = Objects.equal(_instanceClass, OneToOne.class);
                  if (_equals) {
                    String _name_5 = e.getName();
                    _builder.append(_name_5);
                    _builder.append(" \"1\" -- \"1\" ");
                    String _name_6 = r.getType().getName();
                    _builder.append(_name_6);
                    _builder.append(" : ");
                    String _lowerCase = r.getName().toLowerCase();
                    _builder.append(_lowerCase);
                    _builder.append(" >");
                    _builder.newLineIfNotEmpty();
                  }
                }
                {
                  Class<?> _instanceClass_1 = r.eClass().getInstanceClass();
                  boolean _equals_1 = Objects.equal(_instanceClass_1, ManyToMany.class);
                  if (_equals_1) {
                    String _name_7 = e.getName();
                    _builder.append(_name_7);
                    _builder.append(" \"0..*\" -- \"0..*\" ");
                    String _name_8 = r.getType().getName();
                    _builder.append(_name_8);
                    _builder.append(" : ");
                    String _lowerCase_1 = r.getName().toLowerCase();
                    _builder.append(_lowerCase_1);
                    _builder.append(" >");
                    _builder.newLineIfNotEmpty();
                  }
                }
                {
                  Class<?> _instanceClass_2 = r.eClass().getInstanceClass();
                  boolean _equals_2 = Objects.equal(_instanceClass_2, OneToMany.class);
                  if (_equals_2) {
                    String _name_9 = e.getName();
                    _builder.append(_name_9);
                    _builder.append(" \"1\" -- \"0..*\" ");
                    String _name_10 = r.getType().getName();
                    _builder.append(_name_10);
                    _builder.append(" : ");
                    String _lowerCase_2 = r.getName().toLowerCase();
                    _builder.append(_lowerCase_2);
                    _builder.append(" >");
                    _builder.newLineIfNotEmpty();
                  }
                }
              }
            }
            _builder.newLine();
          }
        }
      }
    }
    _builder.newLine();
    _builder.append("@enduml");
    _builder.newLine();
    return _builder;
  }
  
  private CharSequence compile(final br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.restfulWebservice.Module m) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("# Documentation: ");
    String _name = m.getName();
    _builder.append(_name);
    _builder.newLineIfNotEmpty();
    {
      Description _description = m.getDescription();
      boolean _notEquals = (!Objects.equal(_description, null));
      if (_notEquals) {
        String _textfield = m.getDescription().getTextfield();
        _builder.append(_textfield);
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("## Application Conceptual Data Model");
    _builder.newLine();
    _builder.append("![Domain Diagram](classdiagram.png)\t");
    _builder.newLine();
    _builder.append("\t\t\t\t");
    _builder.newLine();
    _builder.append("## Entities");
    _builder.newLine();
    {
      EList<AbstractElement> _elements = m.getElements();
      for(final AbstractElement e : _elements) {
        {
          Class<?> _instanceClass = e.eClass().getInstanceClass();
          boolean _equals = Objects.equal(_instanceClass, Entity.class);
          if (_equals) {
            final Entity entity = ((Entity) e);
            _builder.newLineIfNotEmpty();
            CharSequence _descriptionX = this.descriptionX(entity);
            _builder.append(_descriptionX);
            _builder.newLineIfNotEmpty();
          }
        }
      }
    }
    _builder.newLine();
    {
      EList<AbstractElement> _elements_1 = m.getElements();
      for(final AbstractElement e_1 : _elements_1) {
        {
          Class<?> _instanceClass_1 = e_1.eClass().getInstanceClass();
          boolean _equals_1 = Objects.equal(_instanceClass_1, Entity.class);
          if (_equals_1) {
            final Entity entity_1 = ((Entity) e_1);
            _builder.newLineIfNotEmpty();
            {
              if (((entity_1.getFunctions() != null) && (entity_1.getFunctions().size() > 0))) {
                _builder.append("# Functions of ");
                String _name_1 = entity_1.getName();
                _builder.append(_name_1);
                _builder.append(":\t ");
                _builder.newLineIfNotEmpty();
                {
                  EList<Function> _functions = entity_1.getFunctions();
                  for(final Function f : _functions) {
                    CharSequence _descriptionX_1 = this.descriptionX(f);
                    _builder.append(_descriptionX_1);
                    _builder.newLineIfNotEmpty();
                  }
                }
              }
            }
          }
        }
      }
    }
    _builder.append("## Copyright");
    _builder.newLine();
    _builder.append("This Webservice was Powered by [SEON Application Webservice Generator](");
    _builder.append(this.GITLAB_PATH);
    _builder.append(") ");
    _builder.newLineIfNotEmpty();
    return _builder;
  }
  
  private CharSequence descriptionX(final Function f) {
    StringConcatenation _builder = new StringConcatenation();
    {
      Description _description = f.getDescription();
      boolean _tripleNotEquals = (_description != null);
      if (_tripleNotEquals) {
        _builder.append("* **");
        String _name = f.getName();
        _builder.append(_name);
        _builder.append("** : ");
        String _textfield = f.getDescription().getTextfield();
        _builder.append(_textfield);
      } else {
        _builder.append(" * **");
        String _name_1 = f.getName();
        _builder.append(_name_1);
        _builder.append("** : -");
        _builder.newLineIfNotEmpty();
      }
    }
    return _builder;
  }
  
  private CharSequence descriptionX(final Entity e) {
    StringConcatenation _builder = new StringConcatenation();
    {
      Description _description = e.getDescription();
      boolean _tripleNotEquals = (_description != null);
      if (_tripleNotEquals) {
        _builder.append("* **");
        String _name = e.getName();
        _builder.append(_name);
        _builder.append("** : ");
        String _textfield = e.getDescription().getTextfield();
        _builder.append(_textfield);
      } else {
        _builder.append("* **");
        String _name_1 = e.getName();
        _builder.append(_name_1);
        _builder.append("** : -");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t");
    _builder.newLine();
    return _builder;
  }
  
  private CharSequence compile(final Configuration e) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("# ");
    String _upperCase = e.getSoftware().getName().toUpperCase();
    _builder.append(_upperCase);
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("## General Information");
    _builder.newLine();
    _builder.append("* **Software**:");
    String _name = e.getSoftware().getName();
    _builder.append(_name);
    _builder.newLineIfNotEmpty();
    _builder.append("* **Author**:");
    String _name_1 = e.getAuthor().getName();
    _builder.append(_name_1);
    _builder.newLineIfNotEmpty();
    _builder.append("* **Author\'s e-mail**:");
    String _name_2 = e.getAuthor_email().getName();
    _builder.append(_name_2);
    _builder.newLineIfNotEmpty();
    _builder.append("* **Source Repository**: [");
    String _name_3 = e.getRepository().getName();
    _builder.append(_name_3);
    _builder.append("](");
    String _name_4 = e.getRepository().getName();
    _builder.append(_name_4);
    _builder.append(")  ");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("## Goal");
    _builder.newLine();
    String _name_5 = e.getAbout().getName();
    _builder.append(_name_5);
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("## Documentation");
    _builder.newLine();
    _builder.newLine();
    _builder.append("The Documentation can be found in this [link](./docs/)");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("## Instalation");
    _builder.newLine();
    _builder.newLine();
    _builder.append("To install ");
    String _name_6 = e.getSoftware().getName();
    _builder.append(_name_6);
    _builder.append(", run this command in your terminal:");
    _builder.newLineIfNotEmpty();
    _builder.append("```bash");
    _builder.newLine();
    _builder.append("pip install -r requirements.txt ");
    _builder.newLine();
    _builder.append("```");
    _builder.newLine();
    _builder.newLine();
    _builder.append("## Usage");
    _builder.newLine();
    _builder.newLine();
    _builder.append("```bash");
    _builder.newLine();
    _builder.append("chmod +x run.sh");
    _builder.newLine();
    _builder.append("./run.sh");
    _builder.newLine();
    _builder.append("```");
    _builder.newLine();
    _builder.newLine();
    _builder.append("## Copyright");
    _builder.newLine();
    _builder.append("This webservice was PowerRight by [SEON Application Webservice Generator](");
    _builder.append(this.GITLAB_PATH);
    _builder.append(")");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    return _builder;
  }
}
