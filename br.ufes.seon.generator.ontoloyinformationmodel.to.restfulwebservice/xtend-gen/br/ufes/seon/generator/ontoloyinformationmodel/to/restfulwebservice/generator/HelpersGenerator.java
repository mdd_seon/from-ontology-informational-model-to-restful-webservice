package br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.generator;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.generator.AbstractGenerator;
import org.eclipse.xtext.generator.IFileSystemAccess2;
import org.eclipse.xtext.generator.IGeneratorContext;

@SuppressWarnings("all")
public class HelpersGenerator extends AbstractGenerator {
  @Override
  public void doGenerate(final Resource resource, final IFileSystemAccess2 fsa, final IGeneratorContext context) {
    fsa.generateFile("requirements.txt", this.createRequirements());
    fsa.generateFile("Dockerfile", this.createDockerFile());
    fsa.generateFile("start.sh", this.createStart());
  }
  
  private CharSequence createDockerFile() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("FROM tiangolo/uwsgi-nginx-flask:python3.6-alpine3.7");
    _builder.newLine();
    _builder.append("RUN apk --update add bash nano");
    _builder.newLine();
    _builder.append("COPY ./requirements.txt requirements.txt");
    _builder.newLine();
    _builder.append("RUN pip install -r requirements.txt");
    _builder.newLine();
    return _builder;
  }
  
  private CharSequence createRequirements() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("aniso8601==8.0.0");
    _builder.newLine();
    _builder.append("attrs==19.3.0");
    _builder.newLine();
    _builder.append("click==7.1.2");
    _builder.newLine();
    _builder.append("Flask==1.1.2");
    _builder.newLine();
    _builder.append("flask-restplus==0.13.0");
    _builder.newLine();
    _builder.append("importlib-metadata==1.6.0");
    _builder.newLine();
    _builder.append("itsdangerous==1.1.0");
    _builder.newLine();
    _builder.append("Jinja2==2.11.2");
    _builder.newLine();
    _builder.append("jsonschema==3.2.0");
    _builder.newLine();
    _builder.append("MarkupSafe==1.1.1");
    _builder.newLine();
    _builder.append("pyrsistent==0.16.0");
    _builder.newLine();
    _builder.append("pytz==2020.1");
    _builder.newLine();
    _builder.append("six==1.15.0");
    _builder.newLine();
    _builder.append("Werkzeug==0.16.1");
    _builder.newLine();
    _builder.append("zipp==3.1.0");
    _builder.newLine();
    return _builder;
  }
  
  private CharSequence createStart() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#!/bin/bash");
    _builder.newLine();
    _builder.append("app=\"docker.test\"");
    _builder.newLine();
    _builder.append("docker build -t ${app} .");
    _builder.newLine();
    _builder.append("docker run -d -p 56733:80 \\");
    _builder.newLine();
    _builder.append("  ");
    _builder.append("--name=${app} \\");
    _builder.newLine();
    _builder.append("  ");
    _builder.append("-v $PWD:/app ${app}");
    _builder.newLine();
    _builder.newLine();
    return _builder;
  }
}
