package br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.services.RestfulWebserviceGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalRestfulWebserviceParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'Configuration'", "'{'", "'}'", "'author:'", "'author_email:'", "'repository:'", "'lib_name:'", "'software:'", "'about:'", "'#'", "'is:'", "'module'", "'.'", "'import'", "'.*'", "'entity'", "'extends'", "':'", "'OneToOne'", "'ManyToMany'", "'OneToMany'", "'function'", "'('", "','", "')'"
    };
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__33=33;
    public static final int T__12=12;
    public static final int T__34=34;
    public static final int T__13=13;
    public static final int T__35=35;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_ID=5;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalRestfulWebserviceParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalRestfulWebserviceParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalRestfulWebserviceParser.tokenNames; }
    public String getGrammarFileName() { return "InternalRestfulWebservice.g"; }



     	private RestfulWebserviceGrammarAccess grammarAccess;

        public InternalRestfulWebserviceParser(TokenStream input, RestfulWebserviceGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Application";
       	}

       	@Override
       	protected RestfulWebserviceGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleApplication"
    // InternalRestfulWebservice.g:64:1: entryRuleApplication returns [EObject current=null] : iv_ruleApplication= ruleApplication EOF ;
    public final EObject entryRuleApplication() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleApplication = null;


        try {
            // InternalRestfulWebservice.g:64:52: (iv_ruleApplication= ruleApplication EOF )
            // InternalRestfulWebservice.g:65:2: iv_ruleApplication= ruleApplication EOF
            {
             newCompositeNode(grammarAccess.getApplicationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleApplication=ruleApplication();

            state._fsp--;

             current =iv_ruleApplication; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleApplication"


    // $ANTLR start "ruleApplication"
    // InternalRestfulWebservice.g:71:1: ruleApplication returns [EObject current=null] : ( ( (lv_configuration_0_0= ruleConfiguration ) )? ( (lv_abstractElements_1_0= ruleAbstractElement ) )* ) ;
    public final EObject ruleApplication() throws RecognitionException {
        EObject current = null;

        EObject lv_configuration_0_0 = null;

        EObject lv_abstractElements_1_0 = null;



        	enterRule();

        try {
            // InternalRestfulWebservice.g:77:2: ( ( ( (lv_configuration_0_0= ruleConfiguration ) )? ( (lv_abstractElements_1_0= ruleAbstractElement ) )* ) )
            // InternalRestfulWebservice.g:78:2: ( ( (lv_configuration_0_0= ruleConfiguration ) )? ( (lv_abstractElements_1_0= ruleAbstractElement ) )* )
            {
            // InternalRestfulWebservice.g:78:2: ( ( (lv_configuration_0_0= ruleConfiguration ) )? ( (lv_abstractElements_1_0= ruleAbstractElement ) )* )
            // InternalRestfulWebservice.g:79:3: ( (lv_configuration_0_0= ruleConfiguration ) )? ( (lv_abstractElements_1_0= ruleAbstractElement ) )*
            {
            // InternalRestfulWebservice.g:79:3: ( (lv_configuration_0_0= ruleConfiguration ) )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==11) ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // InternalRestfulWebservice.g:80:4: (lv_configuration_0_0= ruleConfiguration )
                    {
                    // InternalRestfulWebservice.g:80:4: (lv_configuration_0_0= ruleConfiguration )
                    // InternalRestfulWebservice.g:81:5: lv_configuration_0_0= ruleConfiguration
                    {

                    					newCompositeNode(grammarAccess.getApplicationAccess().getConfigurationConfigurationParserRuleCall_0_0());
                    				
                    pushFollow(FOLLOW_3);
                    lv_configuration_0_0=ruleConfiguration();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getApplicationRule());
                    					}
                    					set(
                    						current,
                    						"configuration",
                    						lv_configuration_0_0,
                    						"br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.RestfulWebservice.Configuration");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalRestfulWebservice.g:98:3: ( (lv_abstractElements_1_0= ruleAbstractElement ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==20||LA2_0==22||LA2_0==24||LA2_0==26) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalRestfulWebservice.g:99:4: (lv_abstractElements_1_0= ruleAbstractElement )
            	    {
            	    // InternalRestfulWebservice.g:99:4: (lv_abstractElements_1_0= ruleAbstractElement )
            	    // InternalRestfulWebservice.g:100:5: lv_abstractElements_1_0= ruleAbstractElement
            	    {

            	    					newCompositeNode(grammarAccess.getApplicationAccess().getAbstractElementsAbstractElementParserRuleCall_1_0());
            	    				
            	    pushFollow(FOLLOW_3);
            	    lv_abstractElements_1_0=ruleAbstractElement();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getApplicationRule());
            	    					}
            	    					add(
            	    						current,
            	    						"abstractElements",
            	    						lv_abstractElements_1_0,
            	    						"br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.RestfulWebservice.AbstractElement");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleApplication"


    // $ANTLR start "entryRuleConfiguration"
    // InternalRestfulWebservice.g:121:1: entryRuleConfiguration returns [EObject current=null] : iv_ruleConfiguration= ruleConfiguration EOF ;
    public final EObject entryRuleConfiguration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConfiguration = null;


        try {
            // InternalRestfulWebservice.g:121:54: (iv_ruleConfiguration= ruleConfiguration EOF )
            // InternalRestfulWebservice.g:122:2: iv_ruleConfiguration= ruleConfiguration EOF
            {
             newCompositeNode(grammarAccess.getConfigurationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleConfiguration=ruleConfiguration();

            state._fsp--;

             current =iv_ruleConfiguration; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConfiguration"


    // $ANTLR start "ruleConfiguration"
    // InternalRestfulWebservice.g:128:1: ruleConfiguration returns [EObject current=null] : (otherlv_0= 'Configuration' otherlv_1= '{' ( (lv_software_2_0= ruleSoftware ) ) ( (lv_about_3_0= ruleAbout ) ) ( (lv_lib_4_0= ruleLib ) ) ( (lv_author_5_0= ruleAuthor ) ) ( (lv_author_email_6_0= ruleAuthor_Email ) ) ( (lv_repository_7_0= ruleRepository ) ) otherlv_8= '}' ) ;
    public final EObject ruleConfiguration() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_8=null;
        EObject lv_software_2_0 = null;

        EObject lv_about_3_0 = null;

        EObject lv_lib_4_0 = null;

        EObject lv_author_5_0 = null;

        EObject lv_author_email_6_0 = null;

        EObject lv_repository_7_0 = null;



        	enterRule();

        try {
            // InternalRestfulWebservice.g:134:2: ( (otherlv_0= 'Configuration' otherlv_1= '{' ( (lv_software_2_0= ruleSoftware ) ) ( (lv_about_3_0= ruleAbout ) ) ( (lv_lib_4_0= ruleLib ) ) ( (lv_author_5_0= ruleAuthor ) ) ( (lv_author_email_6_0= ruleAuthor_Email ) ) ( (lv_repository_7_0= ruleRepository ) ) otherlv_8= '}' ) )
            // InternalRestfulWebservice.g:135:2: (otherlv_0= 'Configuration' otherlv_1= '{' ( (lv_software_2_0= ruleSoftware ) ) ( (lv_about_3_0= ruleAbout ) ) ( (lv_lib_4_0= ruleLib ) ) ( (lv_author_5_0= ruleAuthor ) ) ( (lv_author_email_6_0= ruleAuthor_Email ) ) ( (lv_repository_7_0= ruleRepository ) ) otherlv_8= '}' )
            {
            // InternalRestfulWebservice.g:135:2: (otherlv_0= 'Configuration' otherlv_1= '{' ( (lv_software_2_0= ruleSoftware ) ) ( (lv_about_3_0= ruleAbout ) ) ( (lv_lib_4_0= ruleLib ) ) ( (lv_author_5_0= ruleAuthor ) ) ( (lv_author_email_6_0= ruleAuthor_Email ) ) ( (lv_repository_7_0= ruleRepository ) ) otherlv_8= '}' )
            // InternalRestfulWebservice.g:136:3: otherlv_0= 'Configuration' otherlv_1= '{' ( (lv_software_2_0= ruleSoftware ) ) ( (lv_about_3_0= ruleAbout ) ) ( (lv_lib_4_0= ruleLib ) ) ( (lv_author_5_0= ruleAuthor ) ) ( (lv_author_email_6_0= ruleAuthor_Email ) ) ( (lv_repository_7_0= ruleRepository ) ) otherlv_8= '}'
            {
            otherlv_0=(Token)match(input,11,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getConfigurationAccess().getConfigurationKeyword_0());
            		
            otherlv_1=(Token)match(input,12,FOLLOW_5); 

            			newLeafNode(otherlv_1, grammarAccess.getConfigurationAccess().getLeftCurlyBracketKeyword_1());
            		
            // InternalRestfulWebservice.g:144:3: ( (lv_software_2_0= ruleSoftware ) )
            // InternalRestfulWebservice.g:145:4: (lv_software_2_0= ruleSoftware )
            {
            // InternalRestfulWebservice.g:145:4: (lv_software_2_0= ruleSoftware )
            // InternalRestfulWebservice.g:146:5: lv_software_2_0= ruleSoftware
            {

            					newCompositeNode(grammarAccess.getConfigurationAccess().getSoftwareSoftwareParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_6);
            lv_software_2_0=ruleSoftware();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getConfigurationRule());
            					}
            					set(
            						current,
            						"software",
            						lv_software_2_0,
            						"br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.RestfulWebservice.Software");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalRestfulWebservice.g:163:3: ( (lv_about_3_0= ruleAbout ) )
            // InternalRestfulWebservice.g:164:4: (lv_about_3_0= ruleAbout )
            {
            // InternalRestfulWebservice.g:164:4: (lv_about_3_0= ruleAbout )
            // InternalRestfulWebservice.g:165:5: lv_about_3_0= ruleAbout
            {

            					newCompositeNode(grammarAccess.getConfigurationAccess().getAboutAboutParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_7);
            lv_about_3_0=ruleAbout();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getConfigurationRule());
            					}
            					set(
            						current,
            						"about",
            						lv_about_3_0,
            						"br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.RestfulWebservice.About");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalRestfulWebservice.g:182:3: ( (lv_lib_4_0= ruleLib ) )
            // InternalRestfulWebservice.g:183:4: (lv_lib_4_0= ruleLib )
            {
            // InternalRestfulWebservice.g:183:4: (lv_lib_4_0= ruleLib )
            // InternalRestfulWebservice.g:184:5: lv_lib_4_0= ruleLib
            {

            					newCompositeNode(grammarAccess.getConfigurationAccess().getLibLibParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_8);
            lv_lib_4_0=ruleLib();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getConfigurationRule());
            					}
            					set(
            						current,
            						"lib",
            						lv_lib_4_0,
            						"br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.RestfulWebservice.Lib");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalRestfulWebservice.g:201:3: ( (lv_author_5_0= ruleAuthor ) )
            // InternalRestfulWebservice.g:202:4: (lv_author_5_0= ruleAuthor )
            {
            // InternalRestfulWebservice.g:202:4: (lv_author_5_0= ruleAuthor )
            // InternalRestfulWebservice.g:203:5: lv_author_5_0= ruleAuthor
            {

            					newCompositeNode(grammarAccess.getConfigurationAccess().getAuthorAuthorParserRuleCall_5_0());
            				
            pushFollow(FOLLOW_9);
            lv_author_5_0=ruleAuthor();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getConfigurationRule());
            					}
            					set(
            						current,
            						"author",
            						lv_author_5_0,
            						"br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.RestfulWebservice.Author");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalRestfulWebservice.g:220:3: ( (lv_author_email_6_0= ruleAuthor_Email ) )
            // InternalRestfulWebservice.g:221:4: (lv_author_email_6_0= ruleAuthor_Email )
            {
            // InternalRestfulWebservice.g:221:4: (lv_author_email_6_0= ruleAuthor_Email )
            // InternalRestfulWebservice.g:222:5: lv_author_email_6_0= ruleAuthor_Email
            {

            					newCompositeNode(grammarAccess.getConfigurationAccess().getAuthor_emailAuthor_EmailParserRuleCall_6_0());
            				
            pushFollow(FOLLOW_10);
            lv_author_email_6_0=ruleAuthor_Email();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getConfigurationRule());
            					}
            					set(
            						current,
            						"author_email",
            						lv_author_email_6_0,
            						"br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.RestfulWebservice.Author_Email");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalRestfulWebservice.g:239:3: ( (lv_repository_7_0= ruleRepository ) )
            // InternalRestfulWebservice.g:240:4: (lv_repository_7_0= ruleRepository )
            {
            // InternalRestfulWebservice.g:240:4: (lv_repository_7_0= ruleRepository )
            // InternalRestfulWebservice.g:241:5: lv_repository_7_0= ruleRepository
            {

            					newCompositeNode(grammarAccess.getConfigurationAccess().getRepositoryRepositoryParserRuleCall_7_0());
            				
            pushFollow(FOLLOW_11);
            lv_repository_7_0=ruleRepository();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getConfigurationRule());
            					}
            					set(
            						current,
            						"repository",
            						lv_repository_7_0,
            						"br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.RestfulWebservice.Repository");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_8=(Token)match(input,13,FOLLOW_2); 

            			newLeafNode(otherlv_8, grammarAccess.getConfigurationAccess().getRightCurlyBracketKeyword_8());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConfiguration"


    // $ANTLR start "entryRuleAuthor"
    // InternalRestfulWebservice.g:266:1: entryRuleAuthor returns [EObject current=null] : iv_ruleAuthor= ruleAuthor EOF ;
    public final EObject entryRuleAuthor() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAuthor = null;


        try {
            // InternalRestfulWebservice.g:266:47: (iv_ruleAuthor= ruleAuthor EOF )
            // InternalRestfulWebservice.g:267:2: iv_ruleAuthor= ruleAuthor EOF
            {
             newCompositeNode(grammarAccess.getAuthorRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAuthor=ruleAuthor();

            state._fsp--;

             current =iv_ruleAuthor; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAuthor"


    // $ANTLR start "ruleAuthor"
    // InternalRestfulWebservice.g:273:1: ruleAuthor returns [EObject current=null] : (otherlv_0= 'author:' ( (lv_name_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleAuthor() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;


        	enterRule();

        try {
            // InternalRestfulWebservice.g:279:2: ( (otherlv_0= 'author:' ( (lv_name_1_0= RULE_STRING ) ) ) )
            // InternalRestfulWebservice.g:280:2: (otherlv_0= 'author:' ( (lv_name_1_0= RULE_STRING ) ) )
            {
            // InternalRestfulWebservice.g:280:2: (otherlv_0= 'author:' ( (lv_name_1_0= RULE_STRING ) ) )
            // InternalRestfulWebservice.g:281:3: otherlv_0= 'author:' ( (lv_name_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,14,FOLLOW_12); 

            			newLeafNode(otherlv_0, grammarAccess.getAuthorAccess().getAuthorKeyword_0());
            		
            // InternalRestfulWebservice.g:285:3: ( (lv_name_1_0= RULE_STRING ) )
            // InternalRestfulWebservice.g:286:4: (lv_name_1_0= RULE_STRING )
            {
            // InternalRestfulWebservice.g:286:4: (lv_name_1_0= RULE_STRING )
            // InternalRestfulWebservice.g:287:5: lv_name_1_0= RULE_STRING
            {
            lv_name_1_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_name_1_0, grammarAccess.getAuthorAccess().getNameSTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAuthorRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAuthor"


    // $ANTLR start "entryRuleAuthor_Email"
    // InternalRestfulWebservice.g:307:1: entryRuleAuthor_Email returns [EObject current=null] : iv_ruleAuthor_Email= ruleAuthor_Email EOF ;
    public final EObject entryRuleAuthor_Email() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAuthor_Email = null;


        try {
            // InternalRestfulWebservice.g:307:53: (iv_ruleAuthor_Email= ruleAuthor_Email EOF )
            // InternalRestfulWebservice.g:308:2: iv_ruleAuthor_Email= ruleAuthor_Email EOF
            {
             newCompositeNode(grammarAccess.getAuthor_EmailRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAuthor_Email=ruleAuthor_Email();

            state._fsp--;

             current =iv_ruleAuthor_Email; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAuthor_Email"


    // $ANTLR start "ruleAuthor_Email"
    // InternalRestfulWebservice.g:314:1: ruleAuthor_Email returns [EObject current=null] : (otherlv_0= 'author_email:' ( (lv_name_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleAuthor_Email() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;


        	enterRule();

        try {
            // InternalRestfulWebservice.g:320:2: ( (otherlv_0= 'author_email:' ( (lv_name_1_0= RULE_STRING ) ) ) )
            // InternalRestfulWebservice.g:321:2: (otherlv_0= 'author_email:' ( (lv_name_1_0= RULE_STRING ) ) )
            {
            // InternalRestfulWebservice.g:321:2: (otherlv_0= 'author_email:' ( (lv_name_1_0= RULE_STRING ) ) )
            // InternalRestfulWebservice.g:322:3: otherlv_0= 'author_email:' ( (lv_name_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,15,FOLLOW_12); 

            			newLeafNode(otherlv_0, grammarAccess.getAuthor_EmailAccess().getAuthor_emailKeyword_0());
            		
            // InternalRestfulWebservice.g:326:3: ( (lv_name_1_0= RULE_STRING ) )
            // InternalRestfulWebservice.g:327:4: (lv_name_1_0= RULE_STRING )
            {
            // InternalRestfulWebservice.g:327:4: (lv_name_1_0= RULE_STRING )
            // InternalRestfulWebservice.g:328:5: lv_name_1_0= RULE_STRING
            {
            lv_name_1_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_name_1_0, grammarAccess.getAuthor_EmailAccess().getNameSTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAuthor_EmailRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAuthor_Email"


    // $ANTLR start "entryRuleRepository"
    // InternalRestfulWebservice.g:348:1: entryRuleRepository returns [EObject current=null] : iv_ruleRepository= ruleRepository EOF ;
    public final EObject entryRuleRepository() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRepository = null;


        try {
            // InternalRestfulWebservice.g:348:51: (iv_ruleRepository= ruleRepository EOF )
            // InternalRestfulWebservice.g:349:2: iv_ruleRepository= ruleRepository EOF
            {
             newCompositeNode(grammarAccess.getRepositoryRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRepository=ruleRepository();

            state._fsp--;

             current =iv_ruleRepository; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRepository"


    // $ANTLR start "ruleRepository"
    // InternalRestfulWebservice.g:355:1: ruleRepository returns [EObject current=null] : (otherlv_0= 'repository:' ( (lv_name_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleRepository() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;


        	enterRule();

        try {
            // InternalRestfulWebservice.g:361:2: ( (otherlv_0= 'repository:' ( (lv_name_1_0= RULE_STRING ) ) ) )
            // InternalRestfulWebservice.g:362:2: (otherlv_0= 'repository:' ( (lv_name_1_0= RULE_STRING ) ) )
            {
            // InternalRestfulWebservice.g:362:2: (otherlv_0= 'repository:' ( (lv_name_1_0= RULE_STRING ) ) )
            // InternalRestfulWebservice.g:363:3: otherlv_0= 'repository:' ( (lv_name_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,16,FOLLOW_12); 

            			newLeafNode(otherlv_0, grammarAccess.getRepositoryAccess().getRepositoryKeyword_0());
            		
            // InternalRestfulWebservice.g:367:3: ( (lv_name_1_0= RULE_STRING ) )
            // InternalRestfulWebservice.g:368:4: (lv_name_1_0= RULE_STRING )
            {
            // InternalRestfulWebservice.g:368:4: (lv_name_1_0= RULE_STRING )
            // InternalRestfulWebservice.g:369:5: lv_name_1_0= RULE_STRING
            {
            lv_name_1_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_name_1_0, grammarAccess.getRepositoryAccess().getNameSTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getRepositoryRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRepository"


    // $ANTLR start "entryRuleLib"
    // InternalRestfulWebservice.g:389:1: entryRuleLib returns [EObject current=null] : iv_ruleLib= ruleLib EOF ;
    public final EObject entryRuleLib() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLib = null;


        try {
            // InternalRestfulWebservice.g:389:44: (iv_ruleLib= ruleLib EOF )
            // InternalRestfulWebservice.g:390:2: iv_ruleLib= ruleLib EOF
            {
             newCompositeNode(grammarAccess.getLibRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLib=ruleLib();

            state._fsp--;

             current =iv_ruleLib; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLib"


    // $ANTLR start "ruleLib"
    // InternalRestfulWebservice.g:396:1: ruleLib returns [EObject current=null] : (otherlv_0= 'lib_name:' ( (lv_name_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleLib() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;


        	enterRule();

        try {
            // InternalRestfulWebservice.g:402:2: ( (otherlv_0= 'lib_name:' ( (lv_name_1_0= RULE_STRING ) ) ) )
            // InternalRestfulWebservice.g:403:2: (otherlv_0= 'lib_name:' ( (lv_name_1_0= RULE_STRING ) ) )
            {
            // InternalRestfulWebservice.g:403:2: (otherlv_0= 'lib_name:' ( (lv_name_1_0= RULE_STRING ) ) )
            // InternalRestfulWebservice.g:404:3: otherlv_0= 'lib_name:' ( (lv_name_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,17,FOLLOW_12); 

            			newLeafNode(otherlv_0, grammarAccess.getLibAccess().getLib_nameKeyword_0());
            		
            // InternalRestfulWebservice.g:408:3: ( (lv_name_1_0= RULE_STRING ) )
            // InternalRestfulWebservice.g:409:4: (lv_name_1_0= RULE_STRING )
            {
            // InternalRestfulWebservice.g:409:4: (lv_name_1_0= RULE_STRING )
            // InternalRestfulWebservice.g:410:5: lv_name_1_0= RULE_STRING
            {
            lv_name_1_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_name_1_0, grammarAccess.getLibAccess().getNameSTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getLibRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLib"


    // $ANTLR start "entryRuleSoftware"
    // InternalRestfulWebservice.g:430:1: entryRuleSoftware returns [EObject current=null] : iv_ruleSoftware= ruleSoftware EOF ;
    public final EObject entryRuleSoftware() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSoftware = null;


        try {
            // InternalRestfulWebservice.g:430:49: (iv_ruleSoftware= ruleSoftware EOF )
            // InternalRestfulWebservice.g:431:2: iv_ruleSoftware= ruleSoftware EOF
            {
             newCompositeNode(grammarAccess.getSoftwareRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSoftware=ruleSoftware();

            state._fsp--;

             current =iv_ruleSoftware; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSoftware"


    // $ANTLR start "ruleSoftware"
    // InternalRestfulWebservice.g:437:1: ruleSoftware returns [EObject current=null] : (otherlv_0= 'software:' ( (lv_name_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleSoftware() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;


        	enterRule();

        try {
            // InternalRestfulWebservice.g:443:2: ( (otherlv_0= 'software:' ( (lv_name_1_0= RULE_STRING ) ) ) )
            // InternalRestfulWebservice.g:444:2: (otherlv_0= 'software:' ( (lv_name_1_0= RULE_STRING ) ) )
            {
            // InternalRestfulWebservice.g:444:2: (otherlv_0= 'software:' ( (lv_name_1_0= RULE_STRING ) ) )
            // InternalRestfulWebservice.g:445:3: otherlv_0= 'software:' ( (lv_name_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,18,FOLLOW_12); 

            			newLeafNode(otherlv_0, grammarAccess.getSoftwareAccess().getSoftwareKeyword_0());
            		
            // InternalRestfulWebservice.g:449:3: ( (lv_name_1_0= RULE_STRING ) )
            // InternalRestfulWebservice.g:450:4: (lv_name_1_0= RULE_STRING )
            {
            // InternalRestfulWebservice.g:450:4: (lv_name_1_0= RULE_STRING )
            // InternalRestfulWebservice.g:451:5: lv_name_1_0= RULE_STRING
            {
            lv_name_1_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_name_1_0, grammarAccess.getSoftwareAccess().getNameSTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getSoftwareRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSoftware"


    // $ANTLR start "entryRuleAbout"
    // InternalRestfulWebservice.g:471:1: entryRuleAbout returns [EObject current=null] : iv_ruleAbout= ruleAbout EOF ;
    public final EObject entryRuleAbout() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAbout = null;


        try {
            // InternalRestfulWebservice.g:471:46: (iv_ruleAbout= ruleAbout EOF )
            // InternalRestfulWebservice.g:472:2: iv_ruleAbout= ruleAbout EOF
            {
             newCompositeNode(grammarAccess.getAboutRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAbout=ruleAbout();

            state._fsp--;

             current =iv_ruleAbout; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAbout"


    // $ANTLR start "ruleAbout"
    // InternalRestfulWebservice.g:478:1: ruleAbout returns [EObject current=null] : (otherlv_0= 'about:' ( (lv_name_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleAbout() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;


        	enterRule();

        try {
            // InternalRestfulWebservice.g:484:2: ( (otherlv_0= 'about:' ( (lv_name_1_0= RULE_STRING ) ) ) )
            // InternalRestfulWebservice.g:485:2: (otherlv_0= 'about:' ( (lv_name_1_0= RULE_STRING ) ) )
            {
            // InternalRestfulWebservice.g:485:2: (otherlv_0= 'about:' ( (lv_name_1_0= RULE_STRING ) ) )
            // InternalRestfulWebservice.g:486:3: otherlv_0= 'about:' ( (lv_name_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,19,FOLLOW_12); 

            			newLeafNode(otherlv_0, grammarAccess.getAboutAccess().getAboutKeyword_0());
            		
            // InternalRestfulWebservice.g:490:3: ( (lv_name_1_0= RULE_STRING ) )
            // InternalRestfulWebservice.g:491:4: (lv_name_1_0= RULE_STRING )
            {
            // InternalRestfulWebservice.g:491:4: (lv_name_1_0= RULE_STRING )
            // InternalRestfulWebservice.g:492:5: lv_name_1_0= RULE_STRING
            {
            lv_name_1_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_name_1_0, grammarAccess.getAboutAccess().getNameSTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAboutRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAbout"


    // $ANTLR start "entryRuleDescription"
    // InternalRestfulWebservice.g:512:1: entryRuleDescription returns [EObject current=null] : iv_ruleDescription= ruleDescription EOF ;
    public final EObject entryRuleDescription() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDescription = null;


        try {
            // InternalRestfulWebservice.g:512:52: (iv_ruleDescription= ruleDescription EOF )
            // InternalRestfulWebservice.g:513:2: iv_ruleDescription= ruleDescription EOF
            {
             newCompositeNode(grammarAccess.getDescriptionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDescription=ruleDescription();

            state._fsp--;

             current =iv_ruleDescription; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDescription"


    // $ANTLR start "ruleDescription"
    // InternalRestfulWebservice.g:519:1: ruleDescription returns [EObject current=null] : (otherlv_0= '#' ( (lv_textfield_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleDescription() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_textfield_1_0=null;


        	enterRule();

        try {
            // InternalRestfulWebservice.g:525:2: ( (otherlv_0= '#' ( (lv_textfield_1_0= RULE_STRING ) ) ) )
            // InternalRestfulWebservice.g:526:2: (otherlv_0= '#' ( (lv_textfield_1_0= RULE_STRING ) ) )
            {
            // InternalRestfulWebservice.g:526:2: (otherlv_0= '#' ( (lv_textfield_1_0= RULE_STRING ) ) )
            // InternalRestfulWebservice.g:527:3: otherlv_0= '#' ( (lv_textfield_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,20,FOLLOW_12); 

            			newLeafNode(otherlv_0, grammarAccess.getDescriptionAccess().getNumberSignKeyword_0());
            		
            // InternalRestfulWebservice.g:531:3: ( (lv_textfield_1_0= RULE_STRING ) )
            // InternalRestfulWebservice.g:532:4: (lv_textfield_1_0= RULE_STRING )
            {
            // InternalRestfulWebservice.g:532:4: (lv_textfield_1_0= RULE_STRING )
            // InternalRestfulWebservice.g:533:5: lv_textfield_1_0= RULE_STRING
            {
            lv_textfield_1_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_textfield_1_0, grammarAccess.getDescriptionAccess().getTextfieldSTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getDescriptionRule());
            					}
            					setWithLastConsumed(
            						current,
            						"textfield",
            						lv_textfield_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDescription"


    // $ANTLR start "entryRuleEntity_Type"
    // InternalRestfulWebservice.g:553:1: entryRuleEntity_Type returns [EObject current=null] : iv_ruleEntity_Type= ruleEntity_Type EOF ;
    public final EObject entryRuleEntity_Type() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEntity_Type = null;


        try {
            // InternalRestfulWebservice.g:553:52: (iv_ruleEntity_Type= ruleEntity_Type EOF )
            // InternalRestfulWebservice.g:554:2: iv_ruleEntity_Type= ruleEntity_Type EOF
            {
             newCompositeNode(grammarAccess.getEntity_TypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEntity_Type=ruleEntity_Type();

            state._fsp--;

             current =iv_ruleEntity_Type; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEntity_Type"


    // $ANTLR start "ruleEntity_Type"
    // InternalRestfulWebservice.g:560:1: ruleEntity_Type returns [EObject current=null] : (otherlv_0= 'is:' ( (lv_name_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleEntity_Type() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;


        	enterRule();

        try {
            // InternalRestfulWebservice.g:566:2: ( (otherlv_0= 'is:' ( (lv_name_1_0= RULE_STRING ) ) ) )
            // InternalRestfulWebservice.g:567:2: (otherlv_0= 'is:' ( (lv_name_1_0= RULE_STRING ) ) )
            {
            // InternalRestfulWebservice.g:567:2: (otherlv_0= 'is:' ( (lv_name_1_0= RULE_STRING ) ) )
            // InternalRestfulWebservice.g:568:3: otherlv_0= 'is:' ( (lv_name_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,21,FOLLOW_12); 

            			newLeafNode(otherlv_0, grammarAccess.getEntity_TypeAccess().getIsKeyword_0());
            		
            // InternalRestfulWebservice.g:572:3: ( (lv_name_1_0= RULE_STRING ) )
            // InternalRestfulWebservice.g:573:4: (lv_name_1_0= RULE_STRING )
            {
            // InternalRestfulWebservice.g:573:4: (lv_name_1_0= RULE_STRING )
            // InternalRestfulWebservice.g:574:5: lv_name_1_0= RULE_STRING
            {
            lv_name_1_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_name_1_0, grammarAccess.getEntity_TypeAccess().getNameSTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getEntity_TypeRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEntity_Type"


    // $ANTLR start "entryRuleModule"
    // InternalRestfulWebservice.g:594:1: entryRuleModule returns [EObject current=null] : iv_ruleModule= ruleModule EOF ;
    public final EObject entryRuleModule() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleModule = null;


        try {
            // InternalRestfulWebservice.g:594:47: (iv_ruleModule= ruleModule EOF )
            // InternalRestfulWebservice.g:595:2: iv_ruleModule= ruleModule EOF
            {
             newCompositeNode(grammarAccess.getModuleRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleModule=ruleModule();

            state._fsp--;

             current =iv_ruleModule; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleModule"


    // $ANTLR start "ruleModule"
    // InternalRestfulWebservice.g:601:1: ruleModule returns [EObject current=null] : ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'module' ( (lv_name_2_0= ruleQualifiedName ) ) otherlv_3= '{' ( (lv_elements_4_0= ruleAbstractElement ) )* otherlv_5= '}' ) ;
    public final EObject ruleModule() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_description_0_0 = null;

        AntlrDatatypeRuleToken lv_name_2_0 = null;

        EObject lv_elements_4_0 = null;



        	enterRule();

        try {
            // InternalRestfulWebservice.g:607:2: ( ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'module' ( (lv_name_2_0= ruleQualifiedName ) ) otherlv_3= '{' ( (lv_elements_4_0= ruleAbstractElement ) )* otherlv_5= '}' ) )
            // InternalRestfulWebservice.g:608:2: ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'module' ( (lv_name_2_0= ruleQualifiedName ) ) otherlv_3= '{' ( (lv_elements_4_0= ruleAbstractElement ) )* otherlv_5= '}' )
            {
            // InternalRestfulWebservice.g:608:2: ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'module' ( (lv_name_2_0= ruleQualifiedName ) ) otherlv_3= '{' ( (lv_elements_4_0= ruleAbstractElement ) )* otherlv_5= '}' )
            // InternalRestfulWebservice.g:609:3: ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'module' ( (lv_name_2_0= ruleQualifiedName ) ) otherlv_3= '{' ( (lv_elements_4_0= ruleAbstractElement ) )* otherlv_5= '}'
            {
            // InternalRestfulWebservice.g:609:3: ( (lv_description_0_0= ruleDescription ) )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==20) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // InternalRestfulWebservice.g:610:4: (lv_description_0_0= ruleDescription )
                    {
                    // InternalRestfulWebservice.g:610:4: (lv_description_0_0= ruleDescription )
                    // InternalRestfulWebservice.g:611:5: lv_description_0_0= ruleDescription
                    {

                    					newCompositeNode(grammarAccess.getModuleAccess().getDescriptionDescriptionParserRuleCall_0_0());
                    				
                    pushFollow(FOLLOW_13);
                    lv_description_0_0=ruleDescription();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getModuleRule());
                    					}
                    					set(
                    						current,
                    						"description",
                    						lv_description_0_0,
                    						"br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.RestfulWebservice.Description");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            otherlv_1=(Token)match(input,22,FOLLOW_14); 

            			newLeafNode(otherlv_1, grammarAccess.getModuleAccess().getModuleKeyword_1());
            		
            // InternalRestfulWebservice.g:632:3: ( (lv_name_2_0= ruleQualifiedName ) )
            // InternalRestfulWebservice.g:633:4: (lv_name_2_0= ruleQualifiedName )
            {
            // InternalRestfulWebservice.g:633:4: (lv_name_2_0= ruleQualifiedName )
            // InternalRestfulWebservice.g:634:5: lv_name_2_0= ruleQualifiedName
            {

            					newCompositeNode(grammarAccess.getModuleAccess().getNameQualifiedNameParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_2_0=ruleQualifiedName();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getModuleRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.RestfulWebservice.QualifiedName");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_15); 

            			newLeafNode(otherlv_3, grammarAccess.getModuleAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalRestfulWebservice.g:655:3: ( (lv_elements_4_0= ruleAbstractElement ) )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==20||LA4_0==22||LA4_0==24||LA4_0==26) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalRestfulWebservice.g:656:4: (lv_elements_4_0= ruleAbstractElement )
            	    {
            	    // InternalRestfulWebservice.g:656:4: (lv_elements_4_0= ruleAbstractElement )
            	    // InternalRestfulWebservice.g:657:5: lv_elements_4_0= ruleAbstractElement
            	    {

            	    					newCompositeNode(grammarAccess.getModuleAccess().getElementsAbstractElementParserRuleCall_4_0());
            	    				
            	    pushFollow(FOLLOW_15);
            	    lv_elements_4_0=ruleAbstractElement();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getModuleRule());
            	    					}
            	    					add(
            	    						current,
            	    						"elements",
            	    						lv_elements_4_0,
            	    						"br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.RestfulWebservice.AbstractElement");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

            otherlv_5=(Token)match(input,13,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getModuleAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleModule"


    // $ANTLR start "entryRuleAbstractElement"
    // InternalRestfulWebservice.g:682:1: entryRuleAbstractElement returns [EObject current=null] : iv_ruleAbstractElement= ruleAbstractElement EOF ;
    public final EObject entryRuleAbstractElement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAbstractElement = null;


        try {
            // InternalRestfulWebservice.g:682:56: (iv_ruleAbstractElement= ruleAbstractElement EOF )
            // InternalRestfulWebservice.g:683:2: iv_ruleAbstractElement= ruleAbstractElement EOF
            {
             newCompositeNode(grammarAccess.getAbstractElementRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAbstractElement=ruleAbstractElement();

            state._fsp--;

             current =iv_ruleAbstractElement; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAbstractElement"


    // $ANTLR start "ruleAbstractElement"
    // InternalRestfulWebservice.g:689:1: ruleAbstractElement returns [EObject current=null] : (this_Module_0= ruleModule | this_Entity_1= ruleEntity | this_Import_2= ruleImport ) ;
    public final EObject ruleAbstractElement() throws RecognitionException {
        EObject current = null;

        EObject this_Module_0 = null;

        EObject this_Entity_1 = null;

        EObject this_Import_2 = null;



        	enterRule();

        try {
            // InternalRestfulWebservice.g:695:2: ( (this_Module_0= ruleModule | this_Entity_1= ruleEntity | this_Import_2= ruleImport ) )
            // InternalRestfulWebservice.g:696:2: (this_Module_0= ruleModule | this_Entity_1= ruleEntity | this_Import_2= ruleImport )
            {
            // InternalRestfulWebservice.g:696:2: (this_Module_0= ruleModule | this_Entity_1= ruleEntity | this_Import_2= ruleImport )
            int alt5=3;
            switch ( input.LA(1) ) {
            case 20:
                {
                int LA5_1 = input.LA(2);

                if ( (LA5_1==RULE_STRING) ) {
                    int LA5_5 = input.LA(3);

                    if ( (LA5_5==22) ) {
                        alt5=1;
                    }
                    else if ( (LA5_5==26) ) {
                        alt5=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 5, 5, input);

                        throw nvae;
                    }
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 5, 1, input);

                    throw nvae;
                }
                }
                break;
            case 22:
                {
                alt5=1;
                }
                break;
            case 26:
                {
                alt5=2;
                }
                break;
            case 24:
                {
                alt5=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }

            switch (alt5) {
                case 1 :
                    // InternalRestfulWebservice.g:697:3: this_Module_0= ruleModule
                    {

                    			newCompositeNode(grammarAccess.getAbstractElementAccess().getModuleParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_Module_0=ruleModule();

                    state._fsp--;


                    			current = this_Module_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalRestfulWebservice.g:706:3: this_Entity_1= ruleEntity
                    {

                    			newCompositeNode(grammarAccess.getAbstractElementAccess().getEntityParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_Entity_1=ruleEntity();

                    state._fsp--;


                    			current = this_Entity_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalRestfulWebservice.g:715:3: this_Import_2= ruleImport
                    {

                    			newCompositeNode(grammarAccess.getAbstractElementAccess().getImportParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_Import_2=ruleImport();

                    state._fsp--;


                    			current = this_Import_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAbstractElement"


    // $ANTLR start "entryRuleQualifiedName"
    // InternalRestfulWebservice.g:727:1: entryRuleQualifiedName returns [String current=null] : iv_ruleQualifiedName= ruleQualifiedName EOF ;
    public final String entryRuleQualifiedName() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQualifiedName = null;


        try {
            // InternalRestfulWebservice.g:727:53: (iv_ruleQualifiedName= ruleQualifiedName EOF )
            // InternalRestfulWebservice.g:728:2: iv_ruleQualifiedName= ruleQualifiedName EOF
            {
             newCompositeNode(grammarAccess.getQualifiedNameRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQualifiedName=ruleQualifiedName();

            state._fsp--;

             current =iv_ruleQualifiedName.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQualifiedName"


    // $ANTLR start "ruleQualifiedName"
    // InternalRestfulWebservice.g:734:1: ruleQualifiedName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) ;
    public final AntlrDatatypeRuleToken ruleQualifiedName() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        Token kw=null;
        Token this_ID_2=null;


        	enterRule();

        try {
            // InternalRestfulWebservice.g:740:2: ( (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) )
            // InternalRestfulWebservice.g:741:2: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            {
            // InternalRestfulWebservice.g:741:2: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            // InternalRestfulWebservice.g:742:3: this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )*
            {
            this_ID_0=(Token)match(input,RULE_ID,FOLLOW_16); 

            			current.merge(this_ID_0);
            		

            			newLeafNode(this_ID_0, grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0());
            		
            // InternalRestfulWebservice.g:749:3: (kw= '.' this_ID_2= RULE_ID )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==23) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalRestfulWebservice.g:750:4: kw= '.' this_ID_2= RULE_ID
            	    {
            	    kw=(Token)match(input,23,FOLLOW_14); 

            	    				current.merge(kw);
            	    				newLeafNode(kw, grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0());
            	    			
            	    this_ID_2=(Token)match(input,RULE_ID,FOLLOW_16); 

            	    				current.merge(this_ID_2);
            	    			

            	    				newLeafNode(this_ID_2, grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1());
            	    			

            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQualifiedName"


    // $ANTLR start "entryRuleImport"
    // InternalRestfulWebservice.g:767:1: entryRuleImport returns [EObject current=null] : iv_ruleImport= ruleImport EOF ;
    public final EObject entryRuleImport() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImport = null;


        try {
            // InternalRestfulWebservice.g:767:47: (iv_ruleImport= ruleImport EOF )
            // InternalRestfulWebservice.g:768:2: iv_ruleImport= ruleImport EOF
            {
             newCompositeNode(grammarAccess.getImportRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleImport=ruleImport();

            state._fsp--;

             current =iv_ruleImport; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImport"


    // $ANTLR start "ruleImport"
    // InternalRestfulWebservice.g:774:1: ruleImport returns [EObject current=null] : (otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) ) ) ;
    public final EObject ruleImport() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        AntlrDatatypeRuleToken lv_importedNamespace_1_0 = null;



        	enterRule();

        try {
            // InternalRestfulWebservice.g:780:2: ( (otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) ) ) )
            // InternalRestfulWebservice.g:781:2: (otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) ) )
            {
            // InternalRestfulWebservice.g:781:2: (otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) ) )
            // InternalRestfulWebservice.g:782:3: otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) )
            {
            otherlv_0=(Token)match(input,24,FOLLOW_14); 

            			newLeafNode(otherlv_0, grammarAccess.getImportAccess().getImportKeyword_0());
            		
            // InternalRestfulWebservice.g:786:3: ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) )
            // InternalRestfulWebservice.g:787:4: (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard )
            {
            // InternalRestfulWebservice.g:787:4: (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard )
            // InternalRestfulWebservice.g:788:5: lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard
            {

            					newCompositeNode(grammarAccess.getImportAccess().getImportedNamespaceQualifiedNameWithWildcardParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_2);
            lv_importedNamespace_1_0=ruleQualifiedNameWithWildcard();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getImportRule());
            					}
            					set(
            						current,
            						"importedNamespace",
            						lv_importedNamespace_1_0,
            						"br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.RestfulWebservice.QualifiedNameWithWildcard");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImport"


    // $ANTLR start "entryRuleQualifiedNameWithWildcard"
    // InternalRestfulWebservice.g:809:1: entryRuleQualifiedNameWithWildcard returns [String current=null] : iv_ruleQualifiedNameWithWildcard= ruleQualifiedNameWithWildcard EOF ;
    public final String entryRuleQualifiedNameWithWildcard() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQualifiedNameWithWildcard = null;


        try {
            // InternalRestfulWebservice.g:809:65: (iv_ruleQualifiedNameWithWildcard= ruleQualifiedNameWithWildcard EOF )
            // InternalRestfulWebservice.g:810:2: iv_ruleQualifiedNameWithWildcard= ruleQualifiedNameWithWildcard EOF
            {
             newCompositeNode(grammarAccess.getQualifiedNameWithWildcardRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQualifiedNameWithWildcard=ruleQualifiedNameWithWildcard();

            state._fsp--;

             current =iv_ruleQualifiedNameWithWildcard.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQualifiedNameWithWildcard"


    // $ANTLR start "ruleQualifiedNameWithWildcard"
    // InternalRestfulWebservice.g:816:1: ruleQualifiedNameWithWildcard returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_QualifiedName_0= ruleQualifiedName (kw= '.*' )? ) ;
    public final AntlrDatatypeRuleToken ruleQualifiedNameWithWildcard() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        AntlrDatatypeRuleToken this_QualifiedName_0 = null;



        	enterRule();

        try {
            // InternalRestfulWebservice.g:822:2: ( (this_QualifiedName_0= ruleQualifiedName (kw= '.*' )? ) )
            // InternalRestfulWebservice.g:823:2: (this_QualifiedName_0= ruleQualifiedName (kw= '.*' )? )
            {
            // InternalRestfulWebservice.g:823:2: (this_QualifiedName_0= ruleQualifiedName (kw= '.*' )? )
            // InternalRestfulWebservice.g:824:3: this_QualifiedName_0= ruleQualifiedName (kw= '.*' )?
            {

            			newCompositeNode(grammarAccess.getQualifiedNameWithWildcardAccess().getQualifiedNameParserRuleCall_0());
            		
            pushFollow(FOLLOW_17);
            this_QualifiedName_0=ruleQualifiedName();

            state._fsp--;


            			current.merge(this_QualifiedName_0);
            		

            			afterParserOrEnumRuleCall();
            		
            // InternalRestfulWebservice.g:834:3: (kw= '.*' )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==25) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // InternalRestfulWebservice.g:835:4: kw= '.*'
                    {
                    kw=(Token)match(input,25,FOLLOW_2); 

                    				current.merge(kw);
                    				newLeafNode(kw, grammarAccess.getQualifiedNameWithWildcardAccess().getFullStopAsteriskKeyword_1());
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQualifiedNameWithWildcard"


    // $ANTLR start "entryRuleEntity"
    // InternalRestfulWebservice.g:845:1: entryRuleEntity returns [EObject current=null] : iv_ruleEntity= ruleEntity EOF ;
    public final EObject entryRuleEntity() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEntity = null;


        try {
            // InternalRestfulWebservice.g:845:47: (iv_ruleEntity= ruleEntity EOF )
            // InternalRestfulWebservice.g:846:2: iv_ruleEntity= ruleEntity EOF
            {
             newCompositeNode(grammarAccess.getEntityRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEntity=ruleEntity();

            state._fsp--;

             current =iv_ruleEntity; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEntity"


    // $ANTLR start "ruleEntity"
    // InternalRestfulWebservice.g:852:1: ruleEntity returns [EObject current=null] : ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'entity' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= 'extends' ( ( ruleQualifiedName ) ) )? otherlv_5= '{' ( (lv_entity_type_6_0= ruleEntity_Type ) ) ( (lv_attributes_7_0= ruleAttribute ) )* ( (lv_functions_8_0= ruleFunction ) )* ( (lv_relations_9_0= ruleRelation ) )* otherlv_10= '}' ) ;
    public final EObject ruleEntity() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_10=null;
        EObject lv_description_0_0 = null;

        EObject lv_entity_type_6_0 = null;

        EObject lv_attributes_7_0 = null;

        EObject lv_functions_8_0 = null;

        EObject lv_relations_9_0 = null;



        	enterRule();

        try {
            // InternalRestfulWebservice.g:858:2: ( ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'entity' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= 'extends' ( ( ruleQualifiedName ) ) )? otherlv_5= '{' ( (lv_entity_type_6_0= ruleEntity_Type ) ) ( (lv_attributes_7_0= ruleAttribute ) )* ( (lv_functions_8_0= ruleFunction ) )* ( (lv_relations_9_0= ruleRelation ) )* otherlv_10= '}' ) )
            // InternalRestfulWebservice.g:859:2: ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'entity' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= 'extends' ( ( ruleQualifiedName ) ) )? otherlv_5= '{' ( (lv_entity_type_6_0= ruleEntity_Type ) ) ( (lv_attributes_7_0= ruleAttribute ) )* ( (lv_functions_8_0= ruleFunction ) )* ( (lv_relations_9_0= ruleRelation ) )* otherlv_10= '}' )
            {
            // InternalRestfulWebservice.g:859:2: ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'entity' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= 'extends' ( ( ruleQualifiedName ) ) )? otherlv_5= '{' ( (lv_entity_type_6_0= ruleEntity_Type ) ) ( (lv_attributes_7_0= ruleAttribute ) )* ( (lv_functions_8_0= ruleFunction ) )* ( (lv_relations_9_0= ruleRelation ) )* otherlv_10= '}' )
            // InternalRestfulWebservice.g:860:3: ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'entity' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= 'extends' ( ( ruleQualifiedName ) ) )? otherlv_5= '{' ( (lv_entity_type_6_0= ruleEntity_Type ) ) ( (lv_attributes_7_0= ruleAttribute ) )* ( (lv_functions_8_0= ruleFunction ) )* ( (lv_relations_9_0= ruleRelation ) )* otherlv_10= '}'
            {
            // InternalRestfulWebservice.g:860:3: ( (lv_description_0_0= ruleDescription ) )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==20) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // InternalRestfulWebservice.g:861:4: (lv_description_0_0= ruleDescription )
                    {
                    // InternalRestfulWebservice.g:861:4: (lv_description_0_0= ruleDescription )
                    // InternalRestfulWebservice.g:862:5: lv_description_0_0= ruleDescription
                    {

                    					newCompositeNode(grammarAccess.getEntityAccess().getDescriptionDescriptionParserRuleCall_0_0());
                    				
                    pushFollow(FOLLOW_18);
                    lv_description_0_0=ruleDescription();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getEntityRule());
                    					}
                    					set(
                    						current,
                    						"description",
                    						lv_description_0_0,
                    						"br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.RestfulWebservice.Description");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            otherlv_1=(Token)match(input,26,FOLLOW_14); 

            			newLeafNode(otherlv_1, grammarAccess.getEntityAccess().getEntityKeyword_1());
            		
            // InternalRestfulWebservice.g:883:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalRestfulWebservice.g:884:4: (lv_name_2_0= RULE_ID )
            {
            // InternalRestfulWebservice.g:884:4: (lv_name_2_0= RULE_ID )
            // InternalRestfulWebservice.g:885:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_19); 

            					newLeafNode(lv_name_2_0, grammarAccess.getEntityAccess().getNameIDTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getEntityRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_2_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            // InternalRestfulWebservice.g:901:3: (otherlv_3= 'extends' ( ( ruleQualifiedName ) ) )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==27) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // InternalRestfulWebservice.g:902:4: otherlv_3= 'extends' ( ( ruleQualifiedName ) )
                    {
                    otherlv_3=(Token)match(input,27,FOLLOW_14); 

                    				newLeafNode(otherlv_3, grammarAccess.getEntityAccess().getExtendsKeyword_3_0());
                    			
                    // InternalRestfulWebservice.g:906:4: ( ( ruleQualifiedName ) )
                    // InternalRestfulWebservice.g:907:5: ( ruleQualifiedName )
                    {
                    // InternalRestfulWebservice.g:907:5: ( ruleQualifiedName )
                    // InternalRestfulWebservice.g:908:6: ruleQualifiedName
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getEntityRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getEntityAccess().getSuperTypeEntityCrossReference_3_1_0());
                    					
                    pushFollow(FOLLOW_4);
                    ruleQualifiedName();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_5=(Token)match(input,12,FOLLOW_20); 

            			newLeafNode(otherlv_5, grammarAccess.getEntityAccess().getLeftCurlyBracketKeyword_4());
            		
            // InternalRestfulWebservice.g:927:3: ( (lv_entity_type_6_0= ruleEntity_Type ) )
            // InternalRestfulWebservice.g:928:4: (lv_entity_type_6_0= ruleEntity_Type )
            {
            // InternalRestfulWebservice.g:928:4: (lv_entity_type_6_0= ruleEntity_Type )
            // InternalRestfulWebservice.g:929:5: lv_entity_type_6_0= ruleEntity_Type
            {

            					newCompositeNode(grammarAccess.getEntityAccess().getEntity_typeEntity_TypeParserRuleCall_5_0());
            				
            pushFollow(FOLLOW_21);
            lv_entity_type_6_0=ruleEntity_Type();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getEntityRule());
            					}
            					set(
            						current,
            						"entity_type",
            						lv_entity_type_6_0,
            						"br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.RestfulWebservice.Entity_Type");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalRestfulWebservice.g:946:3: ( (lv_attributes_7_0= ruleAttribute ) )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==20) ) {
                    int LA10_1 = input.LA(2);

                    if ( (LA10_1==RULE_STRING) ) {
                        int LA10_4 = input.LA(3);

                        if ( (LA10_4==RULE_ID) ) {
                            alt10=1;
                        }


                    }


                }
                else if ( (LA10_0==RULE_ID) ) {
                    int LA10_3 = input.LA(2);

                    if ( (LA10_3==28) ) {
                        alt10=1;
                    }


                }


                switch (alt10) {
            	case 1 :
            	    // InternalRestfulWebservice.g:947:4: (lv_attributes_7_0= ruleAttribute )
            	    {
            	    // InternalRestfulWebservice.g:947:4: (lv_attributes_7_0= ruleAttribute )
            	    // InternalRestfulWebservice.g:948:5: lv_attributes_7_0= ruleAttribute
            	    {

            	    					newCompositeNode(grammarAccess.getEntityAccess().getAttributesAttributeParserRuleCall_6_0());
            	    				
            	    pushFollow(FOLLOW_21);
            	    lv_attributes_7_0=ruleAttribute();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getEntityRule());
            	    					}
            	    					add(
            	    						current,
            	    						"attributes",
            	    						lv_attributes_7_0,
            	    						"br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.RestfulWebservice.Attribute");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);

            // InternalRestfulWebservice.g:965:3: ( (lv_functions_8_0= ruleFunction ) )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==20||LA11_0==32) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // InternalRestfulWebservice.g:966:4: (lv_functions_8_0= ruleFunction )
            	    {
            	    // InternalRestfulWebservice.g:966:4: (lv_functions_8_0= ruleFunction )
            	    // InternalRestfulWebservice.g:967:5: lv_functions_8_0= ruleFunction
            	    {

            	    					newCompositeNode(grammarAccess.getEntityAccess().getFunctionsFunctionParserRuleCall_7_0());
            	    				
            	    pushFollow(FOLLOW_21);
            	    lv_functions_8_0=ruleFunction();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getEntityRule());
            	    					}
            	    					add(
            	    						current,
            	    						"functions",
            	    						lv_functions_8_0,
            	    						"br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.RestfulWebservice.Function");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

            // InternalRestfulWebservice.g:984:3: ( (lv_relations_9_0= ruleRelation ) )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( (LA12_0==RULE_ID) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // InternalRestfulWebservice.g:985:4: (lv_relations_9_0= ruleRelation )
            	    {
            	    // InternalRestfulWebservice.g:985:4: (lv_relations_9_0= ruleRelation )
            	    // InternalRestfulWebservice.g:986:5: lv_relations_9_0= ruleRelation
            	    {

            	    					newCompositeNode(grammarAccess.getEntityAccess().getRelationsRelationParserRuleCall_8_0());
            	    				
            	    pushFollow(FOLLOW_22);
            	    lv_relations_9_0=ruleRelation();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getEntityRule());
            	    					}
            	    					add(
            	    						current,
            	    						"relations",
            	    						lv_relations_9_0,
            	    						"br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.RestfulWebservice.Relation");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);

            otherlv_10=(Token)match(input,13,FOLLOW_2); 

            			newLeafNode(otherlv_10, grammarAccess.getEntityAccess().getRightCurlyBracketKeyword_9());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEntity"


    // $ANTLR start "entryRuleAttribute"
    // InternalRestfulWebservice.g:1011:1: entryRuleAttribute returns [EObject current=null] : iv_ruleAttribute= ruleAttribute EOF ;
    public final EObject entryRuleAttribute() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAttribute = null;


        try {
            // InternalRestfulWebservice.g:1011:50: (iv_ruleAttribute= ruleAttribute EOF )
            // InternalRestfulWebservice.g:1012:2: iv_ruleAttribute= ruleAttribute EOF
            {
             newCompositeNode(grammarAccess.getAttributeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAttribute=ruleAttribute();

            state._fsp--;

             current =iv_ruleAttribute; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAttribute"


    // $ANTLR start "ruleAttribute"
    // InternalRestfulWebservice.g:1018:1: ruleAttribute returns [EObject current=null] : ( ( (lv_description_0_0= ruleDescription ) )? ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_type_3_0= RULE_ID ) ) ) ;
    public final EObject ruleAttribute() throws RecognitionException {
        EObject current = null;

        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token lv_type_3_0=null;
        EObject lv_description_0_0 = null;



        	enterRule();

        try {
            // InternalRestfulWebservice.g:1024:2: ( ( ( (lv_description_0_0= ruleDescription ) )? ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_type_3_0= RULE_ID ) ) ) )
            // InternalRestfulWebservice.g:1025:2: ( ( (lv_description_0_0= ruleDescription ) )? ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_type_3_0= RULE_ID ) ) )
            {
            // InternalRestfulWebservice.g:1025:2: ( ( (lv_description_0_0= ruleDescription ) )? ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_type_3_0= RULE_ID ) ) )
            // InternalRestfulWebservice.g:1026:3: ( (lv_description_0_0= ruleDescription ) )? ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_type_3_0= RULE_ID ) )
            {
            // InternalRestfulWebservice.g:1026:3: ( (lv_description_0_0= ruleDescription ) )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==20) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // InternalRestfulWebservice.g:1027:4: (lv_description_0_0= ruleDescription )
                    {
                    // InternalRestfulWebservice.g:1027:4: (lv_description_0_0= ruleDescription )
                    // InternalRestfulWebservice.g:1028:5: lv_description_0_0= ruleDescription
                    {

                    					newCompositeNode(grammarAccess.getAttributeAccess().getDescriptionDescriptionParserRuleCall_0_0());
                    				
                    pushFollow(FOLLOW_14);
                    lv_description_0_0=ruleDescription();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getAttributeRule());
                    					}
                    					set(
                    						current,
                    						"description",
                    						lv_description_0_0,
                    						"br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.RestfulWebservice.Description");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalRestfulWebservice.g:1045:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalRestfulWebservice.g:1046:4: (lv_name_1_0= RULE_ID )
            {
            // InternalRestfulWebservice.g:1046:4: (lv_name_1_0= RULE_ID )
            // InternalRestfulWebservice.g:1047:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_23); 

            					newLeafNode(lv_name_1_0, grammarAccess.getAttributeAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAttributeRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,28,FOLLOW_14); 

            			newLeafNode(otherlv_2, grammarAccess.getAttributeAccess().getColonKeyword_2());
            		
            // InternalRestfulWebservice.g:1067:3: ( (lv_type_3_0= RULE_ID ) )
            // InternalRestfulWebservice.g:1068:4: (lv_type_3_0= RULE_ID )
            {
            // InternalRestfulWebservice.g:1068:4: (lv_type_3_0= RULE_ID )
            // InternalRestfulWebservice.g:1069:5: lv_type_3_0= RULE_ID
            {
            lv_type_3_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(lv_type_3_0, grammarAccess.getAttributeAccess().getTypeIDTerminalRuleCall_3_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAttributeRule());
            					}
            					setWithLastConsumed(
            						current,
            						"type",
            						lv_type_3_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAttribute"


    // $ANTLR start "entryRuleRelation"
    // InternalRestfulWebservice.g:1089:1: entryRuleRelation returns [EObject current=null] : iv_ruleRelation= ruleRelation EOF ;
    public final EObject entryRuleRelation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRelation = null;


        try {
            // InternalRestfulWebservice.g:1089:49: (iv_ruleRelation= ruleRelation EOF )
            // InternalRestfulWebservice.g:1090:2: iv_ruleRelation= ruleRelation EOF
            {
             newCompositeNode(grammarAccess.getRelationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRelation=ruleRelation();

            state._fsp--;

             current =iv_ruleRelation; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRelation"


    // $ANTLR start "ruleRelation"
    // InternalRestfulWebservice.g:1096:1: ruleRelation returns [EObject current=null] : (this_OneToOne_0= ruleOneToOne | this_ManyToMany_1= ruleManyToMany | this_OneToMany_2= ruleOneToMany ) ;
    public final EObject ruleRelation() throws RecognitionException {
        EObject current = null;

        EObject this_OneToOne_0 = null;

        EObject this_ManyToMany_1 = null;

        EObject this_OneToMany_2 = null;



        	enterRule();

        try {
            // InternalRestfulWebservice.g:1102:2: ( (this_OneToOne_0= ruleOneToOne | this_ManyToMany_1= ruleManyToMany | this_OneToMany_2= ruleOneToMany ) )
            // InternalRestfulWebservice.g:1103:2: (this_OneToOne_0= ruleOneToOne | this_ManyToMany_1= ruleManyToMany | this_OneToMany_2= ruleOneToMany )
            {
            // InternalRestfulWebservice.g:1103:2: (this_OneToOne_0= ruleOneToOne | this_ManyToMany_1= ruleManyToMany | this_OneToMany_2= ruleOneToMany )
            int alt14=3;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==RULE_ID) ) {
                switch ( input.LA(2) ) {
                case 29:
                    {
                    alt14=1;
                    }
                    break;
                case 30:
                    {
                    alt14=2;
                    }
                    break;
                case 31:
                    {
                    alt14=3;
                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 14, 1, input);

                    throw nvae;
                }

            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 14, 0, input);

                throw nvae;
            }
            switch (alt14) {
                case 1 :
                    // InternalRestfulWebservice.g:1104:3: this_OneToOne_0= ruleOneToOne
                    {

                    			newCompositeNode(grammarAccess.getRelationAccess().getOneToOneParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_OneToOne_0=ruleOneToOne();

                    state._fsp--;


                    			current = this_OneToOne_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalRestfulWebservice.g:1113:3: this_ManyToMany_1= ruleManyToMany
                    {

                    			newCompositeNode(grammarAccess.getRelationAccess().getManyToManyParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_ManyToMany_1=ruleManyToMany();

                    state._fsp--;


                    			current = this_ManyToMany_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalRestfulWebservice.g:1122:3: this_OneToMany_2= ruleOneToMany
                    {

                    			newCompositeNode(grammarAccess.getRelationAccess().getOneToManyParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_OneToMany_2=ruleOneToMany();

                    state._fsp--;


                    			current = this_OneToMany_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRelation"


    // $ANTLR start "entryRuleOneToOne"
    // InternalRestfulWebservice.g:1134:1: entryRuleOneToOne returns [EObject current=null] : iv_ruleOneToOne= ruleOneToOne EOF ;
    public final EObject entryRuleOneToOne() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOneToOne = null;


        try {
            // InternalRestfulWebservice.g:1134:49: (iv_ruleOneToOne= ruleOneToOne EOF )
            // InternalRestfulWebservice.g:1135:2: iv_ruleOneToOne= ruleOneToOne EOF
            {
             newCompositeNode(grammarAccess.getOneToOneRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOneToOne=ruleOneToOne();

            state._fsp--;

             current =iv_ruleOneToOne; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOneToOne"


    // $ANTLR start "ruleOneToOne"
    // InternalRestfulWebservice.g:1141:1: ruleOneToOne returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'OneToOne' ( (otherlv_2= RULE_ID ) ) ) ;
    public final EObject ruleOneToOne() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;


        	enterRule();

        try {
            // InternalRestfulWebservice.g:1147:2: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'OneToOne' ( (otherlv_2= RULE_ID ) ) ) )
            // InternalRestfulWebservice.g:1148:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'OneToOne' ( (otherlv_2= RULE_ID ) ) )
            {
            // InternalRestfulWebservice.g:1148:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'OneToOne' ( (otherlv_2= RULE_ID ) ) )
            // InternalRestfulWebservice.g:1149:3: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'OneToOne' ( (otherlv_2= RULE_ID ) )
            {
            // InternalRestfulWebservice.g:1149:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalRestfulWebservice.g:1150:4: (lv_name_0_0= RULE_ID )
            {
            // InternalRestfulWebservice.g:1150:4: (lv_name_0_0= RULE_ID )
            // InternalRestfulWebservice.g:1151:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_24); 

            					newLeafNode(lv_name_0_0, grammarAccess.getOneToOneAccess().getNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getOneToOneRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_1=(Token)match(input,29,FOLLOW_14); 

            			newLeafNode(otherlv_1, grammarAccess.getOneToOneAccess().getOneToOneKeyword_1());
            		
            // InternalRestfulWebservice.g:1171:3: ( (otherlv_2= RULE_ID ) )
            // InternalRestfulWebservice.g:1172:4: (otherlv_2= RULE_ID )
            {
            // InternalRestfulWebservice.g:1172:4: (otherlv_2= RULE_ID )
            // InternalRestfulWebservice.g:1173:5: otherlv_2= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getOneToOneRule());
            					}
            				
            otherlv_2=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(otherlv_2, grammarAccess.getOneToOneAccess().getTypeEntityCrossReference_2_0());
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOneToOne"


    // $ANTLR start "entryRuleManyToMany"
    // InternalRestfulWebservice.g:1188:1: entryRuleManyToMany returns [EObject current=null] : iv_ruleManyToMany= ruleManyToMany EOF ;
    public final EObject entryRuleManyToMany() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleManyToMany = null;


        try {
            // InternalRestfulWebservice.g:1188:51: (iv_ruleManyToMany= ruleManyToMany EOF )
            // InternalRestfulWebservice.g:1189:2: iv_ruleManyToMany= ruleManyToMany EOF
            {
             newCompositeNode(grammarAccess.getManyToManyRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleManyToMany=ruleManyToMany();

            state._fsp--;

             current =iv_ruleManyToMany; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleManyToMany"


    // $ANTLR start "ruleManyToMany"
    // InternalRestfulWebservice.g:1195:1: ruleManyToMany returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'ManyToMany' ( (otherlv_2= RULE_ID ) ) ) ;
    public final EObject ruleManyToMany() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;


        	enterRule();

        try {
            // InternalRestfulWebservice.g:1201:2: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'ManyToMany' ( (otherlv_2= RULE_ID ) ) ) )
            // InternalRestfulWebservice.g:1202:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'ManyToMany' ( (otherlv_2= RULE_ID ) ) )
            {
            // InternalRestfulWebservice.g:1202:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'ManyToMany' ( (otherlv_2= RULE_ID ) ) )
            // InternalRestfulWebservice.g:1203:3: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'ManyToMany' ( (otherlv_2= RULE_ID ) )
            {
            // InternalRestfulWebservice.g:1203:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalRestfulWebservice.g:1204:4: (lv_name_0_0= RULE_ID )
            {
            // InternalRestfulWebservice.g:1204:4: (lv_name_0_0= RULE_ID )
            // InternalRestfulWebservice.g:1205:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_25); 

            					newLeafNode(lv_name_0_0, grammarAccess.getManyToManyAccess().getNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getManyToManyRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_1=(Token)match(input,30,FOLLOW_14); 

            			newLeafNode(otherlv_1, grammarAccess.getManyToManyAccess().getManyToManyKeyword_1());
            		
            // InternalRestfulWebservice.g:1225:3: ( (otherlv_2= RULE_ID ) )
            // InternalRestfulWebservice.g:1226:4: (otherlv_2= RULE_ID )
            {
            // InternalRestfulWebservice.g:1226:4: (otherlv_2= RULE_ID )
            // InternalRestfulWebservice.g:1227:5: otherlv_2= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getManyToManyRule());
            					}
            				
            otherlv_2=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(otherlv_2, grammarAccess.getManyToManyAccess().getTypeEntityCrossReference_2_0());
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleManyToMany"


    // $ANTLR start "entryRuleOneToMany"
    // InternalRestfulWebservice.g:1242:1: entryRuleOneToMany returns [EObject current=null] : iv_ruleOneToMany= ruleOneToMany EOF ;
    public final EObject entryRuleOneToMany() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOneToMany = null;


        try {
            // InternalRestfulWebservice.g:1242:50: (iv_ruleOneToMany= ruleOneToMany EOF )
            // InternalRestfulWebservice.g:1243:2: iv_ruleOneToMany= ruleOneToMany EOF
            {
             newCompositeNode(grammarAccess.getOneToManyRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOneToMany=ruleOneToMany();

            state._fsp--;

             current =iv_ruleOneToMany; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOneToMany"


    // $ANTLR start "ruleOneToMany"
    // InternalRestfulWebservice.g:1249:1: ruleOneToMany returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'OneToMany' ( (otherlv_2= RULE_ID ) ) ) ;
    public final EObject ruleOneToMany() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;


        	enterRule();

        try {
            // InternalRestfulWebservice.g:1255:2: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'OneToMany' ( (otherlv_2= RULE_ID ) ) ) )
            // InternalRestfulWebservice.g:1256:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'OneToMany' ( (otherlv_2= RULE_ID ) ) )
            {
            // InternalRestfulWebservice.g:1256:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'OneToMany' ( (otherlv_2= RULE_ID ) ) )
            // InternalRestfulWebservice.g:1257:3: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'OneToMany' ( (otherlv_2= RULE_ID ) )
            {
            // InternalRestfulWebservice.g:1257:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalRestfulWebservice.g:1258:4: (lv_name_0_0= RULE_ID )
            {
            // InternalRestfulWebservice.g:1258:4: (lv_name_0_0= RULE_ID )
            // InternalRestfulWebservice.g:1259:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_26); 

            					newLeafNode(lv_name_0_0, grammarAccess.getOneToManyAccess().getNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getOneToManyRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_1=(Token)match(input,31,FOLLOW_14); 

            			newLeafNode(otherlv_1, grammarAccess.getOneToManyAccess().getOneToManyKeyword_1());
            		
            // InternalRestfulWebservice.g:1279:3: ( (otherlv_2= RULE_ID ) )
            // InternalRestfulWebservice.g:1280:4: (otherlv_2= RULE_ID )
            {
            // InternalRestfulWebservice.g:1280:4: (otherlv_2= RULE_ID )
            // InternalRestfulWebservice.g:1281:5: otherlv_2= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getOneToManyRule());
            					}
            				
            otherlv_2=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(otherlv_2, grammarAccess.getOneToManyAccess().getTypeEntityCrossReference_2_0());
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOneToMany"


    // $ANTLR start "entryRuleFunction"
    // InternalRestfulWebservice.g:1296:1: entryRuleFunction returns [EObject current=null] : iv_ruleFunction= ruleFunction EOF ;
    public final EObject entryRuleFunction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFunction = null;


        try {
            // InternalRestfulWebservice.g:1296:49: (iv_ruleFunction= ruleFunction EOF )
            // InternalRestfulWebservice.g:1297:2: iv_ruleFunction= ruleFunction EOF
            {
             newCompositeNode(grammarAccess.getFunctionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFunction=ruleFunction();

            state._fsp--;

             current =iv_ruleFunction; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFunction"


    // $ANTLR start "ruleFunction"
    // InternalRestfulWebservice.g:1303:1: ruleFunction returns [EObject current=null] : ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'function' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '(' ( ( (lv_params_4_0= RULE_ID ) ) (otherlv_5= ',' ( (lv_params_6_0= RULE_ID ) ) )* )? otherlv_7= ')' otherlv_8= ':' ( (lv_type_9_0= RULE_ID ) ) ) ;
    public final EObject ruleFunction() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token lv_params_4_0=null;
        Token otherlv_5=null;
        Token lv_params_6_0=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token lv_type_9_0=null;
        EObject lv_description_0_0 = null;



        	enterRule();

        try {
            // InternalRestfulWebservice.g:1309:2: ( ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'function' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '(' ( ( (lv_params_4_0= RULE_ID ) ) (otherlv_5= ',' ( (lv_params_6_0= RULE_ID ) ) )* )? otherlv_7= ')' otherlv_8= ':' ( (lv_type_9_0= RULE_ID ) ) ) )
            // InternalRestfulWebservice.g:1310:2: ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'function' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '(' ( ( (lv_params_4_0= RULE_ID ) ) (otherlv_5= ',' ( (lv_params_6_0= RULE_ID ) ) )* )? otherlv_7= ')' otherlv_8= ':' ( (lv_type_9_0= RULE_ID ) ) )
            {
            // InternalRestfulWebservice.g:1310:2: ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'function' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '(' ( ( (lv_params_4_0= RULE_ID ) ) (otherlv_5= ',' ( (lv_params_6_0= RULE_ID ) ) )* )? otherlv_7= ')' otherlv_8= ':' ( (lv_type_9_0= RULE_ID ) ) )
            // InternalRestfulWebservice.g:1311:3: ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'function' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '(' ( ( (lv_params_4_0= RULE_ID ) ) (otherlv_5= ',' ( (lv_params_6_0= RULE_ID ) ) )* )? otherlv_7= ')' otherlv_8= ':' ( (lv_type_9_0= RULE_ID ) )
            {
            // InternalRestfulWebservice.g:1311:3: ( (lv_description_0_0= ruleDescription ) )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==20) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // InternalRestfulWebservice.g:1312:4: (lv_description_0_0= ruleDescription )
                    {
                    // InternalRestfulWebservice.g:1312:4: (lv_description_0_0= ruleDescription )
                    // InternalRestfulWebservice.g:1313:5: lv_description_0_0= ruleDescription
                    {

                    					newCompositeNode(grammarAccess.getFunctionAccess().getDescriptionDescriptionParserRuleCall_0_0());
                    				
                    pushFollow(FOLLOW_27);
                    lv_description_0_0=ruleDescription();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getFunctionRule());
                    					}
                    					set(
                    						current,
                    						"description",
                    						lv_description_0_0,
                    						"br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.RestfulWebservice.Description");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            otherlv_1=(Token)match(input,32,FOLLOW_14); 

            			newLeafNode(otherlv_1, grammarAccess.getFunctionAccess().getFunctionKeyword_1());
            		
            // InternalRestfulWebservice.g:1334:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalRestfulWebservice.g:1335:4: (lv_name_2_0= RULE_ID )
            {
            // InternalRestfulWebservice.g:1335:4: (lv_name_2_0= RULE_ID )
            // InternalRestfulWebservice.g:1336:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_28); 

            					newLeafNode(lv_name_2_0, grammarAccess.getFunctionAccess().getNameIDTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getFunctionRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_2_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_3=(Token)match(input,33,FOLLOW_29); 

            			newLeafNode(otherlv_3, grammarAccess.getFunctionAccess().getLeftParenthesisKeyword_3());
            		
            // InternalRestfulWebservice.g:1356:3: ( ( (lv_params_4_0= RULE_ID ) ) (otherlv_5= ',' ( (lv_params_6_0= RULE_ID ) ) )* )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==RULE_ID) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // InternalRestfulWebservice.g:1357:4: ( (lv_params_4_0= RULE_ID ) ) (otherlv_5= ',' ( (lv_params_6_0= RULE_ID ) ) )*
                    {
                    // InternalRestfulWebservice.g:1357:4: ( (lv_params_4_0= RULE_ID ) )
                    // InternalRestfulWebservice.g:1358:5: (lv_params_4_0= RULE_ID )
                    {
                    // InternalRestfulWebservice.g:1358:5: (lv_params_4_0= RULE_ID )
                    // InternalRestfulWebservice.g:1359:6: lv_params_4_0= RULE_ID
                    {
                    lv_params_4_0=(Token)match(input,RULE_ID,FOLLOW_30); 

                    						newLeafNode(lv_params_4_0, grammarAccess.getFunctionAccess().getParamsIDTerminalRuleCall_4_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getFunctionRule());
                    						}
                    						addWithLastConsumed(
                    							current,
                    							"params",
                    							lv_params_4_0,
                    							"org.eclipse.xtext.common.Terminals.ID");
                    					

                    }


                    }

                    // InternalRestfulWebservice.g:1375:4: (otherlv_5= ',' ( (lv_params_6_0= RULE_ID ) ) )*
                    loop16:
                    do {
                        int alt16=2;
                        int LA16_0 = input.LA(1);

                        if ( (LA16_0==34) ) {
                            alt16=1;
                        }


                        switch (alt16) {
                    	case 1 :
                    	    // InternalRestfulWebservice.g:1376:5: otherlv_5= ',' ( (lv_params_6_0= RULE_ID ) )
                    	    {
                    	    otherlv_5=(Token)match(input,34,FOLLOW_14); 

                    	    					newLeafNode(otherlv_5, grammarAccess.getFunctionAccess().getCommaKeyword_4_1_0());
                    	    				
                    	    // InternalRestfulWebservice.g:1380:5: ( (lv_params_6_0= RULE_ID ) )
                    	    // InternalRestfulWebservice.g:1381:6: (lv_params_6_0= RULE_ID )
                    	    {
                    	    // InternalRestfulWebservice.g:1381:6: (lv_params_6_0= RULE_ID )
                    	    // InternalRestfulWebservice.g:1382:7: lv_params_6_0= RULE_ID
                    	    {
                    	    lv_params_6_0=(Token)match(input,RULE_ID,FOLLOW_30); 

                    	    							newLeafNode(lv_params_6_0, grammarAccess.getFunctionAccess().getParamsIDTerminalRuleCall_4_1_1_0());
                    	    						

                    	    							if (current==null) {
                    	    								current = createModelElement(grammarAccess.getFunctionRule());
                    	    							}
                    	    							addWithLastConsumed(
                    	    								current,
                    	    								"params",
                    	    								lv_params_6_0,
                    	    								"org.eclipse.xtext.common.Terminals.ID");
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop16;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_7=(Token)match(input,35,FOLLOW_23); 

            			newLeafNode(otherlv_7, grammarAccess.getFunctionAccess().getRightParenthesisKeyword_5());
            		
            otherlv_8=(Token)match(input,28,FOLLOW_14); 

            			newLeafNode(otherlv_8, grammarAccess.getFunctionAccess().getColonKeyword_6());
            		
            // InternalRestfulWebservice.g:1408:3: ( (lv_type_9_0= RULE_ID ) )
            // InternalRestfulWebservice.g:1409:4: (lv_type_9_0= RULE_ID )
            {
            // InternalRestfulWebservice.g:1409:4: (lv_type_9_0= RULE_ID )
            // InternalRestfulWebservice.g:1410:5: lv_type_9_0= RULE_ID
            {
            lv_type_9_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(lv_type_9_0, grammarAccess.getFunctionAccess().getTypeIDTerminalRuleCall_7_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getFunctionRule());
            					}
            					setWithLastConsumed(
            						current,
            						"type",
            						lv_type_9_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFunction"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000005500002L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000005502000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000800002L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000002000002L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000008001000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000100102020L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000000002020L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000000800000020L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000000C00000000L});

}