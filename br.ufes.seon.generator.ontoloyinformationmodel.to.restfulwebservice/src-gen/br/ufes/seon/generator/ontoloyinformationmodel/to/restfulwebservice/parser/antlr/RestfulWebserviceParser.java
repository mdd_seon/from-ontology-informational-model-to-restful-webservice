/*
 * generated by Xtext 2.21.0
 */
package br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.parser.antlr;

import br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.parser.antlr.internal.InternalRestfulWebserviceParser;
import br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.services.RestfulWebserviceGrammarAccess;
import com.google.inject.Inject;
import org.eclipse.xtext.parser.antlr.AbstractAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;

public class RestfulWebserviceParser extends AbstractAntlrParser {

	@Inject
	private RestfulWebserviceGrammarAccess grammarAccess;

	@Override
	protected void setInitialHiddenTokens(XtextTokenStream tokenStream) {
		tokenStream.setInitialHiddenTokens("RULE_WS", "RULE_ML_COMMENT", "RULE_SL_COMMENT");
	}
	

	@Override
	protected InternalRestfulWebserviceParser createParser(XtextTokenStream stream) {
		return new InternalRestfulWebserviceParser(stream, getGrammarAccess());
	}

	@Override 
	protected String getDefaultRuleName() {
		return "Application";
	}

	public RestfulWebserviceGrammarAccess getGrammarAccess() {
		return this.grammarAccess;
	}

	public void setGrammarAccess(RestfulWebserviceGrammarAccess grammarAccess) {
		this.grammarAccess = grammarAccess;
	}
}
