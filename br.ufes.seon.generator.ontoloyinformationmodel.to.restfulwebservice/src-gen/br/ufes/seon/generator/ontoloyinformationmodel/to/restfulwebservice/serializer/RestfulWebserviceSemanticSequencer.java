/*
 * generated by Xtext 2.21.0
 */
package br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.serializer;

import br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.restfulWebservice.About;
import br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.restfulWebservice.Application;
import br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.restfulWebservice.Attribute;
import br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.restfulWebservice.Author;
import br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.restfulWebservice.Author_Email;
import br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.restfulWebservice.Configuration;
import br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.restfulWebservice.Description;
import br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.restfulWebservice.Entity;
import br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.restfulWebservice.Entity_Type;
import br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.restfulWebservice.Function;
import br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.restfulWebservice.Import;
import br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.restfulWebservice.Lib;
import br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.restfulWebservice.ManyToMany;
import br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.restfulWebservice.OneToMany;
import br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.restfulWebservice.OneToOne;
import br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.restfulWebservice.Repository;
import br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.restfulWebservice.RestfulWebservicePackage;
import br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.restfulWebservice.Software;
import br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.services.RestfulWebserviceGrammarAccess;
import com.google.inject.Inject;
import java.util.Set;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.xtext.Action;
import org.eclipse.xtext.Parameter;
import org.eclipse.xtext.ParserRule;
import org.eclipse.xtext.serializer.ISerializationContext;
import org.eclipse.xtext.serializer.acceptor.SequenceFeeder;
import org.eclipse.xtext.serializer.sequencer.AbstractDelegatingSemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService.ValueTransient;

@SuppressWarnings("all")
public class RestfulWebserviceSemanticSequencer extends AbstractDelegatingSemanticSequencer {

	@Inject
	private RestfulWebserviceGrammarAccess grammarAccess;
	
	@Override
	public void sequence(ISerializationContext context, EObject semanticObject) {
		EPackage epackage = semanticObject.eClass().getEPackage();
		ParserRule rule = context.getParserRule();
		Action action = context.getAssignedAction();
		Set<Parameter> parameters = context.getEnabledBooleanParameters();
		if (epackage == RestfulWebservicePackage.eINSTANCE)
			switch (semanticObject.eClass().getClassifierID()) {
			case RestfulWebservicePackage.ABOUT:
				sequence_About(context, (About) semanticObject); 
				return; 
			case RestfulWebservicePackage.APPLICATION:
				sequence_Application(context, (Application) semanticObject); 
				return; 
			case RestfulWebservicePackage.ATTRIBUTE:
				sequence_Attribute(context, (Attribute) semanticObject); 
				return; 
			case RestfulWebservicePackage.AUTHOR:
				sequence_Author(context, (Author) semanticObject); 
				return; 
			case RestfulWebservicePackage.AUTHOR_EMAIL:
				sequence_Author_Email(context, (Author_Email) semanticObject); 
				return; 
			case RestfulWebservicePackage.CONFIGURATION:
				sequence_Configuration(context, (Configuration) semanticObject); 
				return; 
			case RestfulWebservicePackage.DESCRIPTION:
				sequence_Description(context, (Description) semanticObject); 
				return; 
			case RestfulWebservicePackage.ENTITY:
				sequence_Entity(context, (Entity) semanticObject); 
				return; 
			case RestfulWebservicePackage.ENTITY_TYPE:
				sequence_Entity_Type(context, (Entity_Type) semanticObject); 
				return; 
			case RestfulWebservicePackage.FUNCTION:
				sequence_Function(context, (Function) semanticObject); 
				return; 
			case RestfulWebservicePackage.IMPORT:
				sequence_Import(context, (Import) semanticObject); 
				return; 
			case RestfulWebservicePackage.LIB:
				sequence_Lib(context, (Lib) semanticObject); 
				return; 
			case RestfulWebservicePackage.MANY_TO_MANY:
				sequence_ManyToMany(context, (ManyToMany) semanticObject); 
				return; 
			case RestfulWebservicePackage.MODULE:
				sequence_Module(context, (br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.restfulWebservice.Module) semanticObject); 
				return; 
			case RestfulWebservicePackage.ONE_TO_MANY:
				sequence_OneToMany(context, (OneToMany) semanticObject); 
				return; 
			case RestfulWebservicePackage.ONE_TO_ONE:
				sequence_OneToOne(context, (OneToOne) semanticObject); 
				return; 
			case RestfulWebservicePackage.REPOSITORY:
				sequence_Repository(context, (Repository) semanticObject); 
				return; 
			case RestfulWebservicePackage.SOFTWARE:
				sequence_Software(context, (Software) semanticObject); 
				return; 
			}
		if (errorAcceptor != null)
			errorAcceptor.accept(diagnosticProvider.createInvalidContextOrTypeDiagnostic(semanticObject, context));
	}
	
	/**
	 * Contexts:
	 *     About returns About
	 *
	 * Constraint:
	 *     name=STRING
	 */
	protected void sequence_About(ISerializationContext context, About semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, RestfulWebservicePackage.Literals.ABOUT__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, RestfulWebservicePackage.Literals.ABOUT__NAME));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getAboutAccess().getNameSTRINGTerminalRuleCall_1_0(), semanticObject.getName());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     Application returns Application
	 *
	 * Constraint:
	 *     ((configuration=Configuration abstractElements+=AbstractElement+) | abstractElements+=AbstractElement+)?
	 */
	protected void sequence_Application(ISerializationContext context, Application semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     Attribute returns Attribute
	 *
	 * Constraint:
	 *     (description=Description? name=ID type=ID)
	 */
	protected void sequence_Attribute(ISerializationContext context, Attribute semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     Author returns Author
	 *
	 * Constraint:
	 *     name=STRING
	 */
	protected void sequence_Author(ISerializationContext context, Author semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, RestfulWebservicePackage.Literals.AUTHOR__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, RestfulWebservicePackage.Literals.AUTHOR__NAME));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getAuthorAccess().getNameSTRINGTerminalRuleCall_1_0(), semanticObject.getName());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     Author_Email returns Author_Email
	 *
	 * Constraint:
	 *     name=STRING
	 */
	protected void sequence_Author_Email(ISerializationContext context, Author_Email semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, RestfulWebservicePackage.Literals.AUTHOR_EMAIL__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, RestfulWebservicePackage.Literals.AUTHOR_EMAIL__NAME));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getAuthor_EmailAccess().getNameSTRINGTerminalRuleCall_1_0(), semanticObject.getName());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     Configuration returns Configuration
	 *
	 * Constraint:
	 *     (
	 *         software=Software 
	 *         about=About 
	 *         lib=Lib 
	 *         author=Author 
	 *         author_email=Author_Email 
	 *         repository=Repository
	 *     )
	 */
	protected void sequence_Configuration(ISerializationContext context, Configuration semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, RestfulWebservicePackage.Literals.CONFIGURATION__SOFTWARE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, RestfulWebservicePackage.Literals.CONFIGURATION__SOFTWARE));
			if (transientValues.isValueTransient(semanticObject, RestfulWebservicePackage.Literals.CONFIGURATION__ABOUT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, RestfulWebservicePackage.Literals.CONFIGURATION__ABOUT));
			if (transientValues.isValueTransient(semanticObject, RestfulWebservicePackage.Literals.CONFIGURATION__LIB) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, RestfulWebservicePackage.Literals.CONFIGURATION__LIB));
			if (transientValues.isValueTransient(semanticObject, RestfulWebservicePackage.Literals.CONFIGURATION__AUTHOR) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, RestfulWebservicePackage.Literals.CONFIGURATION__AUTHOR));
			if (transientValues.isValueTransient(semanticObject, RestfulWebservicePackage.Literals.CONFIGURATION__AUTHOR_EMAIL) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, RestfulWebservicePackage.Literals.CONFIGURATION__AUTHOR_EMAIL));
			if (transientValues.isValueTransient(semanticObject, RestfulWebservicePackage.Literals.CONFIGURATION__REPOSITORY) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, RestfulWebservicePackage.Literals.CONFIGURATION__REPOSITORY));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getConfigurationAccess().getSoftwareSoftwareParserRuleCall_2_0(), semanticObject.getSoftware());
		feeder.accept(grammarAccess.getConfigurationAccess().getAboutAboutParserRuleCall_3_0(), semanticObject.getAbout());
		feeder.accept(grammarAccess.getConfigurationAccess().getLibLibParserRuleCall_4_0(), semanticObject.getLib());
		feeder.accept(grammarAccess.getConfigurationAccess().getAuthorAuthorParserRuleCall_5_0(), semanticObject.getAuthor());
		feeder.accept(grammarAccess.getConfigurationAccess().getAuthor_emailAuthor_EmailParserRuleCall_6_0(), semanticObject.getAuthor_email());
		feeder.accept(grammarAccess.getConfigurationAccess().getRepositoryRepositoryParserRuleCall_7_0(), semanticObject.getRepository());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     Description returns Description
	 *
	 * Constraint:
	 *     textfield=STRING
	 */
	protected void sequence_Description(ISerializationContext context, Description semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, RestfulWebservicePackage.Literals.DESCRIPTION__TEXTFIELD) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, RestfulWebservicePackage.Literals.DESCRIPTION__TEXTFIELD));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getDescriptionAccess().getTextfieldSTRINGTerminalRuleCall_1_0(), semanticObject.getTextfield());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     AbstractElement returns Entity
	 *     Entity returns Entity
	 *
	 * Constraint:
	 *     (
	 *         description=Description? 
	 *         name=ID 
	 *         superType=[Entity|QualifiedName]? 
	 *         entity_type=Entity_Type 
	 *         attributes+=Attribute* 
	 *         functions+=Function* 
	 *         relations+=Relation*
	 *     )
	 */
	protected void sequence_Entity(ISerializationContext context, Entity semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     Entity_Type returns Entity_Type
	 *
	 * Constraint:
	 *     name=STRING
	 */
	protected void sequence_Entity_Type(ISerializationContext context, Entity_Type semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, RestfulWebservicePackage.Literals.ENTITY_TYPE__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, RestfulWebservicePackage.Literals.ENTITY_TYPE__NAME));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getEntity_TypeAccess().getNameSTRINGTerminalRuleCall_1_0(), semanticObject.getName());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     Function returns Function
	 *
	 * Constraint:
	 *     (description=Description? name=ID (params+=ID params+=ID*)? type=ID)
	 */
	protected void sequence_Function(ISerializationContext context, Function semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     AbstractElement returns Import
	 *     Import returns Import
	 *
	 * Constraint:
	 *     importedNamespace=QualifiedNameWithWildcard
	 */
	protected void sequence_Import(ISerializationContext context, Import semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, RestfulWebservicePackage.Literals.IMPORT__IMPORTED_NAMESPACE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, RestfulWebservicePackage.Literals.IMPORT__IMPORTED_NAMESPACE));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getImportAccess().getImportedNamespaceQualifiedNameWithWildcardParserRuleCall_1_0(), semanticObject.getImportedNamespace());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     Lib returns Lib
	 *
	 * Constraint:
	 *     name=STRING
	 */
	protected void sequence_Lib(ISerializationContext context, Lib semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, RestfulWebservicePackage.Literals.LIB__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, RestfulWebservicePackage.Literals.LIB__NAME));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getLibAccess().getNameSTRINGTerminalRuleCall_1_0(), semanticObject.getName());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     Relation returns ManyToMany
	 *     ManyToMany returns ManyToMany
	 *
	 * Constraint:
	 *     (name=ID type=[Entity|ID])
	 */
	protected void sequence_ManyToMany(ISerializationContext context, ManyToMany semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, RestfulWebservicePackage.Literals.RELATION__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, RestfulWebservicePackage.Literals.RELATION__NAME));
			if (transientValues.isValueTransient(semanticObject, RestfulWebservicePackage.Literals.RELATION__TYPE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, RestfulWebservicePackage.Literals.RELATION__TYPE));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getManyToManyAccess().getNameIDTerminalRuleCall_0_0(), semanticObject.getName());
		feeder.accept(grammarAccess.getManyToManyAccess().getTypeEntityIDTerminalRuleCall_2_0_1(), semanticObject.eGet(RestfulWebservicePackage.Literals.RELATION__TYPE, false));
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     Module returns Module
	 *     AbstractElement returns Module
	 *
	 * Constraint:
	 *     (description=Description? name=QualifiedName elements+=AbstractElement*)
	 */
	protected void sequence_Module(ISerializationContext context, br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.restfulWebservice.Module semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     Relation returns OneToMany
	 *     OneToMany returns OneToMany
	 *
	 * Constraint:
	 *     (name=ID type=[Entity|ID])
	 */
	protected void sequence_OneToMany(ISerializationContext context, OneToMany semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, RestfulWebservicePackage.Literals.RELATION__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, RestfulWebservicePackage.Literals.RELATION__NAME));
			if (transientValues.isValueTransient(semanticObject, RestfulWebservicePackage.Literals.RELATION__TYPE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, RestfulWebservicePackage.Literals.RELATION__TYPE));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getOneToManyAccess().getNameIDTerminalRuleCall_0_0(), semanticObject.getName());
		feeder.accept(grammarAccess.getOneToManyAccess().getTypeEntityIDTerminalRuleCall_2_0_1(), semanticObject.eGet(RestfulWebservicePackage.Literals.RELATION__TYPE, false));
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     Relation returns OneToOne
	 *     OneToOne returns OneToOne
	 *
	 * Constraint:
	 *     (name=ID type=[Entity|ID])
	 */
	protected void sequence_OneToOne(ISerializationContext context, OneToOne semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, RestfulWebservicePackage.Literals.RELATION__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, RestfulWebservicePackage.Literals.RELATION__NAME));
			if (transientValues.isValueTransient(semanticObject, RestfulWebservicePackage.Literals.RELATION__TYPE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, RestfulWebservicePackage.Literals.RELATION__TYPE));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getOneToOneAccess().getNameIDTerminalRuleCall_0_0(), semanticObject.getName());
		feeder.accept(grammarAccess.getOneToOneAccess().getTypeEntityIDTerminalRuleCall_2_0_1(), semanticObject.eGet(RestfulWebservicePackage.Literals.RELATION__TYPE, false));
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     Repository returns Repository
	 *
	 * Constraint:
	 *     name=STRING
	 */
	protected void sequence_Repository(ISerializationContext context, Repository semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, RestfulWebservicePackage.Literals.REPOSITORY__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, RestfulWebservicePackage.Literals.REPOSITORY__NAME));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getRepositoryAccess().getNameSTRINGTerminalRuleCall_1_0(), semanticObject.getName());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     Software returns Software
	 *
	 * Constraint:
	 *     name=STRING
	 */
	protected void sequence_Software(ISerializationContext context, Software semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, RestfulWebservicePackage.Literals.SOFTWARE__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, RestfulWebservicePackage.Literals.SOFTWARE__NAME));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getSoftwareAccess().getNameSTRINGTerminalRuleCall_1_0(), semanticObject.getName());
		feeder.finish();
	}
	
	
}
