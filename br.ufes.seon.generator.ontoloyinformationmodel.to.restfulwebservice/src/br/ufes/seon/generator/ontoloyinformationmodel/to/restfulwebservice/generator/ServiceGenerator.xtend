package br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.generator

import br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.restfulWebservice.Configuration
import br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.restfulWebservice.Entity
import java.util.List
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.AbstractGenerator
import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.xtext.generator.IGeneratorContext

class ServiceGenerator extends AbstractGenerator{
	
	var PATH = "src/"
	
	override doGenerate(Resource resource, IFileSystemAccess2 fsa, IGeneratorContext context) {

		val List<Configuration> configurations = resource.allContents.toIterable.filter(Configuration).toList;
		
		val Configuration configuration = configurations.get(0)
		
		val lib_name = configuration.lib.name.toLowerCase

		//Criando o config.py 
		fsa.generateFile(PATH +"config.py", createConfigFile())

		//Criando o run.py 
		fsa.generateFile(PATH +"run.py", createRun())
		
		//Criando o run.sh
		fsa.generateFile(PATH +"run.sh", createRunScript())
		
		var List<Entity> entities = resource.allContents.toIterable.filter(Entity).toList;
		
		fsa.generateFile(PATH +"app/__init__.py", entities.configurationInit(configuration))	
		
		for (e: entities){
			fsa.generateFile(PATH +"app/"+e.name.toLowerCase+"/controllers.py", e.createEntityRestful(lib_name))
			fsa.generateFile(PATH +"app/"+e.name.toLowerCase+"/__init__.py", "")	
		}		
	}
	
	def createEntityRestful(Entity entity,String lib_name)'''
		from flask import request, jsonify
		from flask_restplus import Resource, fields
		import json
		from app import api
		from «lib_name».application import factories
		
		«entity.name.toLowerCase»_model = api.model('«entity.name.toFirstUpper»', {
			'uuid': field.String,
			«FOR attribute: entity.attributes»	
		    '«attribute.name»': fields.«attribute.type»,
		    «ENDFOR»
		    «IF entity.superType!=null»
			    «FOR attribute: entity.superType.attributes»	
			   	'«attribute.name»': fields.«attribute.type»,
			    «ENDFOR»
		    «ENDIF»
		})
		
		«entity.name.toLowerCase»_namespace = api.namespace('«entity.name»', description='«IF entity.description!=null»«entity.description.textfield»«ENDIF»')
		
		@«entity.name.toLowerCase»_namespace.route('/')
		class ControllerGetPut(Resource):
			def __init__(self, *args, **kwargs):
				self.model_application = factories.«entity.name»Factory()
			
			@api.response(200, 'Success')
			def get(self,uuid):
				"""
					Get  a «entity.name.toLowerCase»
				"""        
				try:
					result = self.model_application.find_by_uuid(uuid)
					return jsonify(result.to_dict())
			            
				except Exception as identifier:
					return identifier
			    
			@api.doc(body=«entity.name.toLowerCase»_model)
			@api.response(200, 'Success', «entity.name.toLowerCase»_model)
			@api.expect(«entity.name.toLowerCase»_model, validate=False)
			def put(self,uuid):
				"""
					Udpate a «entity.name.toLowerCase»
				"""
				try:
					result = self.model_application.find_by_uuid(uuid)
					self.model_application.update(result)
					return jsonify(result.to_dict())
				except Exception as identifier:
					return identifier
			            
			@api.response(200, 'Success')      
			def delete(self,uuid):
				"""
					Delete a «entity.name.toLowerCase»
				"""
				try:
					result = self.model_application.delete(uuid)    
					return jsonify(result)
				except Exception as identifier:
					return identifier
		
		@«entity.name.toLowerCase»_namespace.route('/<uuid>')
		@«entity.name.toLowerCase»_namespace.param('uuid', '«entity.name» UUID')
		class ControllerGetListPost(Resource):
		    
			def __init__(self, *args, **kwargs):
				self.model_application = factories.«entity.name»Factory()
			
			@api.response(200, 'Success', [«entity.name.toLowerCase»_model])
			def get(self):
				"""
					Returns list of «entity.name.toLowerCase»
				"""
				try:
					result = self.model_application.find_all()
					response = []
					for instance in result:
						response.append(instance.to_dict())
					return jsonify(response)
				except Exception as identifier:
					return json.dumps(identifier)
		    
			@api.response(200, 'Success', «entity.name.toLowerCase»_model)
			def post(self):
				"""
					Create a new «entity.name.toLowerCase»
				"""
				try:
					model = request.get()
					result = self.model_application.save(model)
					return jsonify(result)
					
				except Exception as identifier:
					return json.dumps(identifier)

	'''

	def configurationInit(List<Entity> entities, Configuration configuration)'''
		from flask import Flask, Blueprint
		from flask_restplus import Api
		
		app = Flask(__name__)
		api = Api(app, 
		        version='1.0', 
		        title='«configuration.software.name»',
		    	description='«configuration.about.name»',
		)
		
		# Configurations
		app.config.from_object('config')
		blueprint = Blueprint('api', __name__, url_prefix='/api')
		api.init_app(blueprint)
		
		«FOR e: entities»
			from app.«e.name.toLowerCase».controllers import «e.name.toLowerCase»_namespace
		«ENDFOR»
		
		«FOR e: entities»
			api.add_namespace(«e.name.toLowerCase»_namespace)
		«ENDFOR»
		app.register_blueprint(blueprint)
	'''
	
	def createRunScript()'''
		python run.py
	'''

	def createRun()'''
		from app import app
		app.run(host='0.0.0.0', port=8090, debug=True)
	'''
	
	def createConfigFile()'''
		DEBUG = True
		
		# Define the application directory
		import os
		BASE_DIR = os.path.abspath(os.path.dirname(__file__))  
		
		# Application threads. A common general assumption is
		# using 2 per available processor cores - to handle
		# incoming requests using one and performing background
		# operations using the other.
		THREADS_PER_PAGE = 2
		
		# Enable protection agains *Cross-site Request Forgery (CSRF)*
		CSRF_ENABLED     = True
		
		# Use a secure, unique and absolutely secret key for
		# signing the data. 
		CSRF_SESSION_KEY = "secret"
		
		# Secret key for signing cookies
		SECRET_KEY = "secret"
	
	'''
}