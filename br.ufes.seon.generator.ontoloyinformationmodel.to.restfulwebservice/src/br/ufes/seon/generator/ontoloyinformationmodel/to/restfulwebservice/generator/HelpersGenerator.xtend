package br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.generator

import org.eclipse.xtext.generator.AbstractGenerator
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.xtext.generator.IGeneratorContext

class HelpersGenerator extends AbstractGenerator{
	
	override doGenerate(Resource resource, IFileSystemAccess2 fsa, IGeneratorContext context) {
		fsa.generateFile("requirements.txt", createRequirements())
		fsa.generateFile("Dockerfile", createDockerFile())
		fsa.generateFile("start.sh", createStart())
	}
	
	private def createDockerFile()'''
	FROM tiangolo/uwsgi-nginx-flask:python3.6-alpine3.7
	RUN apk --update add bash nano
	COPY ./requirements.txt requirements.txt
	RUN pip install -r requirements.txt
	'''
	
	private def createRequirements()'''
		aniso8601==8.0.0
		attrs==19.3.0
		click==7.1.2
		Flask==1.1.2
		flask-restplus==0.13.0
		importlib-metadata==1.6.0
		itsdangerous==1.1.0
		Jinja2==2.11.2
		jsonschema==3.2.0
		MarkupSafe==1.1.1
		pyrsistent==0.16.0
		pytz==2020.1
		six==1.15.0
		Werkzeug==0.16.1
		zipp==3.1.0
	'''
	private def createStart()'''
	#!/bin/bash
	app="docker.test"
	docker build -t ${app} .
	docker run -d -p 56733:80 \
	  --name=${app} \
	  -v $PWD:/app ${app}
	
	'''
	
}