/*
 * generated by Xtext 2.21.0
 */
package br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.tests

import br.ufes.seon.generator.ontoloyinformationmodel.to.restfulwebservice.restfulWebservice.Application
import com.google.inject.Inject
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.util.ParseHelper
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith

@ExtendWith(InjectionExtension)
@InjectWith(RestfulWebserviceInjectorProvider)
class RestfulWebserviceParsingTest {
	@Inject
	ParseHelper<Application> parseHelper
	
	@Test
	def void loadModel() {
		val result = parseHelper.parse('''
			Hello Xtext!
		''')
		Assertions.assertNotNull(result)
		val errors = result.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
	}
}
