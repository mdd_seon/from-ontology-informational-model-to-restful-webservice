# From Ontology Informational Model to Restful Webservice

This project transforms an ontology informational model into Restful Webservice. 

## Install

Copy the *.jar into plugin in Eclipse


## Usage
0. Open Eclipse
1. Create a Java Project
2. Create a file com extension .restfull
3. Write a model
4. Enjoy your webservice based on ontology model.

**Example:**
```python
Configuration {
	software: "EO"
	about: "The Enterprise Ontology (EO) aims at establishing a common conceptualization on the Entreprise domain, including organizations, organizational units, people, roles, teams and projects."
	lib_name: "eo_lib"
	author: "Paulo Sérgio dos Santos Júnior"
	author_email:"paulossjunior@gmail.com"
	repository: "https://gitlab.com/integration_seon/webservice/eo_webservice"	
}

# "Conceptual model of the Teams."
module Teams {
	
	# "A human Physical Agent."
	entity Person{
		is: "person"
		
		name: String
		
		email: String
		
	}
	# "A Person allocated in a Team."
	entity TeamMember extends Person {
		is: "person"
		teammebership OneToMany TeamMembership
	}
	
	# "A Social Role, recognized by the Organization, assigned to Agents when they are hired, included in a team, allocated or  participating in activities.Ex.: System Analyst, Designer, Programmer, 
	Client Organization."
	entity Organization_Role{
		is: "organizational_role"
		name: String
		description : Text
		teammebership OneToMany TeamMembership
		
	}
	
	# "Relationship among Team member, organizational Role and team."
	entity TeamMembership {
		is: "TeamMembership"
		date: Date
		
	}
	
	# "Social Agent representing a group of people with a defined purpose. 
	Ex.: a Testing team, a Quality Assurance team, a Deployment team."
	entity Team {
		is: "Team"
		name: String
		description : Text
		
		teammebership OneToMany TeamMembership
	}
	# "A Team with a project"
	entity ProjectTeam extends Team{
		is: "Team"
		
	}
	# "A Social Object as a temporary endeavor undertaken to create a unique product, service, or result.
	Ex.: A project to produce a software on demand."
	entity Project{
		is: "Project"
		name: String
		description : Text
		team OneToMany ProjectTeam
	}
	
}
```
## Run the webservice
```bash
 python run.sh
```
